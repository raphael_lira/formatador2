/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.height = '420px';

};


CKEDITOR.on('instanceReady', function (event) {
	event.editor.on('paste', function (pasteEvent) {
    	var items = pasteEvent.data.dataValue;
		var texto = JSON.stringify(items);
		var regex  = /[\\'"]<img\s+src=[\\'"].+[\\'"]\s?\/>[\\'"]/g;
	
		if(regex.test(texto)){
			pasteEvent.stop();
		}
  	});
});