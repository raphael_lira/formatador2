<?php
session_start();

class image{

    private $save_path = 'uploads/';
    private $image_string = '';
    private $image_name = '';
    private $image;
    private $response = array();

    public $loaded = false;

    public function __construct(){
        $this->response = array(
            'error' => 1,
            'message' => 'unknown error.'
        );

//        $this->image_string = filter_input(INPUT_POST, 'image');
		$this->image_string = str_replace(array("%2b", "%2f", "%3d"), array("+", "/", "="), $_POST['image']);


        $ext = substr($this->image_string,11,3);
        $randomLetters = $rand = substr(md5(microtime()),rand(0,26),6);
        $imgnumber = count(scandir($this->save_path));

        $this->image_name = "$imgnumber$randomLetters.$ext";

        if(!empty($this->image_name) && !empty($this->image_string)){
            $this->loaded = true;
        }
    }

    public function save(){
        if(!empty($this->image_name) && !empty($this->image_string)){
            return $this->progress();
        }
        else{
            $this->response['message'] = 'Error. Not all required infor is given.';
            $this->response['error'] = 1;
            return $this->response;
        }
    }

    private function progress(){
        $imgarr = explode(',', $this->image_string);
        if(!isset($imgarr[1])){
            $this->response['message'] = 'Error on post data image. String is not the expected string.';
            $this->response['error'] = 1;
            return $this->response;
        }
        $this->image = base64_decode($imgarr[1]);
        if(!is_null($this->image)){
            $file = $this->save_path . $this->image_name;
            if(file_exists($file)){
                $this->response['message'] = 'Image already exists on server.';
                $this->response['error'] = 1;
                return $this->response;
            }
            if(file_put_contents($file, $this->image) !== false){
                $this->response['message'] = 'Image saved to server';
                $this->response['error'] = 0;
                $this->response['source'] = 'ckeditor/plugins/imageuploader/'.$file;
                return $this->response;
            }
            else{
                $this->response['error'] = 1;
                $this->response['message'] = 'Error writing file to disk';
                return $this->response;
            }
        }
        else{
            $this->response['message'] = 'Error decoding base64 string.';
            return $this->response;
        }
    }
}

$img = new image();
if($img->loaded){
    $result = $img->save();
    echo json_encode($result);
}
else{
    $result = array(
        'error' => 1,
        'message' => 'Not all post data given'
    );
    echo json_encode($result);
}
?>