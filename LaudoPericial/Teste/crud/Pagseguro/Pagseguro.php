<?php 

class Pagseguro {
	
	public $callback = array(
		1 =>	'Pendente',
		2 =>	'Pago',
		3 =>	'Negado',
		4 =>	'Expirado',
		5 =>	'Cancelado',
		6 =>	'Não Finalizado',
		7 =>	'Autorizado',
		8 =>	'Chargeback'
	);
	public $statusPagseguro = array(
		'PENDING' 				=> '1',
		'ACTIVE'  				=> '7',
		'EXPIRED'				=> '4',
		'CANCELLED_BY_RECEIVER' => '5',
		'CANCELLED_BY_SENDER'	=> '5',
		'CANCELLED'				=> '5'
	);
	
	private $urlPag = "https://ws.sandbox.pagseguro.uol.com.br/v2/";
	private $compra ;
    private $token = '8728F78B99744075A3AB1D2F4BEEF563';
    private $email = 'raphael_lira_san@hotmail.com';
    private $valor = 0;

	function __construct(){
        $this->compra = new stdClass() ;
		$this->compra->preApprovalDetails = '';//descrição na fatura 
		$this->compra->redirectURL= 'http://preventiva.med.br/LaudoPericial';//url o qual retornará após finalizado
	}


	public function consulta($token){
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$this->urlPag.'pre-approvals/notifications/'.$token.'?email='.$this->email.'&token='.$this->token);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		curl_close($curl);
		$retorno = simplexml_load_string($response);
		return $retorno;
    }
	
	public function setId($id){
		$this->compra->reference = $id;
	}

	
	public function setItens($nome,$valor){
		$this->compra->preApprovalName = $nome;
        $this->valor = $valor;
	}
	
	public function setEndereco($rua,$numero,$complemento,$bairro,$cidade,$uf){
		
		$this->compra->senderAddressStreet = $rua;
		$this->compra->senderAddressNumber =  $numero;
		$this->compra->senderAddressComplement =  $complemento;
		$this->compra->senderAddressDistrict = $bairro;
		$this->compra->senderAddressCity = $cidade;
		$this->compra->senderAddressState = $uf;
		$this->compra->senderAddressCountry = "BRA";		
	}
	
	public function setConsumidor($nome,$email){
		$this->compra->senderName = $nome;
		$this->compra->senderEmail = $email;	
		
	}
	
	

	public function finalizar(){
        $this->compra->preApprovalCharge = "auto";
        $this->compra->preApprovalPeriod = 'Monthly';
        $this->compra->preApprovalFinalDate = date('c', strtotime(date('c'). ' + 2 years'));
        $this->compra->email = $this->email;
        $this->compra->token = $this->token; 
        $this->compra->preApprovalAmountPerPayment = number_format($this->valor,2,'.','');
        $this->compra->preApprovalMaxTotalAmount = number_format(($this->valor * 24),2,'.','');
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->urlPag."pre-approvals/request");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query((array) $this->compra));
		curl_setopt($curl, CURLOPT_HTTPHEADER, 
            array(
			 'Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1'
		    )
        );

		$response = curl_exec($curl);

		curl_close($curl);
		
		$retorno = simplexml_load_string($response);
		
		return $retorno ;
	}
	
}

/* 

	HELPERS

*/

function formatarDinheiro($valor){
	return number_format($valor,2,',','.') ; 	
}

function dinheiroToFloat($valor){
	if($valor == '' || $valor == null){
		$valor = 0;
	}
	$retorno = str_replace('_','',$valor);
	if(strpos($valor, ',') > 0){
		$retorno = str_replace('.','',$retorno);
		$retorno = str_replace(',','.',$retorno);
	}
	return $retorno;
}
