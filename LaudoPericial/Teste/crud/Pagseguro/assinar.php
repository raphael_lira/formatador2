<?php

/*

	gateway 0=outros meios de pagamentos|1=pagseguro

*/

session_start();
if(!isset($_SESSION['usuarioID'])){
	header('Location: ../../../login.php');
}

if(!isset($_GET['id'])){
	header('Location: ../lista_licenca.php');
}

require "Pagseguro.php";

require '../database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = " SELECT * FROM licenca WHERE id_licenca = {$_GET['id']}";

//puxa os dados da licença
foreach ($pdo->query($sql) as $row){
	$tituloAtual = $row['titulo'];
	$valor = $row['valor'];
	$id = $row['id_licenca'];
}

//classe Pagseguro
$pagseguro = new Pagseguro;

//seta o id da licença
$pagseguro->setId($id);

//interação com a api do pagseguro		
$pagseguro->setItens($tituloAtual,$valor);

$retorno = $pagseguro->finalizar();		

//echo var_dump($retorno);exit;

header("Location: https://sandbox.pagseguro.uol.com.br/v2/pre-approvals/request.html?code=".$retorno->code);