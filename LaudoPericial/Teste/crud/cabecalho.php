<?php
session_start();
if(!isset($_SESSION['usuarioID'])){
	header('Location: ../../login.php');
}

?>
<!DOCTYPE html>
<head>
    	<title>Cabeçalho padrão</title>
    	<meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
   	
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	
	<script src="js/bootstrap.min.js"></script>
	<script src="../ckeditor/ckeditor.js"></script>
	<script src="../ckeditor/samples/js/sample.js"></script>
	
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
	<script>
		$(document).ready(function() {
			initSample();

			CKEDITOR.instances.editor.on('change', function() {
				$('#cabecalho').text(CKEDITOR.instances.editor.getData());
				
				$('.alert').hide(100);				
				
				$.post('salva_cabecalho.php',{cabecalho:$('#cabecalho').text()},function(data,sts){
					if(sts == "success"){
						$('.alertSalvo').show(300);
					}else{
						$('.alertErro').show(300);
					}
				})
			});

		});
	</script>
</head>
<?php
	include 'database.php';
	$pdo = Database::connect();

	$sql_user = "SELECT login, adm, id_licenca FROM usuario WHERE id_usuario = {$_SESSION['usuarioID']}";
	foreach ($pdo->query($sql_user) as $row){
		$user = $row['login'];
		$_SESSION['usuarioLogin'] = $row['login'];;
		$_SESSION['usuarioTipo'] = $row['adm'];
		$_SESSION['usuarioLicenca'] = $row['id_licenca'];
	}

	if ($_SESSION['usuarioLicenca'] == ''){
		header('Location: ../sair.php');
	}

?>
<html>
	<body>
		<section id="content">
			<div id="header">Logado como <b><?php echo $_SESSION['usuarioLogin']?></b> <?php if ($_SESSION['usuarioLicenca'] == 1) { ?>| <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> <?php }?> | <a href="../sair.php">Sair</a> </div>

			<div id="nav" style="margin: auto;">

				<div class="container offset1" >
						<div class="row">
							<h3>Cabeçalho padrão</h3>
						</div>
						<form id="form">
							<div class="row">
							
								<?php 
									$arquivo = fopen("capitulos/cabecalho.txt", "r");

									$cabecalho = '';

									while(!feof($arquivo)){
										$linha = fgets($arquivo, 1024);
										$cabecalho .= $linha;
									};
									fclose($arquivo);

								?>
								<div id="editor" tabindex="1"><?php echo sprintf('%s',$cabecalho);?></div>
								<textarea style="display:none;" name="cabecalho" id="cabecalho"><?php echo $cabecalho ?></textarea>
								
															
							</div>
						</form>
					</div> <!-- /container -->
				</div>
		</section>
		<section id="" class="container-fluid ">
			<div class="row col-md-8">
				<div class="alert alerta-gerais alert-success alertSalvo text-center" style="display:none;" role="alert">
					Salvo com sucesso.
				</div>
				<div class="alert alerta-gerais alert-danger alertErro text-center" style="display:none;" role="alert">
					Erro: Falha ao salvar.
				</div>
			</div>
		</section>
	</body>
	

</html>