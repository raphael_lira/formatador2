<p>Luiz Carlos Moreira</p>

<p>Especialista em Medicina do Trabalho ANAMT / AMB<br />
Ergonomista Certificado ABERGO<br />
Profissional Certificado PCMSO-G (Galvanoplastia) /FUNDACENTRO<br />
Membro do ICOH &ndash; International Commission on Occupational Health<br />
Associado &agrave; Sociedade Paulista de Per&iacute;cias M&eacute;dicas<br />
Associado &agrave; Sociedade Brasileira de Per&iacute;cias M&eacute;dicas<br />
Especialista em Per&iacute;cias M&eacute;dicas pela Associa&ccedil;&atilde;o Brasileira de Medicina Legal e Per&iacute;cias M&eacute;dicas &ndash; ABMLPM / AMB<br />
M&eacute;dico Perito da Justi&ccedil;a do Trabalho de Campinas<br />
luiz@preventiva.med.br&nbsp; &nbsp;</p>
