<?php
    session_start();
    if(!isset($_SESSION['usuarioID'])){
        header('Location: ../../login.php');
    }
	require 'database.php';
    
	if (!function_exists('mb_ucfirst')) {
        function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
            $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
            $str_end = "";
            if ($lower_str_end) {
                $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
            }
            else {
                $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
            }
            $str = $first_letter . $str_end;
            return $str;
        }
    }

	if ( !empty($_POST)) {
		// keep track validation errors
		$nome_docError = null;
        	$data_periciaError = null;
		$num_procError = null;
		$reclamanteError = null;
		$reclamadaError = null;
		$cidadeError = null;
		$estadoError = null;
		
		// keep track post values
		$nome_doc = ($_POST['nome_doc']);
		$data_pericia = ($_POST['data_pericia']);
		$num_proc = ($_POST['num_proc']);
		$reclamante = ($_POST['reclamante']);
		$reclamada = ($_POST['reclamada']);
		$cidade = ucwords($_POST['cidade']);
		$estado = $_POST['estado'];
		$instancia_vara = trim("EXCELENTÍSSIMO SENHOR DOUTOR JUIZ DO TRABALHO DA Xa VARA DA JUSTIÇA DO TRABALHO DE $cidade - ".mb_strtoupper($estado));
		$descricao_laudo = trim('Luiz Carlos Moreira, na qualidade de Perito Médico nomeado por V. Exa., apresento o Laudo Médico Pericial, referente ao processo supra mencionado. Solicito a gentileza da liberação dos honorários prévios periciais existentes, bem como o arbitramento dos honorários periciais em definitivo, independente de acordo entre as partes, cujo valor estimado total é de R$ 6.780,00 (Seis mil, setecentos e oitenta) reais, corrigidos à época do pagamento, considerando os tempos técnicos destinados à análise detalhada dos autos, à avaliação médica pericial do Reclamante, à confecção minuciosa do Laudo Pericial e considerando ainda as despesas de deslocamento, de material e de equipamentos utilizados. Solicito ainda, que seja designado pagamento de honorários periciais em caso de acordo antecipado entre as partes; que em caso de sucumbência do(a) Reclamante, o pagamento dos honorários periciais seja requisitado nos termos do Provimento GP-GR 01/2009.');
		$cabecalho = '';		

		// validate input
		$valid = true;
		if (empty($nome_doc)) {
			$nome_docError = ('Por favor, preencha o campo \'Nome do documento\'');
			$valid = false;
		}

		if (empty($reclamante)) {
			$reclamanteError = ('Por favor, preencha o campo \'Reclamante\'');
			$valid = false;
		}

		if (empty($reclamada)) {
			$reclamadaError = ('Por favor, preencha o campo \'Reclamada\'');
			$valid = false;
		}
		
		if (empty($data_pericia)) {
            $data_periciaError = ('Por favor, preencha o campo \'Data da perícia\'');
			$valid = false;
		}
		
		if (empty($num_proc)) {
			$num_procError = ('Por favor, preencha o campo \'Número do processo\'');
			$valid = false;
		}
		
		if (empty($cidade)) {
			$cidadeError = ('Por favor, preencha o campo \'Cidade\'');
			$valid = false;
		}
		
		if (empty($estado)) {
			$estadoError = ('Por favor, preencha o campo \'Estado\'');
			$valid = false;
		}
		
		// insere dados no postgresql
		if ($valid) {
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $data = implode("-",array_reverse(explode("/",$data_pericia)));
            $sql = "INSERT INTO documentos(nome_documento, data_pericia, n_processo, id_usuario, reclamante, reclamada, instancia_vara, descricao_laudo, cidade, estado, cabecalho)
					VALUES ('$nome_doc', '$data', '$num_proc', {$_SESSION['usuarioID']}, '$reclamante', '$reclamada', '$instancia_vara', '$descricao_laudo', '$cidade', '$estado', '$cabecalho')";
            //die("<pre>$sql</pre>");
            $pdo->query($sql);
            $sql = "SELECT MAX(id_doc) AS id_doc FROM documentos";
            $qry = $pdo->query($sql);
            $rs = $qry->fetch(PDO::FETCH_ASSOC);
            $doc = array_values($rs)[0];
	  		$_SESSION['doc'] = $doc;
	    	$_SESSION['vara'] = $instancia_vara;
	    	$_SESSION['processo'] = $num_proc;
	    	$_SESSION['reclamante'] = $reclamante;
	    	$_SESSION['reclamada'] = $reclamada;
	    	$_SESSION['desc_laudo'] = $descricao_laudo;
	    	$_SESSION['cidade'] = $cidade;
	    	$_SESSION['estado'] = $estado;
	    	$_SESSION['document'] = $nome_doc;
	    	$_SESSION['data_pericia'] = $data_pericia;
	    	$_SESSION['cabecalho'] = $cabecalho;

			for($i=1; $i<=14; $i++){
				$name = 'capitulo_'.$i.'.txt';
				$file = fopen($name, 'w+');
				fclose($file);
				$sql_cap = "INSERT INTO capitulos (id_documento, paragrafo, titulo, n_capitulo)
							VALUES ($doc, lo_import('/var/www/html/preventiva.med.br/public_html/LaudoPericial/crud/$name'), '', $i)";//alterar para o endereço correto do arquivo
				//die("<pre>$sql_cap</pre>");
				$pdo->query($sql_cap);
				$_SESSION['tc'.$i] = '';
				$_SESSION['c'.$i] = '';
			}
			Database::disconnect();
			header("Location: ../index.php?p=cab&doc=".$doc);
		}
	}
?>


<!DOCTYPE html>
<html lang="pt">
<head>
	<title>Novo documento</title>
    <meta charset="utf-8">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../style.css" type="text/css">
    <script src="js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>

	<script>
	$(document).ready(function() {
            	$.datepicker.regional['pt'] = {
                closeText: 'Fechar',
                prevText: '&#x3c;Anterior',
                nextText: 'Seguinte',
                currentText: 'Hoje',
                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                weekHeader: 'Sem',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
            	$.datepicker.setDefaults($.datepicker.regional['pt']);
		$("#data_pericia").datepicker();
	});

	</script>
</head>

<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $_SESSION['usuarioLogin']?></b> <?php if ($_SESSION['usuarioLicenca'] == 1) { ?>| <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> <?php }?> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">
			<div class="container">

					<div class="span10 offset1">
						<div class="row">
							<h3>Novo Documento</h3>
						</div>

						<form class="form-horizontal" action="create.php" method="post">
						  <div class="control-group <?php echo !empty($nome_docError)?'error':'';?>">
							<label class="control-label">Nome do documento</label>
							<div class="controls">
								<input name="nome_doc" id="nome_doc" type="text"  placeholder="Nome do documento" value="<?php echo !empty($nome_doc)?$nome_doc:'';?>">
								<?php if (!empty($nome_docError)): ?>
									<span class="help-inline"><?php echo $nome_docError;?></span>
								<?php endif; ?>
							</div>
						  </div>
						  <div class="control-group <?php echo !empty($data_periciaError)?'error':'';?>">
							<label class="control-label">Data da perícia</label>
							<div class="controls">
								<input name="data_pericia" id="data_pericia" type="text" placeholder="Data da perícia" value="<?php echo !empty($data_pericia)?$data_pericia:'';?>">
								<?php if (!empty($data_periciaError)): ?>
									<span class="help-inline"><?php echo $data_periciaError;?></span>
								<?php endif;?>
							</div>
						  </div>
						  <div class="control-group <?php echo !empty($num_procError)?'error':'';?>">
							<label class="control-label">Número do processo</label>
							<div class="controls">
								<input name="num_proc" id="num_proc" type="text"  placeholder="Número do processo" value="<?php echo !empty($num_proc)?$num_proc:'';?>">
								<?php if (!empty($num_procError)): ?>
									<span class="help-inline"><?php echo $num_procError;?></span>
								<?php endif;?>
							</div>
						  </div>
						  <div class="control-group <?php echo !empty($reclamanteError)?'error':'';?>">
							<label class="control-label">Reclamante</label>
							<div class="controls">
								<input name="reclamante" id="reclamante" type="text"  placeholder="Reclamante" value="<?php echo !empty($reclamante)?$reclamante:'';?>">
								<?php if (!empty($reclamanteError)): ?>
									<span class="help-inline"><?php echo $reclamanteError;?></span>
								<?php endif; ?>
							</div>
						  </div>
						  <div class="control-group <?php echo !empty($reclamadaError)?'error':'';?>">
							<label class="control-label">Reclamada</label>
							<div class="controls">
								<input name="reclamada" id="reclamada" type="text"  placeholder="Reclamada" value="<?php echo !empty($reclamada)?$reclamada:'';?>">
								<?php if (!empty($reclamadaError)): ?>
									<span class="help-inline"><?php echo $reclamadaError;?></span>
								<?php endif; ?>
							</div>
						  </div>
						  <div class="control-group <?php echo !empty($estadoError)?'error':'';?>">
							<label class="control-label">Estado</label>
							<div class="controls">
								<select name="estado" id="estado"> 
									<option value="">Selecione o Estado</option> 
									<option value="ac">Acre</option> 
									<option value="al">Alagoas</option> 
									<option value="am">Amazonas</option> 
									<option value="ap">Amapá</option> 
									<option value="ba">Bahia</option> 
									<option value="ce">Ceará</option> 
									<option value="df">Distrito Federal</option> 
									<option value="es">Espírito Santo</option> 
									<option value="go">Goiás</option> 
									<option value="ma">Maranhão</option> 
									<option value="mt">Mato Grosso</option> 
									<option value="ms">Mato Grosso do Sul</option> 
									<option value="mg">Minas Gerais</option> 
									<option value="pa">Pará</option> 
									<option value="pb">Paraíba</option> 
									<option value="pr">Paraná</option> 
									<option value="pe">Pernambuco</option> 
									<option value="pi">Piauí</option> 
									<option value="rj">Rio de Janeiro</option> 
									<option value="rn">Rio Grande do Norte</option> 
									<option value="ro">Rondônia</option> 
									<option value="rs">Rio Grande do Sul</option> 
									<option value="rr">Roraima</option> 
									<option value="sc">Santa Catarina</option> 
									<option value="sp" selected>São Paulo</option> 
									<option value="se">Sergipe</option> 
									<option value="to">Tocantins</option> 
							        </select>
								<?php if (!empty($estadoError)): ?>
									<span class="help-inline"><?php echo $estadoError;?></span>
								<?php endif; ?>
							</div>
						  </div>
						  <div class="control-group <?php echo !empty($cidadeError)?'error':'';?>">
							<label class="control-label">Cidade</label>
							<div class="controls">
								<input name="cidade" id="cidade" type="text"  placeholder="Cidade" value="<?php echo !empty($cidade)?$cidade:'';?>">
								<?php if (!empty($cidadeError)): ?>
									<span class="help-inline"><?php echo $cidadeError;?></span>
								<?php endif; ?>
							</div>
						  </div>
						  <div class="form-actions">
							  <button type="submit" class="btn btn-success">Criar</button>
							  <a class="btn" href="index.php">Cancelar</a>
						  </div>
						</form>
					</div>

			</div> <!-- /container -->
		</div>
	</section>

  </body>
</html>