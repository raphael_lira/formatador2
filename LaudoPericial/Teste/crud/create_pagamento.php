﻿<!DOCTYPE html>

<?php
	session_start();
	//echo var_dump($_SESSION);exit;
	require 'database.php';
	require 'pagseguro/Pagseguro.php';
	
	


	$pagseguro = new Pagseguro;
	
	$id = 0;
	$licenca = 0;
	
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( !empty($_GET['licenca'])) {
		$licenca = $_REQUEST['licenca'];
	}
	
	if($_SESSION['usuarioTipo'] < 1 && ($_SESSION['usuarioLicenca'] != $id || $_SESSION['usuarioLicenca'] != $licenca)){
		header('Location: lista_licenca.php');
	}

	if ($id != 0){
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql = " SELECT * FROM pagamentos WHERE id_pagamentos = $id";
										  
		foreach ($pdo->query($sql) as $row){
			$gateway = $row['gateway'];
			$status = $row['status'];
			$valor = $row['valor'];
			$transacao = $row['transacao'];
			$licenca = $row['id_licenca'];
	    }
		Database::disconnect();
	}
	
	if ( !empty($_POST)) {
		
		$valor = trim($_POST['valor']);
		$status = trim($_POST['status']);
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		if ($id != 0){
			$sql = "UPDATE pagamentos SET status = '$status',valor = '".dinheiroToFloat($valor)."' WHERE id_pagamentos = $id";
		}else{
			$sql = "INSERT INTO pagamentos(id_licenca,gateway,status,valor,data) VALUES ('$licenca',0,'$status','".dinheiroToFloat($valor)."','".date('Y-m-d H:i:s')."')";
		}		
		$pdo->query($sql);
		
		Database::disconnect();
		
		header("Location: lista_pagamentos.php?id=$licenca");
		
	}

?>
<html lang="pt">
<head>
	<title>Nova licença</title>
    <meta charset="utf-8">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../style.css" type="text/css">
    <script src="js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
	<script>
	
		$(document).ready(function(){
			$(".dinheiro").maskMoney({
				allowNegative: false,
				thousands: '.',
				decimal: ',',
				affixesStay: false
			});
			
		})
		
		
	</script>
</head>

<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $_SESSION['usuarioLogin'] ?></b> | <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">
			<div class="container">

					<div class="span10 offset1">
						<div class="row">
							<h3>Nova Licença</h3>
						</div>

						<form class="form-horizontal" action="" method="post">
						  
						<?php if ($id != 0) {?>
								<input type="hidden" name="id" id="id" value="<?= $id ?>">
							  <div class="control-group">
								<label class="control-label">Valor</label>
								<div class="controls">
									<input name="valor" id="valor" type="text" class="dinheiro"  placeholder="Valor" value="<?php echo formatarDinheiro($valor) ?>">								
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Codigo de transação</label>
								<div class="controls">
									<input readonly name="transacao" id="transacao" type="text" value="<?php echo $transacao ?>">
								</div>
							  </div>	
							  <div class="control-group">
								<label class="control-label">Status</label>
								<div class="controls">
									<select name="status" id="status">
										<?php 
											$keys = array_keys($pagseguro->callback); 
											for($i=0;$i<count($keys);$i++){
										?>		
											<option <?php if($status == $keys[$i]) echo 'selected' ?> value="<?= $keys[$i] ?>"><?= $pagseguro->callback[$keys[$i]] ?></option>
										<?php } ?>
									</select>							
								</div>
							  </div>

						  <?php } else{ ?>
							   	<input type="hidden" name="licenca" id="licenca" value="<?= $licenca ?>">
							 <div class="control-group">
								<label class="control-label">Status</label>
								<div class="controls">
									<select name="status" id="status">
										<?php 
											$keys = array_keys($pagseguro->callback); 
											for($i=0;$i<count($keys);$i++){
										?>		
											<option value="<?= $keys[$i] ?>"><?= $pagseguro->callback[$keys[$i]] ?></option>
										<?php } ?>
									</select>							
								</div>
							  </div>

							  <div class="control-group">
								<label class="control-label">Valor</label>
								<div class="controls">
									<input name="valor" id="valor" type="text" class="dinheiro"  placeholder="Valor" value="">								
								</div>
							  </div>

							<?php }?>							  
						  <div class="form-actions">
							  <?php if ($id != 0) {?>
								<button type="submit" class="btn btn-success">  Alterar </button>
							  <?php } else{?>
								<button type="submit" class="btn btn-success">  Criar </button>
							  <?php }?> 
							  <a class="btn" href="lista_licenca.php">Cancelar</a>
						  </div>
						</form>
					</div>

			</div> <!-- /container -->
		</div>
	</section>

  </body>
</html>