﻿<!DOCTYPE html>
<?php	

	session_start();

	include 'database.php';	
	$id = 0;
	$user = "";
	
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ($id != 0){
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql = " SELECT login, adm, id_licenca, senha FROM usuario WHERE id_usuario = $id AND adm <= {$_SESSION['usuarioTipo']}";
										  
		foreach ($pdo->query($sql) as $row){
			$user = $row['login'];
			$admAtual = $row['adm'];
			$licencaAtual = $row['id_licenca'];
			$senhaAtual = $row['senha'];
	    }
		Database::disconnect();
	}
	
	if ( !empty($_POST)) {
		
		$login = ($_POST['login_user']);
		$senha = ($_POST['senha_user']);
		$adm = ($_POST['tipo_user']);
		$licenca = ($_POST['licenca_user']);
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql = " SELECT id_usuario FROM usuario ORDER BY id_usuario DESC limit 1";
										  
		foreach ($pdo->query($sql) as $row){
			$ultimoID = $row['id_usuario'];
	    }
		
		$ultimoID = $ultimoID + 1;
		
		if ($id != 0){
			$sql = "UPDATE usuario SET senha = '$senha', adm = $adm, id_licenca = $licenca WHERE id_usuario = $id";
		}else{
			$sql = "INSERT INTO usuario (id_usuario, login, senha, adm, id_licenca) VALUES ($ultimoID, '$login','$senha',$adm, $licenca)";		
		}
		$pdo->query($sql);
		
		Database::disconnect();
		
		header("Location: lista_usuarios.php");
		
	}
	
?>
<html lang="pt">
<head>
	<title>Novo usuário</title>
    <meta charset="utf-8">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../style.css" type="text/css">
    <script src="js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>

</head>

<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $_SESSION['usuarioLogin'] ?></b> | <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">
			<div class="container">

					<div class="span10 offset1">
						<div class="row">
							<h3>Novo Usuário</h3>
						</div>

						<form class="form-horizontal" action="" method="post" autocomplete="off">
						  <?php if ($id != 0) {?>
							  <div class="control-group">
								<label class="control-label">Login do usuário</label>
								<div class="controls">
									<input name="login_user" id="login_user" disabled="disabled" type="text" placeholder="Login do usuário" value="<?php echo $user?>">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Senha do usuário</label>
								<div class="controls">
									<input name="senha_user" id="senha_user" type="password"  placeholder="Senha do usuário" value="<?php echo $senhaAtual?>">								
								</div>
							  </div>
						  <?php } else{?>
							<div class="control-group">
								<label class="control-label">Login do usuário</label>
								<div class="controls">
									<input name="login_user" id="login_user" autocomplete="off" type="text" placeholder="Login do usuário" value="">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Senha do usuário</label>
								<div class="controls">
									<input name="senha_user" id="senha_user" autocomplete="new-password" type="password"  placeholder="Senha do usuário" value="">								
								</div>
							  </div>
						  <?php }?>						  
						  <div class="control-group">
							<label class="control-label">Tipo</label>
							<div class="controls">
								<select name="tipo_user">
									<?php 
										if ($id != 0){

											if ($admAtual == 3 && $_SESSION['usuarioTipo'] >= 3){
												echo "<option value='3' selected> Super Administrador </option>";	
											}else{
												echo "<option value='3'> Super Administrador </option>";	
											}
											
											if ($admAtual == 2 && $_SESSION['usuarioTipo'] >= 2){
												echo "<option value='2' selected> Financeiro </option>";	
											}else{
												echo "<option value='2'> Financeiro </option>";	
											}
											
											if ($admAtual == 1){
												echo "<option value='1' selected> Administrador </option>";	
											}else{
												echo "<option value='1'> Administrador </option>";	
											}
											
											if ($admAtual == 0){
												echo "<option value='0' selected> Comum </option>";	
											}else{
												echo "<option value='0'> Comum </option>";	
											}
										}else{
											echo "<option value=''> Sem licença </option>";	
											if($_SESSION['usuarioTipo'] >= 3)
												echo "<option value='3'> Super Administrador </option>";	
											if($_SESSION['usuarioTipo'] >= 2)
												echo "<option value='2'> Financeiro </option>";	
											echo "<option value='1'> Administrador </option>";	
											echo "<option value='0'> Comum </option>";	
										}
									?>
								</select>
							</div>
						  </div>
						  <div class="control-group">
							<label class="control-label">Licença</label>
							<div class="controls">
								<select name="licenca_user">
									<?php				   
									   $pdo = Database::connect();
									   $sql = " SELECT id_licenca, titulo FROM licenca";	
										if ($licencaAtual == ''){
											echo "<option value='null' selected>Sem licença</option>";
										}
										
									   foreach ($pdo->query($sql) as $row){
										if ($id != 0){
											if ($row['id_licenca'] == $licencaAtual){
												echo "<option value='".$row['id_licenca']."' selected>". $row['titulo'] ."</option>";	
											}else{
												echo "<option value='".$row['id_licenca']."'>". $row['titulo'] ."</option>";													
											}
										}else{
											echo "<option value='".$row['id_licenca']."'>". $row['titulo'] ."</option>";	
										}
									   }
									   Database::disconnect();
									?>
								</select>								
							</div>
						  </div>						  
						  <div class="form-actions">
							  <?php if ($id != 0) {?>
								<button type="submit" class="btn btn-success">  Alterar </button>
							  <?php } else{?>
								<button type="submit" class="btn btn-success">  Criar </button>
							  <?php }?> 
							  <a class="btn" href="lista_usuarios.php">Cancelar</a>
						  </div>
						</form>
					</div>

			</div> <!-- /container -->
		</div>
	</section>

  </body>
</html>