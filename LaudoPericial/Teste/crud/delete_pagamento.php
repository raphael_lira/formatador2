﻿<?php 
	session_start();
	require 'database.php';
	$id = 0;
	
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if($_SESSION['usuarioTipo'] < 1 && $_SESSION['usuarioLicenca'] != $_GET['id']){
		header('Location: lista_licenca.php');
	}


	if ( !empty($_POST)) {
		// keep track post values
		$id = $_POST['id'];
		
		// delete data
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
				
		$sql2 = "DELETE FROM pagamentos WHERE id_pagamentos = $id";		
		$pdo->query($sql2);
		
		Database::disconnect();
		header("Location: lista_licenca.php");
		
	} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Excluir pagamento</title>
    <meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
</head>

<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $_SESSION['usuarioLogin']?></b> <?php if ($_SESSION['usuarioLicenca'] == 1) { ?>| <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> <?php }?> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">

			<div class="container">

				<div class="span10 offset1">
					<div class="row">
						<h3>Excluir licença</h3>
					</div>

					<form class="form-horizontal" action="delete_pagamento.php" method="post">
						<input type="hidden" name="id" value="<?php echo $id;?>"/>
						<p class="alert alert-error" style="text-align: center;">
							Tem certeza que deseja excluir a licença?&nbsp;&nbsp;	A ação será irreversível!<br><br>
							Todos os usuários com essa licença ficarão sem acesso ao sistema.
						</p>
						<div class="form-actions">
							<button type="submit" class="btn btn-danger offset2">Sim</button>
							<a class="btn" href="lista_licenca.php">Não</a>
						</div>
					</form>
				</div>
			</div> <!-- /container -->
		</div>
	</section>
  </body>
</html>