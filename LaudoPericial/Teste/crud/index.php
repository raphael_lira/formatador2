<?php
session_start();
if(!isset($_SESSION['usuarioID'])){
	header('Location: ../../login.php');
}



?>
<!DOCTYPE html>
<head>
    	<title>Documentos</title>
    	<meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
   	
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	
	<script src="js/bootstrap.min.js"></script>
	
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
	<script>
		
		$(document).ready(function(){
			var conexao = new WebSocket('ws://localhost:8080');
			var doc = null;
			conexao.onopen = function(response) {
				conexao.send('{"metodo":"default"}');
			};
			conexao.onmessage = function(response) {
				response = JSON.parse(response.data);
				if(response.metodo == 'abrirDoc'){
					editar(response.status);
				}else if(response.metodo == 'downloadDoc'){
					download(response.status);
				}
			};
			
			$('.btnEditar').on('click',function(){
				var $this = $(this);
				doc = $this.data('id');
				conexao.send('{"metodo":"abrirDoc", "doc":'+doc+'}');
				
			});			
			
			$('.btnDownload').on('click',function(){
				var $this = $(this);
				doc = $this.data('id');
				conexao.send('{"metodo":"downloadDoc", "doc":'+doc+'}');
				
			});
			
			function editar(stsDoc){
				sessionStorage.clear();
				if(stsDoc){
					$.post(
						'update.php',
						{
							'doc' : doc
						},
						function(data){
							//redireciona para a pagina de criacao de pdf
							window.location.assign("../index.php?p=document&doc="+data.doc);
						},
						 'json'
					);
				}else{
					$('#modalErro').modal('show');
				}
			}
			
			function download(stsDoc){
				sessionStorage.clear();
				if(stsDoc){
					$.post(
						'update.php',
						{
							'doc' : doc
						},
						function(data){
							//redireciona para a pagina de criacao de pdf
							window.location.assign("../proc.php");
						},
						 'json'
					);
				}else{
					$('#modalErro').modal('show');
				}
			}
			
		});

	</script>
</head>
<?php
include 'database.php';
$pdo = Database::connect();
 
$sql_user = "SELECT login, adm, id_licenca FROM usuario WHERE id_usuario = {$_SESSION['usuarioID']}";
foreach ($pdo->query($sql_user) as $row){
    $user = $row['login'];
	$_SESSION['usuarioLogin'] = $row['login'];;
	$_SESSION['usuarioTipo'] = $row['adm'];
	$_SESSION['usuarioLicenca'] = $row['id_licenca'];
}

if ($_SESSION['usuarioLicenca'] == ''){
	header('Location: ../sair.php');
}

	// deleta arquivos
	for ($i = 1; $i < 15; $i++ ){
		unlink('capitulos/capitulo'.$i.''.$_SESSION['doc'].'.txt');
	}
	
	//unlink('../docTemp/doc'.$_SESSION['doc'].'.pdf');
  
?>
<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $_SESSION['usuarioLogin']?></b> <?php if ($_SESSION['usuarioLicenca'] == 1) { ?>| <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> <?php }?> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">

			<div class="container offset1" >
					<div class="row">
						<p><h3>Documentos Criados</h3></p>
						<p class="text-left"><a style="color:black;" href="cabecalho.php" target="_blank">Click aqui para alterar o cabeçalho padrão dos PDFs</a></p>
					</div>
					<div class="row">

						<table class="table table-striped table-bordered">
							  <thead>
								<tr>
								  <th>Nome do documento</th>
								  <th>Data da perícia</th>
								  <th>Número do processo</th>
								  <th><a href="create.php" class="btn btn-success">Criar novo documento</a></th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							   	if ($_SESSION['usuarioTipo'] > 1){		
									$sql = " SELECT 	documentos.id_doc,
													documentos.nome_documento,
													documentos.n_processo,
													documentos.id_usuario,
													usuario.id_usuario,
													usuario.id_licenca,
													TO_CHAR(documentos.data_pericia, 'DD/MM/YYYY') AS data_pericia
											FROM documentos, usuario WHERE documentos.id_usuario = usuario.id_usuario 
											ORDER BY data_pericia
									  ";
								
								}elseif ($_SESSION['usuarioTipo'] == 1){
									$sql = " SELECT 	documentos.id_doc,
													documentos.nome_documento,
													documentos.n_processo,
													documentos.id_usuario,
													usuario.id_usuario,
													usuario.id_licenca,
													TO_CHAR(documentos.data_pericia, 'DD/MM/YYYY') AS data_pericia
											FROM documentos, usuario WHERE documentos.id_usuario = usuario.id_usuario and 
											usuario.id_licenca = ". $_SESSION['usuarioLicenca'] ."
											ORDER BY data_pericia
									  ";
								}else{
								   $sql = " SELECT 	id_doc,
													nome_documento,
													n_processo,
													id_usuario,
													TO_CHAR(data_pericia, 'DD/MM/YYYY') AS data_pericia
											FROM documentos WHERE id_usuario = ".$_SESSION['usuarioID']."
											ORDER BY data_pericia
									  ";
								 }
							   foreach ($pdo->query($sql) as $row){
									echo '<tr>';
									echo '<td>'. $row['nome_documento'] . '</td>';
									echo '<td>'. $row['data_pericia'] . '</td>';
									echo '<td>'. $row['n_processo'] . '</td>';
									echo '<td width=350>';
									echo '<a class="btn btn-primary" href="duplicate.php?id='.$row['id_doc'].'">Salvar como</a>';
					                                echo '&nbsp;';
									echo '<a class="btn btnDownload" data-id="'.$row['id_doc'].'">Download</a>';
									echo '&nbsp;';
									echo '<a class="btn btn-success btnEditar" data-id="'.$row['id_doc'].'">Editar</a>';
									echo '&nbsp;';
									echo '<a class="btn btn-danger" href="delete.php?id='.$row['id_doc'].'">Excluir</a>';
									echo '</td>';
									echo '</tr>';
							   }
							   Database::disconnect();
							  ?>
							  </tbody>
						</table>
					</div>
				</div> <!-- /container -->
			</div>
	</section>
</body>
<div id="modalErro" class="modal fade" role="dialog">
  <div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">O arquivo não pode ser aberto</h4>
	  </div>
	  <div class="modal-body">
		<p>Esse arquivo está sendo manipulado por outro usuario e não pode ser aberto!</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>

  </div>
</div>
</html>