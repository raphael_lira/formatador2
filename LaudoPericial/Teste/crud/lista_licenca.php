﻿<!DOCTYPE html>
<head>
    <title>Lista Licenças</title>
    <meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
   	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>	
</head>
<?php
session_start();
$user = $_SESSION['usuarioLogin'] ;
include 'database.php';
$pdo = Database::connect();

?>
<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $user?></b> | <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">

			<div class="container offset1" >
					<div class="row">
						<h3>Licenças Cadastradas</h3>
					</div>
					<div class="row">

						<table class="table table-striped table-bordered">
							  <thead>
								<tr>
								  <th>Nome da licença</th>								  
								  <th><a href="create_licenca.php" class="btn btn-success">Criar nova licença</a></th>
								</tr>
							  </thead>
							  <tbody>
								<?php				   
							   
								   $sql = " SELECT 	*
											FROM licenca
									  ";
									if($_SESSION['usuarioTipo'] <= 1)
										$sql.= " id_licenca = {$_SESSION['usuarioLicenca']} ";
									$tipoAdm = "";
								   foreach ($pdo->query($sql) as $row){
								   
										echo '<tr>';
										echo '<td>'. $row['titulo'] . '</td>';										
										echo '<td width=450>';
										echo '<a class="btn btn-success" href="create_licenca.php?id='.$row['id_licenca'].'">Editar</a>';
										echo '&nbsp;';
									   	if($row['transacao'] == '' || ($row['transacao'] != '' && $row['status'] == 0)){
											echo '<a class="btn btn-primary" href="pagseguro/assinar.php?id='.$row['id_licenca'].'">Pagar(Pagseguro)</a>';
											echo '&nbsp;';
									   	}
										echo '<a class="btn btn-default" href="lista_pagamentos.php?id='.$row['id_licenca'].'">Pagamentos</a>';
										echo '&nbsp;';
										echo '<a class="btn btn-danger" href="delete_licenca.php?id='.$row['id_licenca'].'">Excluir</a>';
										echo '</td>';
										echo '</tr>';
								   }
								   Database::disconnect();
								?>
								  
							  </tbody>
						</table>
						<a class="btn" href="index.php">Voltar</a>
					</div>
				</div>
			</div>
	</section>
</body>
</html>