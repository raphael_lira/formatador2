﻿<?php
session_start();
//echo var_dump($_SESSION);exit;
if(!isset($_SESSION['usuarioID'])){
	header('Location: ../../login.php');
}

if(!isset($_GET['id'])){
	header('Location: lista_licenca.php');
}

if($_SESSION['usuarioTipo'] < 1 && $_SESSION['usuarioLicenca'] != $_GET['id']){
	header('Location: lista_licenca.php');
}


$user = $_SESSION['usuarioLogin'] ;
include 'database.php';
include 'pagseguro/Pagseguro.php';
$pdo = Database::connect();
$pagseguro = new Pagseguro;

$sql = " SELECT * FROM licenca WHERE id_licenca = {$_GET['id']}";

//puxa os dados da licença
foreach ($pdo->query($sql) as $row){
	$tituloAtual = $row['titulo'];
	$valor = $row['valor'];
	$id = $row['id_licenca'];
}

$gateway = array(
	0 => 'Manual',
	1 => 'Pagseguro'
);

?>
<!DOCTYPE html>
<head>
    <title>Lista Licenças</title>
    <meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
   	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>	
</head>

<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $user?></b> | <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">

			<div class="container offset1" >
				<div class="row">
					<h3>Histórico de pagamentos: <?= $tituloAtual ?></h3>
				</div>
				<div class="row text-left">
					<br><br>
				</div>
				<div class="row">

						<table class="table table-striped table-bordered">
							  <thead>
								<tr>
								  <th>Data</th>								  
								  <th>Forma</th>
								  <th>Status</th>
								  <th>Valor</th>
								  <th><?php if($_SESSION['usuarioTipo'] > 1 && $_SESSION['usuarioLicenca'] != $_GET['id']){ ?><a href="create_pagamento.php?licenca=<?=$id?>" class="btn btn-success">Gerar pagamento manual</a><?php } ?></th>
								</tr>
							  </thead>
							  <tbody>
								<?php				   
							   
								   $sql = " SELECT 	
								   				id_pagamentos,
								   				gateway,
												valor,
												status,
												TO_CHAR(data, 'DD/MM/YYYY') AS data
											FROM 
												pagamentos
											WHERE 
												id_licenca = $id
									  ";
									if($_SESSION['usuarioTipo'] <= 1)
										$sql.= " id_licenca = {$_SESSION['usuarioLicenca']} ";
									$tipoAdm = "";
								   foreach ($pdo->query($sql) as $row){
								   
										echo '<tr>';
										echo '<td>'. $row['data'] . '</td>';										
										echo '<td>'. $gateway[$row['gateway']] . '</td>';										
										echo '<td>'. $pagseguro->callback[$row['status']] . '</td>';										
										echo '<td>'. formatarDinheiro($row['valor']) . '</td>';										
										echo '<td>';
									   	echo '<a class="btn btn-success" href="create_pagamento.php?id='.$row['id_pagamentos'].'">Editar</a>';
										echo '&nbsp;';
										echo '<a class="btn btn-danger" href="delete_pagamento.php?id='.$row['id_pagamentos'].'">Excluir</a>';
										echo '</td>';
										echo '</tr>';
								   }
								   Database::disconnect();
								?>
								  
							  </tbody>
						</table>
						<a class="btn" href="lista_licenca.php">Voltar</a>
					</div>
				</div>
			</div>
	</section>
</body>
</html>