﻿<!DOCTYPE html>
<head>
    <title>Lista Usuários</title>
    <meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
   	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>	
</head>
<?php
session_start();
$user = $_SESSION['usuarioLogin'] ;
include 'database.php';
$pdo = Database::connect();

?>
<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $user ?></b> | <a href="../crud/lista_usuarios.php">Usuários</a> | <a href="../crud/lista_licenca.php">Licenças</a> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">

			<div class="container offset1" >
					<div class="row">
						<h3>Usuários Cadastrados</h3>
					</div>
					<div class="row">

						<table class="table table-striped table-bordered">
							  <thead>
								<tr>
								  <th>Nome do usuário</th>
								  <th>Tipo de usuário</th>
								  <th>Licença</th>
								  <th><a href="create_user.php" class="btn btn-success">Criar novo usuário</a></th>
								</tr>
							  </thead>
							  <tbody>
								<?php				   
							   
								   $sql = " SELECT 	usuario.id_usuario,
													usuario.login,
													usuario.adm,
													usuario.id_licenca,
													licenca.titulo													
											FROM usuario LEFT JOIN licenca ON usuario.id_licenca = licenca.id_licenca AND 
											usuario.adm <= {$_SESSION['usuarioTipo']}
									  ";
									  
									$tipoAdm = "";
								   foreach ($pdo->query($sql) as $row){
																
										if ($row['adm'] == 3){ 
											$tipoAdm =  "Super Administrador";
										}elseif ($row['adm'] == 2){ 
											$tipoAdm =  "Financeiro";
										}elseif ($row['adm'] == 1){ 
											$tipoAdm =  "Administrador";
										}else{ 
											$tipoAdm = "Comum";
										}
								   
										echo '<tr>';
										echo '<td>'. $row['login'] . '</td>';
										echo '<td>'. $tipoAdm . '</td>';
										echo '<td>'. $row['titulo'] . '</td>';
										echo '<td width=350>';
										echo '<a class="btn btn-success" href="create_user.php?id='.$row['id_usuario'].'">Editar</a>';
										echo '&nbsp;';
										echo '<a class="btn btn-danger" href="delete_user.php?id='.$row['id_usuario'].'">Excluir</a>';
										echo '</td>';
										echo '</tr>';
								   }
								   Database::disconnect();
								?>
								  
							  </tbody>
						</table>
						<a class="btn" href="index.php">Voltar</a>
					</div>
				</div>
			</div>
	</section>
</body>
</html>