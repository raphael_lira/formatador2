<?php
    session_start();
    include('../mpdf60/mpdf.php');
    require 'database.php';
    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql_doc = "SELECT nome_documento, n_processo, reclamante, reclamada, data_pericia FROM documentos WHERE id_doc = {$_GET['doc']}";
 //die($sql_doc);
    foreach ($pdo->query($sql_doc) as $row){
	$processo = $row['n_processo'];
	$reclamante = $row['reclamante'];
	$reclamada = $row['reclamada'];
	$data_pericia = $row['data_pericia'];
	$nome_documento = $row['nome_documento'];
    }

    $paragrafo = "";
    $y = 0;
    for($x=1;$x<=14;$x++) {
        if ($_SESSION['tc'.$x] != '') {
            $y++;
            $paragrafo .= "<table class=\"table\" border=\"1\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 800px;\">
                                <tbody>
                                    <tr>
                                        <td style=\"text-align: center;\">
                                            <span style=\"font-family: verdana, sans-serif; font-size: large; background-image: initial; background-attachment: initial; background-color: silver; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">
                                              ".$y.'. '.addslashes($_SESSION['tc'.$x])."
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                           </table>
                          ";
        } else{
            $paragrafo .= '';
        }
        if ($_SESSION['c'.$x] != '') {
            $paragrafo.= $_SESSION['c'.$x]."<br/><br/>";
        } else{
            $paragrafo .= '';
        }
    }

    //$paragrafo = ;

//    $mpdf=new mPDF();
$mpdf = new mPDF(   '',    // mode - default ''
                    '',    // format - A4, for example, default ''
                    0,     // font size - default 0
                    '',    // default font family
                    15,    // margin_left
                    15,    // margin right
                    71,     // margin top
                    18,    // margin bottom
                    9,     // margin header
                    0,     // margin footer
                    'L' );  // L - landscape, P - portrait
    $mpdf->SetDisplayMode('fullpage');
    //$mpdf->showImageErrors = true;

    $cabecalho = "
    <BODY LANG=\"pt-BR\" TEXT=\"#000000\" LINK=\"#0000ff\" DIR=\"LTR\">
    <DIV TYPE=HEADER>
        <P ALIGN=LEFT STYLE=\"margin-right: -0.22in\">

        </P>
        <P ALIGN=LEFT><FONT COLOR=\"#808080\">

                        </FONT>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT SIZE=2 STYLE=\"font-size: 9pt\"><I>Luiz
        Carlos  Moreira</I></FONT><FONT FACE=\"Zurich LtCn BT, serif\"><FONT SIZE=2 STYLE=\"font-size: 9pt; margin-top: 0px;\"><I>
        </I></FONT></FONT></FONT>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\"><BR>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich LtCn BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><I>Especialista
        em Medicina do Trabalho ANAMT / AMB</I></FONT></FONT></FONT></P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich LtCn BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><I>Ergonomista
        Certificado ABERGO
                       </I></FONT></FONT></FONT>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich Lt BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><I>Profissional
        Certificado PCMSO-G (Galvanoplastia) /FUNDACENTRO</I></FONT></FONT></FONT></P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich Lt BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><SPAN LANG=\"en-US\"><I>Membro
        do ICOH &ndash; International Commission on Occupational Health</I></SPAN></FONT></FONT></FONT></P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich Lt BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><I>Associado
        &agrave; Sociedade Paulista de Per&iacute;cias M&eacute;dicas </I></FONT></FONT></FONT>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich Lt BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><I>Associado
        &agrave; Sociedade Brasileira de Per&iacute;cias M&eacute;dicas</I></FONT></FONT></FONT></P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich Lt BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><I>Especialista
        em Per&iacute;cias M&eacute;dicas pela Associa&ccedil;&atilde;o
        Brasileira de Medicina Legal e Per&iacute;cias M&eacute;dicas &ndash;
        ABMLPM / AMB</I></FONT></FONT></FONT></P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">          <FONT SIZE=3><FONT FACE=\"Zurich Lt BT, serif\"><FONT SIZE=1 STYLE=\"font-size: 7pt\"><I>M&eacute;dico
        Perito da Justi&ccedil;a do Trabalho de Campinas</I></FONT></FONT></FONT></P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\"><BR>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\"><BR>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\"><BR>
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 3px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\">
        </P>
        <P ALIGN=LEFT STYLE=\"margin-bottom: 30px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\"><BR>
        </P>
	<hr style=\"color:#FF4848; margin-top:-85px;\">
	<hr style=\"color:#FFB6B6; margin-top:-156px; margin-bottom: 30px;\">

    </DIV>
    </BODY>";

    $capa = "<div style=\"font-family: verdana, sans-serif; font-size: 18; text-align: left; font-weight: bold; margin-left:15px;\">
	     	<div style=\"\">
		    <b>EXCELENTÍSSIMO SENHOR DOUTOR JUIZ DO TRABALHO DA 3a VARA DA  JUSTIÇA DO TRABALHO  DE CAMPINAS - SP</b>
	     	</div>
	     	<div style=\"line-height: 50px; margin-top:50px;\">
	     	    <div>
		    	Processo: $processo 
	     	    </div>
	     	    <div>
		    	Reclamante: ".mb_strtoupper($reclamante)."
	     	    </div>
	     	    <div>
		    	Reclamada: ".mb_strtoupper($reclamada)."
	     	    </div>
		</div>
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 14; text-align:justify; margin-top:50px; margin-left:15px; line-height: 25px;\">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Luiz Carlos Moreira, na qualidade de Perito Médico nomeado por V. Exa., apresento o
		Laudo Médico Pericial, referente ao processo supra mencionado. Solicito a gentileza da
		liberação dos honorários prévios periciais existentes, bem como o arbitramento dos honorários
		periciais em definitivo, independente de acordo entre as partes, cujo valor estimado total é de
		R$ 6.780,00 (Seis mil, setecentos e oitenta) reais, corrigidos à época do pagamento,
		considerando os tempos técnicos destinados à análise detalhada dos autos, à avaliação
		médica pericial do Reclamante, à confecção minuciosa do Laudo Pericial e considerando ainda
		as despesas de deslocamento, de material e de equipamentos utilizados. Solicito ainda, que
		seja designado pagamento de honorários periciais em caso de acordo antecipado entre as
		partes; que em caso de sucumbência do(a) Reclamante, o pagamento dos honorários periciais
		seja requisitado nos termos do Provimento GP-GR 01/2009.
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 16; text-align:center; margin-top:35px;\">
		Nestes termos, pede deferimento.
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 14; text-align:center; margin-top:35px; font-weight: bold;\">
		Campinas, 16 de Dezembro de 2014.
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 14; text-align:center; margin-top:35px; font-weight: bold;\">
		Luiz Carlos Moreira
	    </div>
	     ";

    $stylesheet = "table{
                      width: 100%;
                      text-align:center;
                      border: 2px solid black;
                   }
    ";

    $footer = "<table name='footer' style='border:0px;' width=\"1000\">
               <tr>
                 <td style='font-size: 18px; padding-bottom: 20px;' align=\"right\">{PAGENO}</td>
               </tr>
             </table>";

    $i = 0;
    $aux = 1;
    for($i=1; $i<=14; $i++){
	
        //Indice e o titulo deste
	if ($_SESSION['tc'.$i] != '') {
            $indice .= "<div style=\"font-size: 14pt; font-family: Verdana, sans-serif;\">";
                                
	    $indice .=     "<div style=\"display: block;\">
				<div style=\"float:left; text-align:left; width:45px;\">
				    $aux.
                           	</div>
				<div style=\"float:left; text-align:left\">
				    {$_SESSION['tc'.$i]}
                           	</div>
			    </div>
				<div style=\" margin-top:-25px; text-align: right;\">
                                    {$_SESSION['nc'.$i]}
                            	</div><br>
                        </div>";
	    $aux++;
	}
	
    }

    $mpdf->SetHTMLHeader($cabecalho);
    $mpdf->SetFooter($footer);
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->WriteHTML($capa);
    $mpdf->AddPage();
    $mpdf->WriteHTML($indice);
    $mpdf->AddPage();
    $mpdf->WriteHTML($paragrafo);


    //$mpdf->Output();
    $mpdf->Output($nome_documento, D);
    exit;
?>