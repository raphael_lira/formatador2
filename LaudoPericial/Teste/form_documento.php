<form id="documento" name="documento" method="post" action="form_documento.php">
    <?php

	//session_start();
				
	//$document = isset($_SESSION['document']) ? stripslashes(addslashes($_SESSION['document'])) : '';
    //$data_pericia = isset($_SESSION['data_pericia']) ? stripslashes(addslashes($_SESSION['data_pericia'])) : '';
    //$estado = isset($_SESSION['estado']) ? stripslashes(addslashes($_SESSION['estado'])) : '';
    //$cidade = isset($_SESSION['cidade']) ? stripslashes(addslashes($_SESSION['cidade'])) : '';
	//$cabecalho = isset($_SESSION['cabecalho']) ? stripslashes(addslashes($_SESSION['cabecalho'])) : '';
		
	echo '<script>';
	echo 'var documento = "'. $_SESSION['document'] .'";';
	echo 'var estado = "'. $_SESSION['estado'] .'";';
	echo 'var dataPericia = "'. $_SESSION['data_pericia'] .'";';
	echo 'var cidade = "'. $_SESSION['cidade'] .'";';
	echo 'var cabecalho = "'. $_SESSION['cabecalho'] .'";';
	echo 'if (documento != ""){ sessionStorage.setItem("documento", "'. $_SESSION['document'] .'"); }';
	echo 'if (dataPericia != ""){ sessionStorage.setItem("dataPericia", "'. $_SESSION['data_pericia'] .'"); }';
	echo 'if (estado != ""){ sessionStorage.setItem("estado", "'. $_SESSION['estado'] .'"); }';
	echo 'if (cidade != ""){ sessionStorage.setItem("cidade", "'. $_SESSION['cidade'] .'"); }';
	echo 'if (cabecalho != ""){ sessionStorage.setItem("cabecalho", "'. $_SESSION['cabecalho'] .'"); }';
	//echo 'console.log(sessionStorage.getItem("documento"));';
	//echo 'console.log(sessionStorage.getItem("dataPericia"));';
	//echo 'console.log(sessionStorage.getItem("estado"));';
	//echo 'console.log(sessionStorage.getItem("cidade"));';
	//echo 'console.log(sessionStorage.getItem("cabecalho"));';
	echo '</script>';
	?>
	
	<script>
	
		$(document).ready(function(){			
			$("#document").text(sessionStorage.getItem("documento"));
			$("#cidade").text(sessionStorage.getItem("cidade"));
			$("#data_pericia").val(sessionStorage.getItem("dataPericia"));
			$("#cabecalho").text(sessionStorage.getItem("cabecalho"));			
			$("#estado > option").each(function() {			
				if (this.value == sessionStorage.getItem("estado")){					
					this.selected = 'selected';
				}										
			});
		});
		
		function Altera_Sessao(id){		
			var texto = $("#"+id).val();
					
			if (id == 'document'){
				sessionStorage.setItem("documento", texto);
				console.log(sessionStorage.getItem("documento"));
			}		
			
			if (id == 'data_pericia'){
				sessionStorage.setItem("dataPericia", texto);
				console.log(sessionStorage.getItem("dataPericia"));
			}	
			
			if (id == 'cidade'){
				sessionStorage.setItem("cidade", texto);
				console.log(sessionStorage.getItem("cidade"));
			}	
			
			if (id == 'cabecalho'){
				sessionStorage.setItem("cabecalho", texto);
				console.log(sessionStorage.getItem("cabecalho"));
			}		

			if (id == 'estado'){
				sessionStorage.setItem("estado", texto);
				console.log(sessionStorage.getItem("estado"));
			}				
		}
		
		function voltar(){
			document.getElementById('btnSalvou').value = 't';
			window.location.href='crud/index.php'; 
		}
	
	</script>

   	<div class="titulo" style = "display: block;">
		<div style="margin: 48px auto 0 auto;">
		
			<div id="result"></div>
			
			<span style="font-size: 18px;">Nome do documento:</span>
			<textarea style="padding: 0px; overflow:hidden; width: 520px; height: 30px; font-size: 18px;" id="document" name="document" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1); Altera_Sessao('document');"></textarea><br>
			<span style="font-size: 18px; ">Data da perícia:</span>
			<input type="text" style="padding: 0px; overflow:hidden; margin-right: 419px; width: 150px; height: 30px; font-size: 18px;" id="data_pericia" name="data_pericia" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1); Altera_Sessao('data_pericia');" onchange="Altera_Sessao('data_pericia')" value=""><br>
			<div style="display: flex; margin: auto;">
                <div style="width: 64.4%;">
                    <span style="font-size: 18px;">Estado:</span>
                    <select name="estado" id="estado" onchange="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1); Altera_Sessao('estado')" style="height:30px;">
                        <option value="" selected>Selecione o Estado</option>
                        <option value="ac" >Acre</option>
                        <option value="al" >Alagoas</option>
                        <option value="am" >Amazonas</option>
                        <option value="ap" >Amapá</option>
                        <option value="ba" >Bahia</option>
                        <option value="ce" >Ceará</option>
                        <option value="df" >Distrito Federal</option>
                        <option value="es" >Espírito Santo</option>
                        <option value="go" >Goiás</option>
                        <option value="ma" >Maranhão</option>
                        <option value="mt" >Mato Grosso</option>
                        <option value="ms" >Mato Grosso do Sul</option>
                        <option value="mg" >Minas Gerais</option>
                        <option value="pa" >Pará</option>
                        <option value="pb" >Paraíba</option>
                        <option value="pr" >Paraná</option>
                        <option value="pe" >Pernambuco</option>
                        <option value="pi" >Piauí</option>
                        <option value="rj" >Rio de Janeiro</option>
                        <option value="rn" >Rio Grande do Norte</option>
                        <option value="ro" >Rondônia</option>
                        <option value="rs" >Rio Grande do Sul</option>
                        <option value="rr" >Roraima</option>
                        <option value="sc" >Santa Catarina</option>
                        <option value="sp" >São Paulo</option>
                        <option value="se" >Sergipe</option>
                        <option value="to" >Tocantins</option>
                    </select>
                </div>
                <div style="width: 60%;">
                    <span style="margin-left:-116px; font-size: 18px;">Cidade:</span>
                    <textarea style="margin-left:2px; width: 53.6%; height: 30px; padding: 0px;"  id="cidade" name="cidade" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1); Altera_Sessao('cidade');"></textarea>
                </div>
            </div><br>
			<span style="font-size: 18px;">Cabeçalho:</span>
			<textarea style="padding: 0px; overflow:hidden; width: 604px; height: 30px; font-size: 18px;" id="cabecalho" name="cabecalho" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1); Altera_Sessao('cabecalho');"></textarea><br>
			
		</div><br>
	</div>

    <p>
		<input type="button" value="Gerar PDF" onclick="Envia_Form('#documento', '', 'proc.php', 0, '_blank'); document.getElementById('btnSalvou').value = 't';" />
		<input type="button" value="Voltar aos documentos" onclick="voltar()" />
		
	</p>

</form>