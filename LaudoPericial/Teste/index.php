<?php
	session_start();

	if(!isset($_SESSION['usuarioID'])){
		header('Location: ../login.php');
	}
	
	require 'crud/database.php';
	$pdo = Database::connect();

	$capitulo = 0;
	if (isset($_GET['p']) && !empty($_GET['p'])){
		$p = $_GET['p'];
		$capitulo = substr($p, 1);
		if (!(isset($_SESSION[$p]))){
			$_SESSION[$p] = "";
		}
		if (!(isset($_SESSION["t$p"]))){
			$_SESSION["t$p"] = "";
			$_SESSION["n$p"] = "";
		}
	} else {
		$p = 0;
	}

	$doc = 0;
	if (isset($_GET['doc']) && !empty($_GET['doc'])){
		$doc = $_GET['doc'];
		if ($_SESSION['usuarioTipo'] > 1){
			$sql = " SELECT count(*)						
					FROM documentos, usuario WHERE documentos.id_doc = $doc AND documentos.id_usuario = usuario.id_usuario ";
		}elseif ($_SESSION['usuarioTipo'] == 1){
			$sql = " SELECT count(*)						
					FROM documentos, usuario WHERE documentos.id_doc = $doc AND documentos.id_usuario = usuario.id_usuario and 
					usuario.id_licenca = ". $_SESSION['usuarioLicenca'] ." ";
		}else{
		   $sql = "SELECT count(*) FROM documentos WHERE id_doc = $doc AND id_usuario = {$_SESSION['usuarioID']}";
		 }
		//$sql = "SELECT count(*) FROM documentos WHERE id_doc = $doc AND id_usuario = {$_SESSION['usuarioID']}";
		$n_doc = $pdo->query($sql)->fetchColumn();
		if($n_doc <= 0){
			$doc = 0;
		}
		
		
	}else{
		header('Location: crud/');
	}


	$i = 1;

	$texto = trim(isset($_SESSION["$p"]) ? $_SESSION["$p"] : '');
	

?>


<!DOCTYPE html>
<html>
	<head>
		
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="ckeditor/ckeditor.js"></script>
		<script src="ckeditor/samples/js/sample.js"></script>
		<script src='jquery.autosize.js'></script>
		
		<script>
			function Envia_Form(id_forn, valor, arquivo, chooser, target){  //valor do chooser: 0 = faz submit com refresh na pagina |  1 = sem refresh na pagina
				$('.alert').hide(100);
				$("#valor_paragrafo").val(valor);
				if(chooser == 0){
					$(id_forn).attr("action",arquivo);
					if(target != ''){
						$(id_forn).prop("target", target);
					}
					$(id_forn).submit();
				} else if(chooser == 1){
					$.post(
						arquivo, 		// arquivo para configurar retorno do ajax
						$(id_forn).serialize(),
						function( data ,sts ){						
							if(sts == "success"){
								$('.alertSalvo').show(300);
							}else{
								$('.alertErro').show(300);
							}						
						}
					);
				}
			}

			$(function() {
   				$('textarea').autosize({append:false});
				$('#editor').autosize({append:false});
			});
			
			$(document).ready(function() {
				
				var conexao = new WebSocket('ws://localhost:8080');
				conexao.onopen = function(response) {
    				conexao.send('{"metodo":"setDoc", "doc":<?php echo $doc ?>}');
				};
				
				
				
					$.datepicker.regional['pt'] = {
                		closeText: 'Fechar',
                		prevText: '&#x3c;Anterior',
                		nextText: 'Seguinte',
               		 	currentText: 'Hoje',
               		 	monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        	        	monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        	        	dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
        	        	dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        	        	dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        	        	weekHeader: 'Sem',
        	        	dateFormat: 'dd/mm/yy',
        	        	firstDay: 0,
        	        	isRTL: false,
        	        	showMonthAfterYear: false,
       		         	yearSuffix: ''};
	            		$.datepicker.setDefaults($.datepicker.regional['pt']);
				$("#data_pericia").datepicker({
					onSelect: Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1)		
				});
				
				$('input[type="hidden"][name="btnSalvou"]').val("f");				
			});
			
			function myFunction() {
				console.log("Dentro da função: " + $('input[type="hidden"][name="btnSalvou"]').val());
				if (document.getElementById('btnSalvou').value != 't'){
					$("#ativaModal").click();
					return "Write something clever here...";
				}
				document.getElementById('btnSalvou').value = 'f';
			}
			
		</script>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link href="editor.css" type="text/css" rel="stylesheet"/>
		<link rel="stylesheet" href="style.css" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
		<title>Gerador</title>
	</head>

	<body onbeforeunload="return myFunction()">
		<section id="content">
			<div id="header">Logado como <b><?php echo $_SESSION['usuarioLogin']?></b> <?php if ($_SESSION['usuarioLicenca'] == 1) { ?>| <a href="crud/lista_usuarios.php">Usuários</a> | <a href="crud/lista_licenca.php">Licenças</a> <?php }?> | <a href="sair.php">Sair</a> </div>
			<input type="hidden" name="btnSalvou" id="btnSalvou" value="f"  />
			
			<div class="alert alert-warning">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Lembre-se!</strong> Não feche o navegador sem antes sair da tela de edição. Isso pode corromper seu documento!
			</div>
			
			<div id="nav">
				<div class="navlink">
					<ul>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=document&doc=<?=$doc?>"  <?php if($p === 'document')  {echo 'class="ativo"';} ?>>Documento</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=cab&doc=<?=$doc?>"  <?php if($p === 'cab')  {echo 'class="ativo"';} ?>>Capa</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c1&doc=<?=$doc?>"  <?php if($p === 'c1')  {echo 'class="ativo"';} ?>>Capítulo 1</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c2&doc=<?=$doc?>"  <?php if($p === 'c2')  {echo 'class="ativo"';} ?>>Capítulo 2</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c3&doc=<?=$doc?>"  <?php if($p === 'c3')  {echo 'class="ativo"';} ?>>Capítulo 3</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c4&doc=<?=$doc?>"  <?php if($p === 'c4')  {echo 'class="ativo"';} ?>>Capítulo 4</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c5&doc=<?=$doc?>"  <?php if($p === 'c5')  {echo 'class="ativo"';} ?>>Capítulo 5</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c6&doc=<?=$doc?>"  <?php if($p === 'c6')  {echo 'class="ativo"';} ?>>Capítulo 6</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c7&doc=<?=$doc?>"  <?php if($p === 'c7')  {echo 'class="ativo"';} ?>>Capítulo 7</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c8&doc=<?=$doc?>"  <?php if($p === 'c8')  {echo 'class="ativo"';} ?>>Capítulo 8</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c9&doc=<?=$doc?>"  <?php if($p === 'c9')  {echo 'class="ativo"';} ?>>Capítulo 9</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c10&doc=<?=$doc?>" <?php if($p === 'c10') {echo 'class="ativo"';} ?>>Capítulo 10</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c11&doc=<?=$doc?>" <?php if($p === 'c11') {echo 'class="ativo"';} ?>>Capítulo 11</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c12&doc=<?=$doc?>" <?php if($p === 'c12') {echo 'class="ativo"';} ?>>Capítulo 12</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c13&doc=<?=$doc?>" <?php if($p === 'c13') {echo 'class="ativo"';} ?>>Capítulo 13</a></li>
						<li><a onclick="document.getElementById('btnSalvou').value = 't';" href="?p=c14&doc=<?=$doc?>" <?php if($p === 'c14') {echo 'class="ativo"';} ?>>Capítulo 14</a></li>
					</ul>
				</div>
				<?php
					
					if($p == 'cab'){
						include_once "form_cabecalho.php";
					} else if($p == 'document'){
						include_once "form_documento.php";
					} else{
						include_once "form_capitulos.php";
					}
				?>




			</div>
		</section>
		
		<button type="button" style="display: none;" id="ativaModal" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
		
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Lembre-se!</h4>
			  </div>
			  <div class="modal-body">
				<p>Não feche o navegador sem antes sair da tela de edição. Isso pode corromper seu documento!</p>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
			  </div>
			</div>

		  </div>
		</div>
		
		<button type="button" style="display: none;" id="ativaDownload" class="btn btn-info btn-lg" data-toggle="modal" data-target="#download">Open Modal</button>
		<div id="download" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Download!</h4>
			  </div>
			  <div class="modal-body">
				<p>Foi gerado 1 arquivo PDF!</p>
				<?php 
					echo '<a class="btn btn-primary" href="docTemp/doc'.$_SESSION['doc'].'.pdf" download>Download doc'. $_SESSION['doc'].'.pdf</a>';
				?>				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
			  </div>
			</div>

		  </div>
		</div>

	</body>
	<section id="" class="container-fluid ">
		<div class="row col-md-8">
			<div class="alert alerta-gerais alert-success alertSalvo text-center" style="display:none;" role="alert">
				Salvo com sucesso.
			</div>
			<div class="alert alerta-gerais alert-danger alertErro text-center" style="display:none;" role="alert">
				Erro: Falha ao salvar.
			</div>
		</div>
	</section>
</html>