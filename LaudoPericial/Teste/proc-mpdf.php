<?php
    session_start();
    include('mpdf60/mpdf.php');
    require 'crud/database.php';
   
	if (!function_exists('mb_ucfirst')) {
        function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
            $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
            $str_end = "";
            if ($lower_str_end) {
                $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
            }
            else {
                $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
            }
            $str = $first_letter . $str_end;
            return $str;
        }
    }

    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $doc = isset($_SESSION['doc']) ? $_SESSION['doc'] : 0;
    $sql_doc = "SELECT 
			n_processo, 
			reclamante, 
			reclamada, 
			data_pericia,
			instancia_vara,
			descricao_laudo,
			cidade,
			estado
		FROM documentos 
		WHERE id_doc = $doc";
//    die($sql_doc);
    foreach ($pdo->query($sql_doc) as $row){
	$processo = stripslashes(addslashes($row['n_processo']));
	$reclamante = stripslashes(addslashes($row['reclamante']));
	$reclamada = stripslashes(addslashes($row['reclamada']));
	$data_pericia = stripslashes(addslashes($row['data_pericia']));
	$instancia_vara = stripslashes(addslashes($row['instancia_vara']));
	$descricao_laudo = stripslashes(addslashes($row['descricao_laudo']));
	$cidade = stripslashes(addslashes($row['cidade']));
	$estado = stripslashes(addslashes($row['estado']));
    }
   
    function datar($data){
		  list($ano, $mes_n, $dia) = explode('-', $data);		  
		  $mes[1] ='Janeiro';
		  $mes[2] ='Fevereiro';
		  $mes[3] ='Março';
		  $mes[4] ='Abril';
		  $mes[5] ='Maio';
		  $mes[6] ='Junho';
		  $mes[7] ='Julho';
		  $mes[8] ='Agosto';
		  $mes[9] ='Setembro';
		  $mes[10]='Outubro';
		  $mes[11]='Novembro';
		  $mes[12]='Dezembro';
		  
		  return ($dia.' de '.$mes[$mes_n].' de '.$ano);
    }
    
    
    
    $paragrafo = "";
    $y = 0;
    for($x=1;$x<=14;$x++) {
        if ($_SESSION['tc'.$x] != '') {
            $y++;
            $paragrafo .= "<table class=\"table\" border=\"1\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 800px;\">
                                <tbody>
                                    <tr>
                                        <td style=\"text-align: center;\">
                                            <span style=\"font-family: verdana, sans-serif; font-size: large; background-image: initial; background-attachment: initial; background-color: silver; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">
                                              ".$y.'. '.addslashes($_SESSION['tc'.$x])."
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                           </table>
                          ";
        } else{
            $paragrafo .= '';
        }
        if ($_SESSION['c'.$x] != '') {
            $paragrafo.= $_SESSION['c'.$x]."<br/><br/>";
        } else{
            $paragrafo .= '';
        }
    }

    //$paragrafo = ;

//    $mpdf=new mPDF();
$mpdf = new mPDF(   '',    // mode - default ''
                    '',    // format - A4, for example, default ''
                    0,     // font size - default 0
                    '',    // default font family
                    15,    // margin_left
                    15,    // margin right
                    71,     // margin top
                    18,    // margin bottom
                    9,     // margin header
                    0,     // margin footer
                    'L' );  // L - landscape, P - portrait
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->showImageErrors = true;

    $cabecalho = "
<BODY LANG=\"pt-BR\" TEXT=\"\"#000000\" LINK=\"#0000ff\" DIR=\"LTR\">
<DIV TYPE=HEADER>
    <P ALIGN=LEFT STYLE=\"margin-right: -0.22in\">

    </P>
    <P ALIGN=LEFT><FONT COLOR=\"\"#808080\">

        </FONT>
    </P>
    <P ALIGN=LEFT class=\"texto\" class=\"texto\">
        <FONT SIZE=3>
            <FONT SIZE=2 STYLE=\"font-size: 9pt\">
                <I>Luiz  Carlos  Moreira</I>
            </FONT>
            <FONT FACE=\"Zurich LtCn BT, serif\">
                <FONT SIZE=2 STYLE=\"font-size: 9pt; margin-top: 0px;\">
                    <I></I>
                </FONT>
            </FONT>
        </FONT>
    </P>
    <P ALIGN=LEFT class=\"texto\"><BR>
    </P>
    <P ALIGN=LEFT class=\"texto\">
        <FONT SIZE=3>
            <FONT FACE=\"Zurich LtCn BT, serif\">
                <FONT SIZE=1 STYLE=\"font-size: 7pt\">
                    <I>
			Especialista em Medicina do Trabalho ANAMT / AMB<br>
			Ergonomista Certificado ABERGO	<br>
			Profissional Certificado PCMSO-G (Galvanoplastia) /FUNDACENTRO<br>
			Membro do ICOH &ndash; International Commission on Occupational Health<br>
			Associado &agrave; Sociedade Paulista de Per&iacute;cias M&eacute;dicas<br>
			Associado &agrave; Sociedade Brasileira de Per&iacute;cias M&eacute;dicas <br>
			Especialista em Per&iacute;cias M&eacute;dicas pela Associa&ccedil;&atilde;o Brasileira de Medicina Legal e Per&iacute;cias M&eacute;dicas &ndash; ABMLPM / AMB<br>
			M&eacute;dico Perito da Justi&ccedil;a do Trabalho de Campinas
		    		</I>
                </FONT>
            </FONT>
        </FONT>
    </P>

    <P ALIGN=LEFT class=\"texto\"><BR>
    </P>
    <P ALIGN=LEFT class=\"texto\"><BR>
    </P>
    <P ALIGN=LEFT class=\"texto\"><BR>
    </P>
    <P ALIGN=LEFT class=\"texto\">
    </P>
    <P ALIGN=LEFT STYLE=\"margin-bottom: 30px; margin-top: 3px; direction: ltr; color: #000000; text-align: justify; font-style: italic;\"><BR>
    </P>
    <hr style=\"color:#FF4848; margin-top:-85px;\">
    <hr style=\"color:#FFB6B6; margin-top:-156px; margin-bottom: 30px;\">

</DIV>
</BODY>";

    $capa = "<div style=\"font-family: verdana, sans-serif; font-size: 18; text-align: left; font-weight: bold; margin-left:15px;\">
	     	<div style=\"\">
		    <b>$instancia_vara</b>
	     	</div>
	     	<div style=\"line-height: 50px; margin-top:50px;\">
	     	    <div>
		    	Processo: $processo 
	     	    </div>
	     	    <div>
		    	Reclamante: ".mb_strtoupper($reclamante)."
	     	    </div>
	     	    <div>
		    	Reclamada: ".mb_strtoupper($reclamada)."
	     	    </div>
		</div>
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 14; text-align:justify; margin-top:50px; margin-left:15px; line-height: 25px;\">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$descricao_laudo	    
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 16; text-align:center; margin-top:35px;\">
		Nestes termos, pede deferimento.
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 14; text-align:center; margin-top:35px; font-weight: bold;\">
		".mb_ucfirst($cidade).", ".datar($data_pericia)."
	    </div>
	    <div style=\"font-family: verdana, sans-serif; font-size: 14; text-align:center; margin-top:35px; font-weight: bold;\">
		Luiz Carlos Moreira
	    </div>
	     ";

    $stylesheet = "table{
                      width: 100%;
                      text-align:center;
                      border: 2px solid black;
                   }
		.texto {
       			font-family:  Zurich LtCn BT, serif;
        		margin-bottom: 3px;
		        margin-top: 3px;
		        direction: ltr;
		        color: #000000;
		        text-align: justify;
		        font-style: italic;
		}
    ";

    $footer = "<table name='footer' style='border:0px;' width=\"1000\">
               <tr>
                 <td style='font-size: 18px; padding-bottom: 20px;' align=\"right\">{PAGENO}</td>
               </tr>
             </table>";

    $i = 0;
    $aux = 1;
    $indice .= "<table class=\"table\" border=\"1\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 800px;\">
                                <tbody>
                                    <tr>
                                        <td style=\"text-align: center;\">
                                            <span style=\"font-family: verdana, sans-serif; font-size: 24px; background-image: initial; background-attachment: initial; background-color: silver; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">
                                              \"LAUDO MÉDICO PERICIAL\"
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                           </table><br><br>
		<div style=\"margin-left: 300px; width: 65px;\">
		    <div style=\"font-style: italic; text-align: center; border-bottom: 2px solid currentColor; padding-bottom: -5px; font-size:20px; font-family: verdana, sans-serif;\">
			ÍNDICE
		    </div>
		</div><br>
";
    for($i=1; $i<=14; $i++){
	
        //Indice e o titulo deste
	if ($_SESSION['tc'.$i] != '') {
            $indice .= "<div style=\"font-size: 14pt; font-family: Verdana, sans-serif;\">";
                                
	    $indice .=     "<div style=\"display: block;\">
				<div style=\"float:left; text-align:left; width:45px;\">
				    $aux.
                           	</div>
				<div style=\"float:left; text-align:left\">
				    {$_SESSION['tc'.$i]}
                           	</div>
			    </div>
				<div style=\" margin-top:-25px; text-align: right;\">
                                    {$_SESSION['nc'.$i]}
                            	</div><br>
                        </div>";
	    $aux++;
	}
	
    }

    $mpdf->SetHTMLHeader($cabecalho);
    $mpdf->SetFooter($footer);
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->WriteHTML($capa);
    $mpdf->AddPage();
    $mpdf->WriteHTML($indice);
    $mpdf->AddPage();
    $mpdf->WriteHTML($paragrafo);


    $mpdf->Output();
    exit;
?>