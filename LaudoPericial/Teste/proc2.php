<?php
session_start();
// Include the main TCPDF library (search for installation path).
define('K_PATH_IMAGES', '');
require_once('TCPDF/tcpdf.php');
require 'crud/database.php';
   
class MyTCPDF extends TCPDF {

    var $htmlHeader;
	var $linestyle = array('color' => array(255, 0, 0));
    public function SetHtmlHeader($htmlHeader) {
        $this->htmlHeader = $htmlHeader;
    }

    public function Header() {
        $this->writeHTMLCell($w = 0, 
							 $h = 0, 
							 $x = '',
							 $y = '',
            				 $this->htmlHeader,
							 $border = 0, $ln = 1,
							 $fill = 0,
            	 			 $reseth = true, 
							 $align = 'top',
							 $autopadding = true);
		$this->Line(17, 60, 185, 60, $this->linestyle);

    }
	
	public function Footer() {
        $this->SetY(-17);
		$this->SetX(-10);
        // Set font
        $this->SetFont('helvetica', '', 12);
        // Page number
        $this->Cell(0, 10, $this->getAliasNumPage(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}

	if (!function_exists('mb_ucfirst')) {
        function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
            $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
            $str_end = "";
            if ($lower_str_end) {
                $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
            }
            else {
                $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
            }
            $str = $first_letter . $str_end;
            return $str;
        }
    }

    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $doc = isset($_SESSION['doc']) ? $_SESSION['doc'] : 0;
	//die($doc);
    $sql_doc = "SELECT 
			n_processo, 
			reclamante, 
			reclamada, 
			data_pericia,
			instancia_vara,
			descricao_laudo,
			cidade,
			estado
		FROM documentos 
		WHERE id_doc = $doc";
    //die($sql_doc);
    foreach ($pdo->query($sql_doc) as $row){
	$processo = stripslashes(addslashes($row['n_processo']));
	$reclamante = stripslashes(addslashes($row['reclamante']));
	$reclamada = stripslashes(addslashes($row['reclamada']));
	$data_pericia = stripslashes(addslashes($row['data_pericia']));
	$instancia_vara = stripslashes(addslashes($row['instancia_vara']));
	$descricao_laudo = stripslashes(addslashes($row['descricao_laudo']));
	$cidade = stripslashes(addslashes($row['cidade']));
	$estado = stripslashes(addslashes($row['estado']));
    }
   
    function datar($data){
		list($ano, $mes_n, $dia) = explode('-', $data);		  
		$mes[1] ='Janeiro';
		$mes[2] ='Fevereiro';
		$mes[3] ='Março';
		$mes[4] ='Abril';
		$mes[5] ='Maio';
		$mes[6] ='Junho';
		$mes[7] ='Julho';
		$mes[8] ='Agosto';
		$mes[9] ='Setembro';
		$mes[10]='Outubro';
		$mes[11]='Novembro';
		$mes[12]='Dezembro';
		$mes_n = $mes_n < 10 ? $mes_n[1] : $mes_n;
		return ($dia.' de '.$mes[$mes_n].' de '.$ano);
    }

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetTitle('Laudo pericial:');

// set default header data
$cabecalho = "
<style type=\"text/css\">
		.texto {
       			font-family:  Zurich LtCn BT, sans-serif;
        		margin-bottom: 3px;
		        margin-top: 3px;
		        direction: ltr;
		        color: #000000;
		        text-align: justify;
		        font-style: italic;
				font-size: 8pt;
		}
</style>

<body lang=\"pt-br\" text=\"\"#000000\" link=\"#0000ff\" dir=\"ltr\">
<div>
    <p><font color=\"\"#808080\">

        </font>
    </p>
    <p align=left class=\"texto\" style=\"font-size: 9pt;\">
                Luiz  Carlos  Moreira
	</p>
    <p align=left class=\"texto\">				
			Especialista em Medicina do Trabalho ANAMT / AMB<br>
			Ergonomista Certificado ABERGO<br>
			Profissional Certificado PCMSO-G (Galvanoplastia) /FUNDACENTRO<br>
			Membro do ICOH – International Commission on Occupational Health<br>
			Associado à Sociedade Paulista de Perícias Médicas<br>
			Associado à Sociedade Brasileira de Perícias Médicas<br>
			Especialista em Perícias Médicas pela Associação Brasileira de Medicina Legal e Perícias Médicas – ABMLPM / AMB<br>
			Médico Perito da Justiça do Trabalho de Campinas<br>
			luiz@preventiva.med.br<br>		
    </p>

</div>
</body>";

$capa0 = "<div style=\"font-family: verdana, sans-serif; font-size: 13; text-align: left; margin-left:-15px; display: inline\">
	     	<div style=\" font-weight: bold; text-align: justify;\"><b>$instancia_vara</b></div><br>";

$capa_processo_label = "	<div style=\" font-weight: bold; font-size:14;\">
								Processo: 
	     	   				</div>";
$capa_reclamante_label = "	<div style=\" font-weight: bold; font-size:14; \">
								Reclamante:
	     	    			</div>";
$capa_reclamada_label = "	<div style=\" font-weight: bold; font-size:14;\">
								Reclamada: 
	     	    			</div>";

$capa_processo_value = "	<div style=\" font-weight: bold; font-size:16;\">
								$processo
	     	   				</div>";
$capa_reclamante_value = "	<div style=\" font-weight: bold; \">
								".mb_strtoupper($reclamante)."
	     	    			</div>";
$capa_reclamada_value = "	<div style=\" font-weight: bold; \">
								".mb_strtoupper($reclamada)."
	     	    			</div>";
 
$capa1 = "<div style=\"font-family: helvetica, sans-serif; font-size: 10.5; text-align:justify; line-height: 15px;\">&nbsp;&nbsp;&nbsp;&nbsp;$descricao_laudo</div>
	    	<div style=\"font-family: sans-serif; font-size: 13; text-align:center; margin-top:35px; line-height: 28px;\">
			Nestes termos, pede deferimento.
	    	</div>
	    	<div style=\"font-family: helvetica, sans-serif; font-size: 12; text-align:center; margin-top:35px; font-weight: bold;\">
			".mb_ucfirst($cidade).", ".datar($data_pericia)."
	    	</div>
	    	<div style=\"font-family: helvetica, sans-serif; font-size: 11; text-align:center; margin-top:35px; font-weight: bold;\">
			Luiz Carlos Moreira
	    	</div>
		</div>
";

	$i = 0;
    $aux = 1;
	$linestyle = array('color' => array(0, 0, 0));
    $indice .= "
			<style type=\"text/css\">
				table{
					width: 100%;
					text-align:justify;
					border: 1px solid black;
					border-width: medium;
				}
			</style>
			<table class=\"table\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 520px;\">
				<tbody>
					<tr>
						<td style=\"text-align: center;\">
							<span style=\"font-family: verdana, sans-serif; font-size: 14px; background-image: initial; background-attachment: initial; background-color: silver; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">
								\"LAUDO MÉDICO PERICIAL\"
							</span>
						</td>
					</tr>
				</tbody>
			</table><br><br>
			<div style=\"margin-left: 300px; width: 30px;\">
		    	<div style=\"font-style: italic; text-align: center; font-size:20px; font-family: verdana, sans-serif;\">
				ÍNDICE
		    	</div>
			</div>
	";
	$indice_numero = '';
	$indice_titulo = '';
	$indice_pagina = '';
    


	$paragrafo = "";
    $y = 0;
    for($x=1;$x<=14;$x++) {
        if ($_SESSION['tc'.$x] != '') {
            $y++;
            $paragrafo .= "
			<style type=\"text/css\">
				table{
					width: 100%;
					text-align:justify;
					border: 1px solid black;
					border-width: medium;
				}
			</style>

			<table class=\"table\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 520px;\">
				<tbody>
					<tr>
						<td style=\"text-align: center;\">
							<span style=\"font-family: verdana, sans-serif; font-size: large; background-image: initial; background-attachment: initial; background-color: silver; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">
								".$y.'. '.addslashes($_SESSION['tc'.$x])."
							</span>
						</td>
					</tr>
				</tbody>
			</table>
                          ";
        } else{
            $paragrafo .= '';
        }
        if ($_SESSION['c'.$x] != '') {
            $paragrafo.= $_SESSION['c'.$x]."<br/><br/>";
        } else{
            $paragrafo .= '';
        }
    }


$linestyle = array('color' => array(0, 0, 0));
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetMargins(PDF_MARGIN_LEFT+10, PDF_MARGIN_TOP+37, PDF_MARGIN_RIGHT+8);
$pdf->SetHtmlHeader($cabecalho);
$pdf->AddPage();
$pdf->writeHTML($capa0, true, 0, true, true);
$pdf->writeHTMLCell(43, 18, '', '', $capa_processo_label, 0, 0, false, '', true);
$pdf->writeHTMLCell(80, 18, '', '', $capa_processo_value, 0, 1, false, '', true);
$pdf->writeHTMLCell(43, 18, '', '', $capa_reclamante_label, 0, 0, false, '', true);
$pdf->writeHTMLCell(80, 18, '', '', $capa_reclamante_value, 0, 1, false, '', true);
$pdf->writeHTMLCell(43, 18, '', '', $capa_reclamada_label, 0, 0, false, '', true);
$pdf->writeHTMLCell(80, 18, '', '', $capa_reclamada_value, 0, 1, false, '', true);
$pdf->writeHTML($capa1, true, 0, true, true);

$pdf->AddPage();
$pdf->SetX(12);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+37, PDF_MARGIN_RIGHT);
$pdf->writeHTML($indice, false, 0, true, true);
$pdf->Line(92, 95, 117, 95, $linestyle);


for($i=1; $i<=14; $i++){
        //Indice e o titulo deste
		if ($_SESSION['tc'.$i] != '') {
			$indice_numero =  "<div style=\"font-size: 14pt; font-family: Helvetica, sans-serif; text-align:left;\">
									$aux.
								</div>";
			$indice_titulo =  "<div style=\"font-size: 14pt; font-family: Helvetica, sans-serif; text-align:left;\">
									{$_SESSION['tc'.$i]}
								</div>";
			$indice_pagina =  "<div style=\"font-size: 14pt; font-family: Helvetica, sans-serif; text-align: left;\">
									pg {$_SESSION['nc'.$i]}
								</div>";	
	    	$aux++;
			$pdf->writeHTMLCell(13, 13, 16, '', $indice_numero, 0, 0, false, '', true);
			$pdf->writeHTMLCell(145, 13, '', '', $indice_titulo, 0, 0, false, '', true);
			$pdf->writeHTMLCell(20, 13, '', '', $indice_pagina, 0, 1, false, '', true);
		}
    }

$pdf->AddPage();

$pdf->SetMargins(PDF_MARGIN_LEFT-2, PDF_MARGIN_TOP+37, PDF_MARGIN_RIGHT+22);
$pdf->SetX(12);
$pdf->writeHTML($paragrafo, true, 0, true, true);

//$pdf->SetHeaderData('ckeditor/plugins/imageuploader/uploads/image(2).png', PDF_HEADER_LOGO_WIDTH, 'PQR', 'XYZ');
$pdf->Output('example_001.pdf', 'I');
