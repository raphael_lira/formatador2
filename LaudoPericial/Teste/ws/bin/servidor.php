<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyApp\Arquivo;
 
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/src/MyApp/Arquivo.php';

 
$server = IoServer::factory(
 new HttpServer(
        new WsServer(
            new Arquivo()
        )
    ),    
    8080
);
 
$server->run();