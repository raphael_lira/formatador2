<?php

namespace MyApp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Arquivo implements MessageComponentInterface{

    //ligado a conexões
    private $connections = [];
    

   
     /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    function onOpen(ConnectionInterface $conn){
        $this->connections[$conn->resourceId]['conn'] = $conn;             
        $this->connections[$conn->resourceId]['doc'] = 0;             
        
        
    }
    
     /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn){
        $disconnectedId = $conn->resourceId;
        unset($this->connections[$disconnectedId]);    
    }
    
     /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e){
        $userId = $this->connections[$conn->resourceId]['user_id'];
        echo "An error has occurred with user $userId: {$e->getMessage()}\n";
        unset($this->connections[$conn->resourceId]);
        $conn->close();
    }
    
     /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $conn The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    function onMessage(ConnectionInterface $conn, $msg){
        $msg = json_decode($msg);
                
        $status = true;
        $resultado = (object) array() ;      
        
        
        if(isset($msg->metodo)){
            
            switch($msg->metodo){
                case 'downloadDoc':
                case 'abrirDoc':
                    $resultado->metodo = $msg->metodo;
                    foreach($this->connections as $conexao){
                        if($conexao['doc'] == $msg->doc){
                            $status = false;
                            break;
                        }
                            
                    }
                    $resultado->status = $status ;
                    $conn->send(json_encode($resultado));
                break;                
                case 'setDoc':
                    $this->connections[$conn->resourceId]['doc'] = $msg->doc;
                break;
            } 
        }     
        
    }
}