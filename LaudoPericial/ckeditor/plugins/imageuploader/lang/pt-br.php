<?php

$imagebrowser1 = "Galeria de Fotos";
$imagebrowser2 = "Total:";
$imagebrowser3 = "Imagens";
$imagebrowser4 = "Arraste seus arquivos aqui";

$uploadpanel1 = "Por favor, selecione um arquivo:";
$uploadpanel2 = "O upload da imagem será feito para:";
$uploadpanel3 = "O caminho da pasta de upload pode ser alterada nas configurações";

$panelsettings1 = "Pasta de Upload:";
$panelsettings2 = "Por favor, escolha uma pasta existente:";
$panelsettings3 = "Histórico do caminho:";
$panelsettings4 = "configurações:";
$panelsettings5 = "Ocultar extensão dos arquivos";
$panelsettings6 = "Exibir extensão dos arquivos";
$panelsettings7 = "Senha:";
$panelsettings8 = "Sair";
$panelsettings9 = "Desabilitar senha";
$panelsettings10 = "Do you like our plugin?";
$panelsettings11 = "Buy Us A Coffee!";
$panelsettings12 = "Support:";
$panelsettings13 = "Plugin FAQ";
$panelsettings14 = "Report a bug";
$panelsettings15 = "Version:";
$panelsettings16 = "Credits:";
$panelsettings17 = "Made with love by Moritz Maleck";
$panelsettings18 = "Icons:";
$panelsettings19 = "Icon pack by Icons8";
$panelsettings20 = "Alterar linguagem";
$panelsettings21 = "Ocultar secção de notícias";
$panelsettings22 = "Exibir secção de notícias";

$newssection1 = "You can disable the news section in the settings panel.";

$buttons1 = "Visualizar";
$buttons2 = "Download";
$buttons3 = "Utilizar";
$buttons4 = "Excluir";
$buttons5 = "Cancelar";
$buttons6 = "Salvar";
$buttons7 = "Upload";

$alerts1 = "Thanks for choosing Image Uploader and Browser for CKEditor!";
$alerts2 = "To use this plugin you need to set <b>CHMOD writable permission (0777)</b> to the <i>imageuploader</i> folder on your server.";
$alerts3 = "How to Change File Permissions Using FileZilla (external link)";
$alerts4 = "Check out the <a href='http://imageuploaderforckeditor.altervista.org/' target='_blank'>Documentation</a> or the <a href='http://imageuploaderforckeditor.altervista.org/support/' target='_blank'>Plugin FAQ</a> for more help.";
$alerts5 = "To use this plugin you need to enable <b>JavaScript</b> in your web browser.";
$alerts6 = "How to enable JavaScript in your browser (external link)";
$alerts7 = "A new version of Image Uploader and Browser for CKEditor is available.";
$alerts8 = "Download it now!";
$alerts9 = "The folder";
$alerts10 = "could not be found.";
$alerts11 = "create the folder";

$dltimageerrors1 = "Um erro ocorreu.";
$dltimageerrors2 = "Você pode  apenas deletar imagens. Por favor, tente novamente ou delete outra imagem.";
$dltimageerrors3 = "O arquivo que você quer deletar não está na pasta de upload selecionada.";
$dltimageerrors4 = "Você não pode deletar arquivos do sitema. Por favor tente novamente ou escolha outra imagem.";
$dltimageerrors5 = "O arquivo selecionado não pode ser deletado. Por favor, tente novamente ou escolha outra imagem. Nota: Não esqueça de permitir a escrita em arquivos da pasta imageuploader no servidor.";
$dltimageerrors6 = "O arquivo que você quer deletar não existe. Por favor, tente novamente ou escolha outra imagem.";

$uploadimgerrors1 = "O arquivo não é uma imagem.";
$uploadimgerrors2 = "Desculpe, o arquivo já existe.";
$uploadimgerrors3 = "Desculpe, o arquivo é muito pesado.";
$uploadimgerrors4 = "Desculpe, apenas imagens com extensão JPG, JPEG, PNG & GIF são permitidas.";
$uploadimgerrors5 = "Desculpe, your file was not uploaded. Don't forget to set CHMOD writable permission (0777) to imageuploader folder on your server.";
$uploadimgerrors6 = "Desculpe, houve um erro ao fazer o upload de seu arquivo. -";
$uploadimgerrors7 = "- Não esqueça de permitir a escrita em arquivos da pasta imageuploader no servidor.";

$loginerrors1 = "Usuário não encontrado, usuário ou senha incorretos!";

$configerrors1 = "Por favor utilize as configurações da galeria de fotos para mudar a visibilidade ou tente novamente.";
$configerrors2 = "Por favor, utilize a galeria de fotos para mudar o estilo do arquivo ou tente novamente try again.";

$loginsite1 = "Bem Vindo!";
$loginsite2 = "Por favor, faça login.";
$loginsite3 = "Usuário";
$loginsite4 = "Senha";
$loginsite5 = "Login";

$createaccount1 = "Por favor, crie uma nova conta (local) para prevenir invasores de verem e alterarem suas imagens.";
$createaccount2 = "Como posso desabilitar a proteção por senha? (link externo)";
$createaccount3 = "A pasta padrão de upload das imagens é <b>ckeditor/plugins/imageuploader/uploads</b>. Você pode mudar esta pasta nas configurações da galeria.";

$langpanel1 = "Por favor, selecione uma linguagem:";
$langpanel2 = "Atualmente selecionado:";
$langpanel3 = "Fechar";