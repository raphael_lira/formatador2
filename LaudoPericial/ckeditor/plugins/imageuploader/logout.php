<?php
session_start();
if(!isset($_SESSION['usuarioID'])) {
    exit;
}

session_destroy();
header("Location: imgbrowser.php");
