<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Reclamante:</span></span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px"><strong>ANTONIO ODOILSON MOREIRA DE&nbsp;OL IVEIRA</strong> , brasileiro, casado, conferente, portador da C&eacute;dula&nbsp;de&nbsp;Identidade RG25.676.679-4 e da CPTS n&deg;24745, s&eacute;rie n&deg;181, inscrito&nbsp;no CPF sob n&deg;149.869.748-82 e no PIS 12331146146, nascido em&nbsp;16.09.1971, filho de Rita Alves de Oliveira, residente e domiciliado na&nbsp;Rua Benedito Luis Cezarino n&deg;44, S&atilde;o Jos&eacute;, em Paul&iacute;nia ( SP) , CEP&nbsp;13140-829.</span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif"><strong>Do contrato de trabalho (conforme &ldquo;inicial&rdquo;):</strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">O Reclamante laborou para a Reclamada no per&iacute;odo de 10 de abril de&nbsp;2000 a 16&nbsp;de novembro de 2015, foi demitido imotivadamente quando&nbsp;exercia as fun&ccedil;&otilde;es de conferente.</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:14.0pt"><span style="font-family:Wingdings">&Oslash; </span></span><em><span style="font-size:14.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">DOEN&Ccedil;A OCUPACIONAL: &ldquo;TRANSTORNOS OSTEOMUSCULARES&rdquo;</span></span></em></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:14.0pt"><span style="font-family:Wingdings">&Oslash; </span></span><em><span style="font-size:14.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">ACIDENTE DE TRABALHO: &ldquo;TRANSTORNOS OSTEOMUSCULARES&rdquo;</span></span></em></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif"><strong><em><u>O Reclamante alega conforme &ldquo;inicial&rdquo; dos autos, </u></em><u>QUE:</u></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px">Ocorre MM Juiz ( a ) que o Reclamante no exerc&iacute;cio de suas atividades&nbsp;fabristeve seu&nbsp;estado de sa&uacute;de totalmente comprometido, sendo que a&nbsp;execu&ccedil;&atilde;o de servi&ccedil;os e acidentes do trabalho ocorridos, lhe trouxeram&nbsp;v&aacute;rias doen&ccedil;as do trabalho.<br />
O Reclamante est&aacute; com les&otilde;es nos punhos (direito e esquerdo), nos&nbsp;ombros (direito e esquerdo) e joelho direito . Sendo que foi submetido a&nbsp;v&aacute;rias cirurgias. Uma no punho direito, duas nos ombros (direito e&nbsp;esquerdo) e uma outra no joelho.<br />
Vejamos abaixo algumas das conclus&otilde;es das resson&acirc;ncias magn&eacute;ticas&nbsp;e ecografias realizados durante o pacto laborativo :<br />
Resson&acirc;ncia Magn&eacute;tica do Punho Esquerdo - Data&nbsp;16/11/2015<br />
Co</span></span><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px">nclus&atilde;o : Cisto artrossinovial/gangli&ocirc;nico em partes moles&nbsp;volares do carpo, profundamente aos tend&otilde;es flexores dos&nbsp;dedos.<br />
Resson&acirc;ncia Magn&eacute;ti</span></span><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px">ca do Punho Direito - Data 14/09/2015 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></span><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px">Conclus&atilde;o: Altera&ccedil;&otilde;es p&oacute;s - cir&uacute;rgicas com artefatos de&nbsp;susceptibilidade magn&eacute;tica junto &agrave; base do processo estiloide&nbsp;da ulna, com sinovite pr&eacute; estiloide e fragmento &oacute;sseo&nbsp;avulsionado adjacente&nbsp;a estas altera&ccedil;&otilde;es. Fibrocartilagem&nbsp;triangular inserida neste fragmento avulsionado. Edema&nbsp;circundado o ligamento radioulnar distal, sem abertura&nbsp;de&nbsp;espa&ccedil;o radio ulnar distal e sem transfixa&ccedil;&atilde;o deste ligamento.&nbsp;Tenossinovite dos do VI compartimento extensor e do recesso&nbsp;pr&eacute;-estiloide.<br />
Resson&acirc;cia Magn&eacute;tica do Ombro Direito - Data 06/08/2015<br />
Conclus&atilde;o: Status p&oacute;s-cir&uacute;rgico com acromioplastia. Leve&nbsp;artrose acromioclavicular. Degenera&ccedil;&atilde;o e fissura do l&aacute;bio&nbsp;</span></span><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px">glenoidal. Leve tendinopatia do supraespinhal. Bursite&nbsp;subacromial subdeltoide a grau leve.<br />
Resson&acirc;ncia Magn&eacute;tica do Punho Direto - Data 14/01/2015<br />
Conclus&atilde;o: Sequela de fratura no processo&nbsp;estiloide ulnar com&nbsp;fragmentos &oacute;sseos na regi&atilde;o de recesso pr&eacute;-estiloide, notando -&nbsp;se edema e cisto &oacute;sseos locais e sinovite, de aspecto mec&acirc;nico.&nbsp;Sinais de les&atilde;o do ligamento radioulnar dista dorsal. Altera&ccedil;&atilde;o&nbsp;de aspecto cr&ocirc;nico do ligamento escafossemilunar, sem&nbsp;descontinuidades. Estiramento/degenera&ccedil;&atilde;o dos ligamentos&nbsp;radiolunopiramidal e radioescafocapitato.<br />
Ecografia de M&atilde;o e Punho Direito - Data 18/11/2014<br />
Conclus&atilde;o: Tenossinuvite dos tend&otilde;es extenso do dedo m&iacute;nimo&nbsp;e&nbsp;ulnar do carpo.<br />
Resson&acirc;ncia Magn&eacute;tica do Joelho Direito - Data 04/04/2014</span></span>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Conclus&atilde;o: Menisco medial com fissura horizontal no corno&nbsp;posterior/ corpo, em contato com a superf&iacute;cie articular inferior&nbsp;atrav&eacute;s de pequeno pertuito. Menisco lateral com fissura&nbsp;horizontal no corno anterior, corpo e&nbsp;transi&ccedil;&atilde;o com o corno&nbsp;posterior, em contato com as superf&iacute;cies articulares atrav&eacute;s de&nbsp;pequenos pertuitos. Foco de condropatia grau II na face medial&nbsp;da tr&oacute;clea femoral e no c&ocirc;ndilo femoral medial.&nbsp;Pequeno/moderado derrame articular. Leve peritendinite da&nbsp;para anserina.<br />
Resson&acirc;ncia Magn&eacute;tica da Coluna Lombossacra - Data&nbsp;24/10/2012<br />
Conclus&atilde;o: Espondilose lombar com degenera&ccedil;&atilde;o discal, sem&nbsp;sinais de h&eacute;rnias discais ou compress&otilde;es radiculares.<br />
Resson&acirc;ncia Magn&eacute;tica do Punho Direito-Data19/11/2010<br />
Conclus&atilde;o: Discretas altera&ccedil;&otilde;es degenerativas acometendo a&nbsp;articula&ccedil;&atilde;o radio-ulnar distal. Irregularidade corticais com&nbsp;fragmenta&ccedil;&atilde;o acometendo o processo estiloide ulnar, associado&nbsp;a altera&ccedil;&otilde;es inflamat&oacute;rias na medular &oacute;ssea e partes moles&nbsp;adjacentes. Discreta tendinopatia do 6 &ordm; compartimento dos&nbsp;extensores. Forma&ccedil;&atilde;o c&iacute;stica lobulada e septada, localizada&nbsp;junto a face volar do captato e hemato, projetando-se o para o&nbsp;interior do t&uacute;nel do carpo, determinando abaulamento do&nbsp;retin&aacute;culo dos flexores.<br />
Laudo Radio l&oacute;gico Coluna Cervical (AP/ PER F I L /OB L &Iacute;QUAS ) -&nbsp;Data 19/11/&nbsp;2010<br />
Conclus&atilde;o: Espondiloartrose cervical.<br />
Coluna Dorsal - Lombar (AP/ PERFIL) Conclus&atilde;o: Espondiloartrose&nbsp;associada &agrave; sinistro - escolios e dorsal - lombar com &aacute;pice em T 5. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bacia (AP) Conclus&atilde;o: Estudo dentro dos padr&otilde;es de&nbsp;normalidade.<br />
Ecografia de Ombros - Data 17/11/2010<br />
Direito Conclus&atilde;o: Bursopatia subacromial (cr&ocirc;mica). &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Esquerdo Conclus&atilde;o : Tenossinovite bicipital sem cole&ccedil;&atilde;o&nbsp;(cr&ocirc;mica) bursopatia subdeltoidea subacromial&nbsp;(cr&ocirc;mica).<br />
Ecografia&nbsp;dos Tecido Moles das M&atilde;os e Punhos - Data&nbsp;22/09/2010<br />
Direita Conclus&atilde;o: prov&aacute;vel entesopatia comprimindo tend&atilde;o&nbsp;extensor ulnar do carpo. Obs . N&atilde;o &eacute; poss&iacute;vel afastar&nbsp;possiblidade de les&atilde;o ligamento na regi&atilde;o ( ligamento colateral&nbsp;ulnar).<br />
Esquerda Conclus&atilde;o: Avali&ccedil;&atilde;o ultrassonogr&aacute;fica dentro dos&nbsp;par&acirc;metros da normalidade.<br />
As les&otilde;es ocorridas em raz&atilde;o do acidente&nbsp;s&atilde;o totalmente&nbsp;irrevers&iacute;veis, dada a presen&ccedil;a de&nbsp;sequelas significativas que&nbsp;impossibilitam suas revers&otilde;es, mesmo com as cirurgias ocorridas, que&nbsp;constitui sem d&uacute;vida nenhuma na ocorr&ecirc;ncia de ACIDENTE DE&nbsp;TRABALHO PESSOAL. Em face dessa ocorr&ecirc;ncia de perda da capacidade&nbsp;produtiva, o Reclamante n&atilde;o mais poder&aacute; livrar-se da condi&ccedil;&atilde;o de&nbsp;empregado &ldquo; imprest&aacute;vel &rdquo; e &ldquo; acidentado &rdquo; para o mercado de trabalho.<br />
A Reclamada emitiu COMUNICA&Ccedil;&Atilde;O DE ACIDENTE DE TRABALHO&nbsp;(CAT), em tr&ecirc;s ocasi&otilde;es diferentes relatando os acidentes t&iacute;picos&nbsp;ocorridos, sendo que em todos os Reclamante ficou afastado de suas&nbsp;atividades e em alguns deles beneficiou-se do aux&iacute;lio doen&ccedil;a.<br />
&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><em><span style="font-size:12.0pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:blue"><span style="font-size:12px">Em Termo de Audi&ecirc;ncia, </span></span></span></span></em><span style="font-size:12px"><em><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">l&ecirc;-se a determina&ccedil;&atilde;o (e agendamento) de Per&iacute;cia M&eacute;dica e men&ccedil;&atilde;o aos honor&aacute;rios pr&eacute;vios.</span></em></span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12px"><span style="font-family:Cambria,serif"><strong><em><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Foi designada avalia&ccedil;&atilde;o m&eacute;dica pericial, realizada no dia 0</span></em><em><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2&nbsp;de Maio&nbsp;de 2018.</span></em></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif"><strong><em><u>O Reclamante informa nesta Per&iacute;cia M&eacute;dica, </u></em><u>QUE:</u></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif"><strong>HPMA &ndash; Hist&oacute;ria Pregressa da Mol&eacute;stia Atual:</strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><u>ACIDENTE DE TRABALHO T&Iacute;PICO E DOEN&Ccedil;A:</u></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Reclamante relatou que&nbsp; 06/2006, estava realizando o envazamento de g&aacute;s GLP em botij&atilde;o de g&aacute;s P 13 Kg, quando o botij&atilde;o escoregou na balan&ccedil;a e foi tracionado para frente; que tentou segurar e puxar o botij&atilde;o para tr&aacute;s, de volta (estava vazio; ainda n&atilde;o cheio com o g&aacute;s GLP);</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Sentiu um tranco no OMBRO DIREITO (fato ocoreu por volta das 16h00; hor&aacute;rio de trabalho habitual at&eacute; 18h00);</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong>Informou ao Sr Encarregado; Foi emitida CAT - Comunica&ccedil;&atilde;o de Acidente de Trabaloho pelo Empregador, sem afastamento do trabalho;</strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Na data do ocorrido, N&Atilde;O procurou por atendimento m&eacute;dico;&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">No dia seguinte de trabalho, foi designado para trabalho compat&iacute;vel, realizando teste de vazamento na v&aacute;lvula;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">No outro dia seguinte, voltou a exercer a atividade habitual de trabalho - envazamento de g&aacute;s GLP;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Continuou a sentir DOR EM OMBRO DIREITO, com aumento progressivo de intensidade; ap&oacute;s cerca de 15 dias, procurou por atendimento m&eacute;dico especializado - Ortopedista; relatou que sentia, tamb&eacute;m, DOR EM OMBRO ESQUERDO e PUNHO DIREITO</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Foi tratado com medicamento e sess&otilde;es de fisioterapia; n&atilde;o paresentou melhora significativa;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Apresentou relat&oacute;rio m&eacute;dico, datado de 26/03/2007, - Dr Waldir Brunelli, Crm 57.337 - COC - Ortopedia em Campinas, referenciando S&Iacute;NDROME DO IMPACTO EM OMBROS - BILATERAL - necessita de 90 dias de afastamento do trabalho ...</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Apresentou relat&oacute;rio m&eacute;dico, datado de 25/01/2008, - Dr Waldir Brunelli, Crm 57.337 - COC - Ortopedia em Campinas, referenciando S&Iacute;NDROME DO IMPACTO EM OMBRO DIREITO - <strong>cirurgia em 31/05/2007 (ARTROSCOPIA ; ACROMIOPLASTIA)&nbsp;</strong>... Sindrome do Canal do Carpo BILATERAL ...&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Relatou que cerca de 1 ano ap&oacute;s a cirurgia ralizada em OMBRO DIREITO, foi submetido a tratamento cir&uacute;rgico (ARTROSCOPIA) em OMBRO ESQUERDO;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Em Abril de 2015, foi submetido a tratameo cir&uacute;rgico em punho direito (nota: aos 16 anos de idade, sofreu queda de cavalo / fratura local / imobilizado na &eacute;poca)</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif"><strong>TRABALHO REALIZADO:</strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif"><strong>De 2a a 6a feira, 44 hs semanais; 01 hora de intevalo pararefei&ccedil;&atilde;o e descanso;</strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-family:Arial,sans-serif"><span style="font-size:12px"><strong>Desde admiss&atilde;o, atuou 6 meses em carga e descarga; mais 12 meses em arruma&ccedil;&atilde;o de vasilhames cheios;&nbsp;</strong></span></span><span style="font-family:Arial,sans-serif"><span style="font-size:12px"><strong>movimenta&ccedil;&atilde;o manual habitual de&nbsp;botij&otilde;es de 13kg de g&aacute;s + peso do valilhame (vazios e cheios)</strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-family:Arial,sans-serif"><span style="font-size:12px"><strong>Depois em platafora / Envase de g&aacute;s GLP: movimenta&ccedil;&atilde;o manual habitual de botij&otilde;es de 13kg de g&aacute;s + peso do valilhame (at&eacute; 2011)</strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-family:Arial,sans-serif"><span style="font-size:12px"><strong>Depois, CONFERENTE at&eacute; a sa&iacute;da da Reclamada (menor movimenta&ccedil;&atilde;o de botij&otilde;es - cerca de 20/30 por dia de trabalho; algunas dias n&atilde;o necess&aacute;rio movimentar)</strong></span></span></p>
