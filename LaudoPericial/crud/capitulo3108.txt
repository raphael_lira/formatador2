<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif">Identificar se existe rela&ccedil;&atilde;o de nexo causal entre o trabalho realizado e as queixas cl&iacute;nicas relatadas / apresentadas, considerando a an&aacute;lise documental, hist&oacute;rico cl&iacute;nico geral evolutivo, dados referenciados pelo(a) pr&oacute;prio(a) Reclamante e diagn&oacute;sticos estabelecidos; </span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif">Identificar se existe atualmente, alguma altera&ccedil;&atilde;o patol&oacute;gica;</span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif">Identificar, <em>em caso positivo</em>, se tal altera&ccedil;&atilde;o patol&oacute;gica &eacute; decorrente do trabalho realizado;</span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif">Identificar se existe algum grau de incapacidade laborativa atualmente, decorrente do trabalho realizado na Reclamada, inclusive para o exerc&iacute;cio da mesma fun&ccedil;&atilde;o que desempenhava na Reclamada.</span></span></p>
