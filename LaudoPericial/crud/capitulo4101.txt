<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;">CTPS <span style="color:blue">XXXX</span> S&eacute;rie <span style="color:blue">XXXX</span> , emitida em <span style="color:blue">XXX</span> &ndash; Campinas &ndash; SP </span></span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;">Data de nascimento:&nbsp; / / </span></span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm">&nbsp;</p>

<table border="1" cellspacing="0" class="Table" style="border-collapse:collapse; border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td style="width:128.2pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;">Empregador</span></span></strong></span></span></p>
			</td>
			<td style="width:127.2pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;">Fun&ccedil;&atilde;o</span></span></strong></span></span></p>
			</td>
			<td style="width:127.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;">Admiss&atilde;o</span></span></strong></span></span></p>
			</td>
			<td style="width:129.05pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16pt"><span style="font-family:Cambria,serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;">Sa&iacute;da</span></span></strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td style="width:128.2pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
			<td style="width:127.2pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
			<td style="width:127.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
			<td style="width:129.05pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>

<p style="margin-left:0cm; margin-right:0cm">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:Arial,sans-serif"><strong>Nega ocupa&ccedil;&atilde;o ap&oacute;s a sa&iacute;da da Reclamada, mesmo de modo informal</strong></span></span></p>
