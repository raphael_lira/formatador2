<?php 
	require 'database.php';
	$id = 0;
	
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( !empty($_POST)) {
		// keep track post values
		$id = $_POST['id'];
		
		// delete data
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "DELETE FROM documentos WHERE id_doc = $id";
		//die("<pre>$sql</pre>");
		$pdo->query($sql);

		$sql = "DELETE FROM capitulos WHERE id_documento = $id";
		$pdo->query($sql);

		Database::disconnect();
		header("Location: index.php");
		
	} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Excluir documento</title>
    <meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
</head>

<body>
	<section id="content">
		<div id="header">Logado como <b>Admin</b> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">

			<div class="container">

				<div class="span10 offset1">
					<div class="row">
						<h3>Excluir documento</h3>
					</div>

					<form class="form-horizontal" action="delete.php" method="post">
						<input type="hidden" name="id" value="<?php echo $id;?>"/>
						<p class="alert alert-error" style="text-align: center;">
							Tem certeza que deseja excluir?&nbsp;&nbsp;	A ação será irreversível!
						</p>
						<div class="form-actions">
							<button type="submit" class="btn btn-danger offset2">Yes</button>
							<a class="btn" href="index.php">No</a>
						</div>
					</form>
				</div>
			</div> <!-- /container -->
		</div>
	</section>
  </body>
</html>