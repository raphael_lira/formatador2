<?php
session_start();
if(!isset($_SESSION['usuarioID'])){
	header('Location: ../../login.php');
}?>
<!DOCTYPE html>
<head>
    	<title>Documentos</title>
    	<meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="../sistema.ico" type="image/x-icon"/>
   	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../style.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
	<script>
		function editar(documento){
			$.post(
				'update.php',
				{
					'doc' : documento
				},
				function(data){
					//redireciona para a pagina de criacao de pdf
					window.location.assign("../index.php?p=document&doc="+data.doc);
				},
				 'json'
			);
		}

		function download(documento){
			$.post(
				'update.php',
				{
					'doc' : documento
				},
				function(data){
					//redireciona para o arquivo que gera o pdf
					window.location.assign("read.php?doc="+data.doc);
				},
				 'json'
			);
		}

	</script>
</head>
<?php
include 'database.php';
$pdo = Database::connect();
 
$sql_user = "SELECT login FROM usuario WHERE id_usuario = {$_SESSION['usuarioID']}";
foreach ($pdo->query($sql_user) as $row){
    $user = $row['login'];
}
?>
<body>
	<section id="content">
		<div id="header">Logado como <b><?php echo $user?></b> | <a href="../sair.php">Sair</a> </div>

		<div id="nav" style="margin: auto;">

			<div class="container offset1" >
					<div class="row">
						<h3>Documentos Criados</h3>
					</div>
					<div class="row">

						<table class="table table-striped table-bordered">
							  <thead>
								<tr>
								  <th>Nome do documento</th>
								  <th>Data da perícia</th>
								  <th>Número do processo</th>
								  <th><a href="create.php" class="btn btn-success">Criar novo documento</a></th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							   
							   
							   $sql = " SELECT 	id_doc,
												nome_documento,
												n_processo,
												TO_CHAR(data_pericia, 'DD/MM/YYYY') AS data_pericia
										FROM documentos
										ORDER BY data_pericia
								  ";
							   foreach ($pdo->query($sql) as $row){
									echo '<tr>';
									echo '<td>'. $row['nome_documento'] . '</td>';
									echo '<td>'. $row['data_pericia'] . '</td>';
									echo '<td>'. $row['n_processo'] . '</td>';
									echo '<td width=350>';
									echo '<a class="btn btn-primary" href="duplicate.php?id='.$row['id_doc'].'">Salvar como</a>';
					                                echo '&nbsp;';
									echo '<a class="btn" href="javascript:download('.$row['id_doc'].');">Download</a>';
									echo '&nbsp;';
									echo '<a class="btn btn-success" href="javascript:editar('.$row['id_doc'].');">Editar</a>';
									echo '&nbsp;';
									echo '<a class="btn btn-danger" href="delete.php?id='.$row['id_doc'].'">Excluir</a>';
									echo '</td>';
									echo '</tr>';
							   }
							   Database::disconnect();
							  ?>
							  </tbody>
						</table>
					</div>
				</div> <!-- /container -->
			</div>
	</section>
</body>
</html>