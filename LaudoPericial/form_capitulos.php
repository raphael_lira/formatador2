<script>
    $(document).ready(function() {
       
    	initSample();

        $('#titulo').keyup(function (event) {
            Envia_Form('#form', CKEDITOR.instances.editor.getData(), 'salva_paragrafo.php?p=<?=$p?>', 1);
        });

        CKEDITOR.instances.editor.on('change', function() {
            Envia_Form('#form', CKEDITOR.instances.editor.getData(), 'salva_paragrafo.php?p=<?=$p?>', 1);
        });

    });
</script>
<form id="form" name="form" method="post" action="proc.php">
    <div class="titulo">
	<div style="width: 3%; margin-left:4px;">
		TÍTULO
	</div>
        <div style="width: 97%;">
		<textarea class="titulo-input" style="height: 1px;" name="titulo" id="titulo" placeholder="Digite o título"><?php echo null !== ($_SESSION["t$p"]) ? ($_SESSION["t$p"]) : '';?></textarea>
	</div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div id="editor" tabindex="1">
				<?php echo sprintf('%s',$texto);?>
            </div>
            <input type="hidden" id="valor_paragrafo" name="valor_paragrafo" />
            <input type="hidden" id="capitulo" name="capitulo" value="<?=$capitulo?>" />
            <input type="hidden" id="documento" name="documento" value="<?=$doc?>"/>
        </div>
        <p style="margin-top: 10px;">
            <br>
            <input type="button" value="Salvar capítulo" onclick="Envia_Form('#form', CKEDITOR.instances.editor.getData(), 'crud/update_fill.php', 1);" />
            <input type="button" value="Gerar PDF" onclick="Envia_Form('#form', CKEDITOR.instances.editor.getData(), 'proc.php', 0, '_blank');" />
            <input type="button" value="Voltar aos documentos" onclick=" window.location.href='crud/index.php';" />
        </p>
    </div>
</form>