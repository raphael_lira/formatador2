<form id="documento" name="documento" method="post" action="form_documento.php">
    <?php
    $document = isset($_SESSION['document']) ? stripslashes(addslashes($_SESSION['document'])) : '';
    $data_pericia = isset($_SESSION['data_pericia']) ? stripslashes(addslashes($_SESSION['data_pericia'])) : '';
    $estado = isset($_SESSION['estado']) ? stripslashes(addslashes($_SESSION['estado'])) : '';
    $cidade = isset($_SESSION['cidade']) ? stripslashes(addslashes($_SESSION['cidade'])) : '';
	$cabecalho = isset($_SESSION['cabecalho']) ? stripslashes(addslashes($_SESSION['cabecalho'])) : '';
    ?>
   	<div class="titulo" style = "display: block;">
		<div style="margin: 48px auto 0 auto;">
			<span style="font-size: 18px;">Nome do documento:</span>
			<textarea style="padding: 0px; overflow:hidden; width: 520px; height: 30px; font-size: 18px;" id="document" name="document" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1);"><?php echo $document?></textarea><br>
			<span style="font-size: 18px; ">Data da perícia:</span>
			<input type="text" style="padding: 0px; overflow:hidden; margin-right: 419px; width: 150px; height: 30px; font-size: 18px;" id="data_pericia" name="data_pericia" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1);" value="<?php echo $data_pericia?>"><br>
			<div style="display: flex; margin: auto;">
                <div style="width: 64.4%;">
                    <span style="font-size: 18px;">Estado:</span>
                    <select name="estado" id="estado" onchange="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1);" style="height:30px;">
                        <option value="" selected>Selecione o Estado</option>
                        <option value="ac" <?php echo ($estado=='ac')?'selected':'' ?>>Acre</option>
                        <option value="al" <?php echo ($estado=='al')?'selected':'' ?>>Alagoas</option>
                        <option value="am" <?php echo ($estado=='am')?'selected':'' ?>>Amazonas</option>
                        <option value="ap" <?php echo ($estado=='ap')?'selected':'' ?>>Amapá</option>
                        <option value="ba" <?php echo ($estado=='ba')?'selected':'' ?>>Bahia</option>
                        <option value="ce" <?php echo ($estado=='ce')?'selected':'' ?>>Ceará</option>
                        <option value="df" <?php echo ($estado=='df')?'selected':'' ?>>Distrito Federal</option>
                        <option value="es" <?php echo ($estado=='es')?'selected':'' ?>>Espírito Santo</option>
                        <option value="go" <?php echo ($estado=='go')?'selected':'' ?>>Goiás</option>
                        <option value="ma" <?php echo ($estado=='ma')?'selected':'' ?>>Maranhão</option>
                        <option value="mt" <?php echo ($estado=='mt')?'selected':'' ?>>Mato Grosso</option>
                        <option value="ms" <?php echo ($estado=='ms')?'selected':'' ?>>Mato Grosso do Sul</option>
                        <option value="mg" <?php echo ($estado=='mg')?'selected':'' ?>>Minas Gerais</option>
                        <option value="pa" <?php echo ($estado=='pa')?'selected':'' ?>>Pará</option>
                        <option value="pb" <?php echo ($estado=='pb')?'selected':'' ?>>Paraíba</option>
                        <option value="pr" <?php echo ($estado=='pr')?'selected':'' ?>>Paraná</option>
                        <option value="pe" <?php echo ($estado=='pe')?'selected':'' ?>>Pernambuco</option>
                        <option value="pi" <?php echo ($estado=='pi')?'selected':'' ?>>Piauí</option>
                        <option value="rj" <?php echo ($estado=='rj')?'selected':'' ?>>Rio de Janeiro</option>
                        <option value="rn" <?php echo ($estado=='rn')?'selected':'' ?>>Rio Grande do Norte</option>
                        <option value="ro" <?php echo ($estado=='ro')?'selected':'' ?>>Rondônia</option>
                        <option value="rs" <?php echo ($estado=='rs')?'selected':'' ?>>Rio Grande do Sul</option>
                        <option value="rr" <?php echo ($estado=='rr')?'selected':'' ?>>Roraima</option>
                        <option value="sc" <?php echo ($estado=='sc')?'selected':'' ?>>Santa Catarina</option>
                        <option value="sp" <?php echo ($estado=='sp')?'selected':'' ?>>São Paulo</option>
                        <option value="se" <?php echo ($estado=='se')?'selected':'' ?>>Sergipe</option>
                        <option value="to" <?php echo ($estado=='to')?'selected':'' ?>>Tocantins</option>
                    </select>
                </div>
                <div style="width: 60%;">
                    <span style="margin-left:-116px; font-size: 18px;">Cidade:</span>
                    <textarea style="margin-left:2px; width: 53.6%; height: 30px; padding: 0px;"  id="cidade" name="cidade" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1);"><?php echo $cidade?></textarea>
                </div>
            </div><br>
			<span style="font-size: 18px;">Cabeçalho:</span>
			<textarea style="padding: 0px; overflow:hidden; width: 604px; height: 30px; font-size: 18px;" id="cabecalho" name="cabecalho" onkeyup="Envia_Form('#documento', '', 'salva_documento.php?doc=<?php echo $doc?>', 1);"><?php echo $cabecalho?></textarea><br>
			
		</div><br>
	</div

    <p>
		<input type="button" value="Gerar PDF" onclick="Envia_Form('#documento', '', 'proc.php', 0, '_blank');" />
		<input type="button" value="Voltar aos documentos" onclick="window.location.href='crud/index.php';" />
	</p>

</form>