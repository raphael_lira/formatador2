<?php
session_start();
// Include the main TCPDF library (search for installation path).
define('K_PATH_IMAGES', '');
require_once('TCPDF/tcpdf.php');
require 'crud/database.php';

class MyTCPDF extends TCPDF {
    var $htmlHeader;
    var $linestyle = array('color' => array(255, 0, 0));
    public function SetHtmlHeader($htmlHeader) {
        $this->htmlHeader = $htmlHeader;
    }

    public function Header() {
        $this->writeHTMLCell($w = 0,
            $h = 0,
            $x = '',
            $y = '',
            $this->htmlHeader,
            $border = 0, $ln = 1,
            $fill = 0,
            $reseth = true,
            $align = 'top',
            $autopadding = true);
        $this->Line(17, 60, 185, 60, $this->linestyle);

    }

    public function Footer() {
        $this->SetY(-17);
        $this->SetX(-10);
        // Set font
        $this->SetFont('verdana', '', 12);
        // Page number
        $this->Cell(0, 10, $this->getAliasNumPage(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";
        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        }
        else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;
        return $str;
    }
}

$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$doc = isset($_SESSION['doc']) ? $_SESSION['doc'] : 0;
//die($doc);
$sql_doc = "SELECT 
			n_processo, 
			reclamante, 
			reclamada, 
			data_pericia,
			instancia_vara,
			descricao_laudo,
			cidade,
			estado
		FROM documentos 
		WHERE id_doc = $doc";
//die($sql_doc);
foreach ($pdo->query($sql_doc) as $row){
    $processo = stripslashes(addslashes($row['n_processo']));
    $reclamante = stripslashes(addslashes($row['reclamante']));
    $reclamada = stripslashes(addslashes($row['reclamada']));
    $data_pericia = stripslashes(addslashes($row['data_pericia']));
    $instancia_vara = stripslashes(addslashes($row['instancia_vara']));
    $descricao_laudo = stripslashes(addslashes($row['descricao_laudo']));
    $cidade = stripslashes(addslashes($row['cidade']));
    $estado = stripslashes(addslashes($row['estado']));
}

function datar($data){
    list($ano, $mes_n, $dia) = explode('-', $data);
    $mes[1] ='Janeiro';
    $mes[2] ='Fevereiro';
    $mes[3] ='Março';
    $mes[4] ='Abril';
    $mes[5] ='Maio';
    $mes[6] ='Junho';
    $mes[7] ='Julho';
    $mes[8] ='Agosto';
    $mes[9] ='Setembro';
    $mes[10]='Outubro';
    $mes[11]='Novembro';
    $mes[12]='Dezembro';
    $mes_n = $mes_n < 10 ? $mes_n[1] : $mes_n;
    return ($dia.' de '.$mes[$mes_n].' de '.$ano);
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetTitle('Laudo pericial:');
//$fontname = TCPDF_FONTS::addTTFfont('TCPDF/fonts/verdanab.ttf', 'TrueTypeUnicode', '', 32);
// set default header data
$cabecalho = "
<style type=\"text/css\">
		.texto {
       			font-family:  Zurich LtCn BT, sans-serif;
        		margin-bottom: 3px;
		        margin-top: 3px;
		        direction: ltr;
		        color: #000000;
		        text-align: justify;
		        font-style: italic;
				font-size: 8pt;
		}
</style>

<body lang=\"pt-br\" text=\"\"#000000\" link=\"#0000ff\" dir=\"ltr\">
<div>
    <p><font color=\"\"#808080\">

        </font>
    </p>
    <p align=left class=\"texto\" style=\"font-size: 9pt;\">
                Luiz  Carlos  Moreira
	</p>
    <p align=left class=\"texto\">				
			Especialista em Medicina do Trabalho ANAMT / AMB<br>
			Ergonomista Certificado ABERGO<br>
			Profissional Certificado PCMSO-G (Galvanoplastia) /FUNDACENTRO<br>
			Membro do ICOH – International Commission on Occupational Health<br>
			Associado à Sociedade Paulista de Perícias Médicas<br>
			Associado à Sociedade Brasileira de Perícias Médicas<br>
			Especialista em Perícias Médicas pela Associação Brasileira de Medicina Legal e Perícias Médicas – ABMLPM / AMB<br>
			Médico Perito da Justiça do Trabalho de Campinas<br>
			luiz@preventiva.med.br<br>		
    </p>

</div>
</body>";
$capa0 = "<div style=\"font-family: verdana; font-size: 13; text-align: left; display: inline\">
	     	<div style=\" font-weight: bold; text-align: justify;\"><b>$instancia_vara</b></div><br><br>";
$capa_processo_label = "	<div style=\" font-weight: bold; font-size:14;\">
								Processo: 
	     	   				</div>";
$capa_reclamante_label = "	<div style=\" font-weight: bold; font-size:14; \">
								Reclamante:
	     	    			</div>";
$capa_reclamada_label = "	<div style=\" font-weight: bold; font-size:14;\">
								Reclamada: 
	     	    			</div>";

$capa_processo_value = "	<div style=\" font-weight: bold; font-size:16;\">
								$processo
	     	   				</div>";
$capa_reclamante_value = "	<div style=\" font-weight: bold; \">
								".mb_strtoupper($reclamante)."
	     	    			</div>";
$capa_reclamada_value = "	<div style=\" font-weight: bold; \">
								".mb_strtoupper($reclamada)."
	     	    			</div>";

$capa1 = "<div style=\"font-family: verdana, sans-serif; font-size: 10.5; text-align:justify; line-height: 15px;\">&nbsp;&nbsp;&nbsp;&nbsp;$descricao_laudo</div>
	    	<div style=\"font-family: sans-serif; font-size: 13; text-align:center; margin-top:35px; line-height: 28px;\">
			Nestes termos, pede deferimento.
	    	</div>
	    	<div style=\"font-family: verdana, sans-serif; font-size: 12; text-align:center; margin-top:35px; font-weight: bold;\">
			".mb_ucfirst($cidade).", ".datar($data_pericia)."
	    	</div>
	    	<div style=\"font-family: verdana, sans-serif; font-size: 11; text-align:center; margin-top:35px; font-weight: bold;\">
			Luiz Carlos Moreira
	    	</div>
		</div>
";

$i = 0;
$aux = 1;
$linestyle = array('color' => array(0, 0, 0));

$indice = "
			<style type=\"text/css\">
				table{
					width: 100%;
					text-align:justify;
					border: 1px solid black;
					border-width: medium;
				}
			</style>
			<table class=\"table\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 520px;\">
				<tbody>
					<tr>
						<td style=\"text-align: center;\">
							<span style=\"font-family: verdana, sans-serif; font-size: 14px; background-color: silver;\">\"LAUDO MÉDICO PERICIAL\"</span>
						</td>
					</tr>
				</tbody>
			</table><br><br>
			<div style=\"margin-left: 300px; width: 30px; text-align: center; font-family: verdana, sans-serif; font-style: italic\">
		    	ÍNDICE<br>
			</div>
	";
$indice_numero = '';
$indice_titulo = '';
$indice_pagina = '';



$linestyle = array('color' => array(0, 0, 0));
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetMargins(PDF_MARGIN_LEFT+10, PDF_MARGIN_TOP+37, PDF_MARGIN_RIGHT+8);
$pdf->SetHtmlHeader($cabecalho);
$pdf->AddPage();
$pdf->writeHTML($capa0, true, 0, true, true);
$pdf->writeHTMLCell(43, 18, '', '', $capa_processo_label, 0, 0, false, '', true);
$pdf->writeHTMLCell(80, 18, '', '', $capa_processo_value, 0, 1, false, '', true);
$pdf->writeHTMLCell(43, 18, '', '', $capa_reclamante_label, 0, 0, false, '', true);
$pdf->writeHTMLCell(80, 18, '', '', $capa_reclamante_value, 0, 1, false, '', true);
$pdf->writeHTMLCell(43, 18, '', '', $capa_reclamada_label, 0, 0, false, '', true);
$pdf->writeHTMLCell(80, 18, '', '', $capa_reclamada_value, 0, 1, false, '', true);
$pdf->writeHTML($capa1, true, 0, true, true);


$pdf->AddPage();

$pdf->SetMargins(PDF_MARGIN_LEFT-2, PDF_MARGIN_TOP+37, PDF_MARGIN_RIGHT+2);
$pdf->SetX(12);

$y = 0;
$paginas[] = [];

for($x=1;$x<=14;$x++) {
    if ($_SESSION['tc'.$x] != '') {
        $y++;
        $paragrafo = "<style type=\"text/css\">
				.table{
					width: 100%;
					text-align:justify;
					border: 1px solid black;
					border-width: medium;
					font-size: 12px;
				}
			</style>
			<table class=\"table\" align=\"center\" cellspacing=\"1\" cellpadding=\"1\" style=\"width: 520px; margin-left: -100px;\"><tbody><tr><td style=\"text-align: center;\"><span style=\"font-family: verdana, sans-serif; font-size: 14; background-color: silver;\">$y.".addslashes($_SESSION['tc'.$x])."</span></td></tr></tbody></table>";
        //die($paragrafo);
        $pdf->writeHTML($paragrafo, true, 0, true, true);
        array_push($paginas, $pdf->PageNo()+1);
    } else{
        $paragrafo = '';
    }
    if ($_SESSION['c'.$x] != '') {
        $paragrafo = $_SESSION['c'.$x]."<p>&nbsp;</p><p>&nbsp;</p>";
        $pdf->writeHTML($paragrafo, true, 0, true, true);
    } else{
        $paragrafo = '<p>&nbsp;</p><0p>&nbsp;</0p><p>&nbsp;</p>';
        $pdf->writeHTML($paragrafo, true, 0, true, true);
    }

}




$dimensions = $pdf->getPageDimensions();
$hasBorder = false; //flag for fringe case
$rowcount = 0;


$pdf->AddPage();
$pdf->SetX(12);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+37, PDF_MARGIN_RIGHT);
$pdf->SetFont('verdana', 'I', 13);
$pdf->writeHTML($indice, false, 0, true, true);
$pdf->Line(97, 89, 117, 89, $linestyle);

$pdf->SetFont('verdana', '', 14);

for($i=1; $i<=14; $i++){
    //Indice e o titulo deste
    if ($_SESSION['tc'.$i] != '') {
        $indice_numero =  "$aux.";
        $indice_titulo =  mb_ucfirst(mb_strtolower("{$_SESSION['tc'.$i]}", 'UTF-8'));
        $indice_pagina =  "pg {$paginas[$i]}";
        $aux++;
        //work out the number of lines required
        $rowcount = max($pdf->getNumLines($indice_numero, 13),$pdf->getNumLines($indice_titulo, 140),$pdf->getNumLines($indice_pagina, 20));

        $startY = $pdf->GetY();

        if (($startY + $rowcount * 6) + $dimensions['bm'] > ($dimensions['hk'])) {
            //this row will cause a page break, draw the bottom border on previous row and give this a top border
            $borders = '';
        } elseif ((ceil($startY) + $rowcount * 6) + $dimensions['bm'] == floor($dimensions['hk'])) {
            //fringe case where this cell will just reach the page break
            //draw the cell with a bottom border as we cannot draw it otherwise
            $borders = '';
            $hasborder = true; //stops the attempt to draw the bottom border on the next row
        } else {
            //normal cell
            $borders = '';
        }

        //now draw it
        $pdf->SetX(14);
        $pdf->MultiCell(13,$rowcount * 6+5, $indice_numero, $borders,'L',0,0);
        $pdf->MultiCell(140,$rowcount * 6+5, $indice_titulo, $borders,'L',0,0);
        $pdf->MultiCell(5,$rowcount * 6+5, '', $borders,'L',0,0);
        $pdf->MultiCell(80,$rowcount * 6+5, $indice_pagina, $borders,'L',0,0);

        $pdf->Ln();
    }
}


//$pdf->writeHTML($paragrafo, true, 0, true, true);
//
//$pdf->AddPage();
//$pdf->SetX(12);
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+37, PDF_MARGIN_RIGHT);
//$pdf->SetFont('verdana', 'I', 13);
//$pdf->writeHTML($indice, false, 0, true, true);
//$pdf->Line(97, 89, 117, 89, $linestyle);

$pdf->movePage($pdf->getNumPages(), 2);

//$pdf->SetHeaderData('ckeditor/plugins/imageuploader/uploads/image(2).png', PDF_HEADER_LOGO_WIDTH, 'PQR', 'XYZ');
$pdf->Output('example_001.pdf', 'I');
