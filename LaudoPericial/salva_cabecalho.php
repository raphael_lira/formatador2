<?php
    session_start();
    require 'crud/database.php';

    $doc = isset($_GET['doc']) ? $_GET['doc'] : '';

    $vara  	= null !== pg_escape_string($_POST['vara'])	 	? pg_escape_string($_POST['vara']) 		: "''";
    $processo 	= null !== pg_escape_string($_POST['processo']) 	? pg_escape_string($_POST['processo']) 		: "''";
    $reclamante	= null !== pg_escape_string($_POST['reclamante']) 	? pg_escape_string($_POST['reclamante']) 	: "''";
    $reclamada 	= null !== pg_escape_string($_POST['reclamada']) 	? pg_escape_string($_POST['reclamada']) 	: "''";
    $desc_laudo	= null !== pg_escape_string($_POST['desc_laudo']) 	? pg_escape_string($_POST['desc_laudo']) 	: "''";

    $_SESSION['vara'] = stripslashes(addslashes($_POST['vara']));
    $_SESSION['processo'] = stripslashes(addslashes($_POST['processo']));
    $_SESSION['reclamante'] = stripslashes(addslashes($_POST['reclamante']));
    $_SESSION['reclamada'] = stripslashes(addslashes($_POST['reclamada']));
    $_SESSION['desc_laudo'] = stripslashes(addslashes($_POST['desc_laudo']));

	if($doc != ''){
    	$pdo = Database::connect();
    	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "UPDATE documentos SET
        		        n_processo = '".$processo."',
						reclamante = '".$reclamante."',
						reclamada = '".$reclamada."',
						instancia_vara = '".$vara."',
						descricao_laudo = '".$desc_laudo."'
	    		WHERE id_doc = $doc";
    	$pdo->query($sql);
		Database::disconnect();
	}
//    $paragrafo  = isset($_POST['valor_paragrafo']) ? $_POST['valor_paragrafo'] : '';
//    $p = $_GET['p'];
//    $_SESSION[$p] = $paragrafo;

?>