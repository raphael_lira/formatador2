<!DOCTYPE html>
	<head>
        	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script>
		    function set(valor){
			$('#listener_click').val(valor);
			$('#login').submit();
		    }
		</script>
		<meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="LaudoPericial/style.css" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Hind:400,700,600,500,300' rel='stylesheet' type='text/css'>
		<title>Gerador</title>
	</head>

	<body>
		<section id="content">
			<form method="post" id="login" action="valida.php" class="text-center">
			  <label>Usuário</label>
			  <p><input type="text" name="usuario" maxlength="50" /></p>
			  
			  <label>Senha</label>
			  <p><input type="password" name="senha" maxlength="50" /></p>
			  
			  <div>
		 	      <button name="laudo" type="button" onclick="set(1)">
				    <img src="LaudoPericial/sistema.ico" width="45px" height="45px" title="Acesso ao sistema de edição de laudos periciais"><br>
			            <b>Laudo Pericial</b>
			      </button>
			      <button name="site" type="button" onclick="set(2)">
				    <img src="site/logo.jpg" width="45px" height="45px" title="Acesso ao site institucional como administrador"><br>
				    <b>Site Institucional</b>
			      </button>
			      <input type="hidden" id="listener_click" name="listener_click" />
			  </div>
			
			</form>
		</section>
</body>
</html>