<?php
require(__DIR__ . '/pt-br.php');
$info1 = pathinfo($_FILES["image1"]["name"]);
$info2 = pathinfo($_FILES["image2"]["name"]);
$info3 = pathinfo($_FILES["image3"]["name"]);
$ext1 = strtolower($info1['extension']);
$ext2 = strtolower($info2['extension']);
$ext3 = strtolower($info3['extension']);
$target_dir = 'imgs/';
$ckpath = "imgs/";
$randomLetters = $rand = substr(md5(microtime()),rand(0,26),6);
$imgnumber = count(scandir($target_dir));
$filename1 = "$imgnumber$randomLetters.".$ext1;
$filename2 = "$imgnumber$randomLetters.".$ext2;
$filename3 = "$imgnumber$randomLetters.".$ext3;
$target_file1 = $target_dir . $filename1;
$target_file2 = $target_dir . $filename2;
$target_file3 = $target_dir . $filename3;
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
$check1 = getimagesize($_FILES["image1"]["tmp_name"]);
$check2 = getimagesize($_FILES["image2"]["tmp_name"]);
$check3 = getimagesize($_FILES["image3"]["tmp_name"]);
$ckfile1 = $ckpath . $filename1;
$ckfile2 = $ckpath . $filename2;
$ckfile3 = $ckpath . $filename3;

if(($check1 !== false) && ($check2 !== false) && ($check3 !== false)) {
    $uploadOk = 1;

} else {
    echo "<script>alert('".$uploadimgerrors1."');</script>";
    $uploadOk = 0;
}
// Check if file already exists
if ((file_exists($target_file1)) && (file_exists($target_file2)) && (file_exists($target_file3))) {
    echo "<script>alert('".$uploadimgerrors2."');</script>";
    $uploadOk = 0;
}
// Check file size
if (($_FILES["image1"]["size"]) && ($_FILES["image2"]["size"]) && ($_FILES["image3"]["size"])) > 1024000) {
    echo "<script>alert('".$uploadimgerrors3."');</script>";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "ico" ) {
    echo "<script>alert('".$uploadimgerrors4."');</script>";
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<script>alert('".$uploadimgerrors5."');</script>";
// if everything is ok, try to upload file
} else {
    if ((move_uploaded_file($_FILES["image1"]["tmp_name"], $target_file1)) && (move_uploaded_file($_FILES["image2"]["tmp_name"], $target_file2)) && (move_uploaded_file($_FILES["image3"]["tmp_name"], $target_file3))) {
        return;
    } else {
        echo "<script>alert('".$uploadimgerrors6." ".$target_file1." ".$uploadimgerrors7."');</script>";
        echo "<script>alert('".$uploadimgerrors6." ".$target_file2." ".$uploadimgerrors7."');</script>";
        echo "<script>alert('".$uploadimgerrors6." ".$target_file3." ".$uploadimgerrors7."');</script>";
        die();
    }
}


?>