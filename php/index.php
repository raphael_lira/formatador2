<?php
session_start();
$logado = isset($_SESSION['usuarioID']);
if(!$logado){
	header('Location: http://www.preventiva.med.br/login.php');
}

require 'database.php';
$pdo = Database::connect();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="mediaQ.css">
		<link rel="stylesheet" type="text/css" href="modal.css">

		<style type="text/css">
			.post{
				text-decoration: none;
				color: #000;
			}

			.imagem{
				max-width: 122px;
				max-height: 127px;
			}
			.div-image{
				background-color: #fff /*#ffcc99*/; 
				height: 100px;
				/*line-height: 150px;*/
				clear: both;
				text-align: center;
				min-width: 122px;
				max-width: 122px;
				padding: 0;
			}
		</style>

		
		<title>Preventiva</title>
	</head>
	<body>
		<?php include "menu.html"; ?>
		<?php
            function readMoreFunction($story_desc,$link,$id, $chars = 25) {
                $story_desc = preg_replace('/<iframe.*?\/iframe>/i','', $story_desc);
                $story_desc = substr($story_desc,0,$chars);
                $story_desc = substr($story_desc,0,strrpos($story_desc,' '));
                $story_desc = $story_desc."... <a href='$link?id=$id'>Ler mais</a>";
                return $story_desc;
            }
            $_SESSION['usuarioID'] = '1';
			$logado = isset($_SESSION['usuarioID']);
            $acao = '<input type="hidden" name="acao">';
            $idimage = '<input type="hidden" name="idimage">';
		?>
		
						<?php
							$sql_post = "SELECT idimage, image1, image2, image3 FROM index where idimage = 1 ";
							$qry = $pdo->query($sql_post);
							
							//var_dump($pdo->errorInfo());

						?>
			<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
					<?php 
					$i=1;
					while ($row = $qry->fetch(PDO::FETCH_OBJ)) { 
				
						?>
	    				<div class="carousel-item active">
						    <img class="d-block img-fluid" src="<?php echo $row->image1; ?>">
	    				</div>
	    				
	 			    	<div class="carousel-item">
	      					<img class="d-block img-fluid" src="<?php echo $row->image2; ?>" alt="Second slide">
	    				</div>
	 			    	<div class="carousel-item">
	      					<img class="d-block img-fluid" src="<?php echo $row->image3; ?>" alt="Second slide">
	    				</div>
    				<?php 
    				}
    				?>
  				</div>
			</div>

			<?php if($logado){ 
				echo "	<a href=\"#\" onclick=\"Modal('Adicionar publicação');return false;\" data-toggle=\"modal\" data-target=\"#add-publicacao\">
							<img style='height: 53px; margin-top: 25px; margin-left: 10px;' src='imgs/icon/add.ico'>
						</a>";
			}?>	


            <div class="modal fade" id="add-publicacao" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                            </button>
                            <h4 class="panel-title" id="title-publicacao">
                                Editar Carousel
                            </h4>
                        </div>
                        <form action="returnIndex.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                            <div class="modal-body" style="padding: 5px;">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control" type="file" name="imagem1" accept="image/*">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control" type="file" name="imagem2" accept="image/*">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control" type="file" name="imagem3" accept="image/*">
                                    </div>
                                </div>
                                <?php
                                echo $acao;
                                echo $idimage;
                                ?>
                            </div>
                            <div class="panel-footer" style="margin-bottom:-14px;">
                                <button style="float: right;" type="button" class="btn btn-default btn-close"
                                        data-dismiss="modal">
                                    Cancelar
                                </button>
                                <input type="submit" class="btn btn-success" value="Salvar"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

		<section>

			<br>
			<div class="row">
				<div class="col-md-6" style="background-color: #fff"> 
					<a href="publicacoes.php"><h1> Publicações </h1></a>
					<div class="row">
						<?php
							$sql_post = "SELECT descricao, titulo, idpublicacao, image FROM publicacoes ORDER BY idpublicacao DESC LIMIT 2 ";
							$qry = $pdo->query($sql_post);
							
							//var_dump($pdo->errorInfo());
							while($row = $qry->fetch(PDO::FETCH_OBJ)) {

								echo "
								<a href=\"conteudo.php?id=$row->idpublicacao\"  style=\"display: flex;\">
									<div class=\"col-md-4 div-image\">
										<img class=\"imagem\" src=\"$row->image\">
									</div>
									<div class=\"col-md-8 post\">
											<h3>$row->titulo</h3>
											<p>".readMoreFunction($row->descricao,'conteudo.php',$row->idpublicacao)."</p>
									</div>
								</a>				
								<div class=\"col-md-12\" style=\"height: 20px\"></div>";
							}
						?>
					</div>
				</div>
				<!-- <div class="col-md-2" style="background-color: #fff"></div>  -->
				<div class="col-md-6" style="background-color: #fff"> 
					<a href="sobre.php"><h1> Sobre </h1></a>
					<h2>Missão da Empresa</h2> <br>
					<p style="text-align: justify;">
						<?php 
							$sql = 'SELECT missao from sobre where quem = 0'; 
							$qry = $pdo->query($sql);
							$result = $qry->fetchAll();
							// $sobreEmpresa=$pdo->query($sql);
							/*foreach($pdo->query($sql) as $row){
								$sobreEmpresa = $row->descricao;
							}*/
							//echo $sobreEmpresa;
							echo readMoreFunction($result[0]['missao'], 'sobre.php', '', 100);
							//var_dump($result);
						?>
					</p>
				</div>
			</div>

		</section>

		<footer>
			<div id="bottombar">
				<div>
					Preventiva - (19)99999-9999 <br> Campinas, SP
				</div>
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

		<script type="text/javascript">
			function Modal(valor, id){
			        
		        $("#title-publicacao").text(valor);
	            $('#titulo').val($('#titulo-'+id).text().trim());
	            $('#descricao').val($('#descricao-'+id).text());
	            $('#titulo').addClass('edit');
	            $('#descricao').addClass('edit');
                $("[name='idimage']").val(id);
                $("[name='acao']").val(2);
		    }
		</script>

	</body>
</html>