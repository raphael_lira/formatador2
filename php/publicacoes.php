<?php session_start();
$logado = isset($_SESSION['usuarioID']);
if(!$logado){
	header('Location: http://www.preventiva.med.br/login.php');
}

?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="mediaQ.css">
		<link rel="stylesheet" type="text/css" href="modal.css">
		

		<style type="text/css">
			.post{
				text-decoration: none;
				color: #000;
				margin-left: -60px;
			}

			.imagem{
				max-width: 263px;
				max-height: 160px;

			}
			.div-image{
				background-color: #fff /*#ffcc99*/; 
				height: 160px;
				line-height: 150px;				
				text-align: center;
				min-width: 263px;
				max-width: 263px;
				padding: 0;
				float: left;
			}
			div#link-post{
				margin-bottom: 20px;
			    clear: both;
			}

			a:hover{
    			text-decoration: none;
			}

			.modal-body:not(.two-col) { padding:0px }
			.glyphicon { margin-right:5px; }
			.glyphicon-new-window { margin-left:5px; }
			.modal-body .radio,.modal-body .checkbox {margin-top: 0px;margin-bottom: 0px;}
			.modal-body .list-group {margin-bottom: 0;}
			.margin-bottom-none { margin-bottom: 0; }
			.modal-body .radio label,.modal-body .checkbox label { display:block; }
			.modal-footer {margin-top: 0px;}
			@media screen and (max-width: 325px){
			    .btn-close {
			        margin-top: 5px;
			        width: 100%;
			    }
			    .btn-results {
			        margin-top: 5px;
			        width: 100%;
			    }
			    .btn-vote{
			        margin-top: 5px;
			        width: 100%;
			    }
			    
			}

			
			.panel-footer {
			    padding: 10px 15px;
			    background-color: #f5f5f5;
			    border-top: 1px solid #ddd;
			    border-bottom-right-radius: 3px;
			    border-bottom-left-radius: 3px;			  
			}
			.panel {
			    margin-bottom: 20px;
			    background-color: #fff;
			    border: 1px solid transparent;
			    border-radius: 4px;
			    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
			    box-shadow: 0 1px 1px rgba(0,0,0,.05);
			}
			.panel-heading {
			    padding: 10px 15px;
			    border-bottom: 1px solid transparent;
			    border-top-right-radius: 3px;
			    border-top-left-radius: 3px;
			}
			.modal-footer .btn+.btn {
			    margin-left: 0px;
			}
			.progress {
			    margin-right: 10px;
			}
			.panel-primary>.panel-heading {
			    color: #fff;
			    background-color: #ffd133;
			    border-color: #ffd133;
			}

			.panel-primary {
			    border-color: #ffd133;
			}
			#title-excluir{
				background-color: #d9534f;
			}
		</style>

		<title>Preventiva</title>
	</head>

	<body>
		<?php include "menu.html"; ?>
		<?php 
			//session_unset($_SESSION['usuarioID']);
//			$_SESSION['usuarioID'] = '1';

            $acao = '<input type="hidden" name="acao">';
            $idpublicacao = '<input type="hidden" name="idpublicacao">';

        function readMoreFunction($story_desc,$id) {
            //Number of characters to show
            $chars = 50;
//            $story_desc = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" allowfullscreen></iframe>",$story_desc);
//            $story_desc = preg_replace('/<iframe.*?\/iframe>/i','', $story_desc);
            $story_desc = substr($story_desc,0,$chars);
            $story_desc = substr($story_desc,0,strrpos($story_desc,' '));
            $story_desc = $story_desc." <a href='conteudo.php?id=$id'>Ler mais...</a>";
            return $story_desc;
        }
        ?>
		<section>

			<!--modal-->		
			<style type="text/css">
			    .edit{
			        color: #000; 
			        font-weight: bold;
			    }
			</style>
						

			<div class="row">
				<div class="col-md-12" style="display: flex;">
					<h1>Publicações
					<?php if($logado){ 
					 	echo "	<a href=\"#\" onclick=\"Modal('Adicionar publicação');return false;\" data-toggle=\"modal\" data-target=\"#add-publicacao\">
					 				<img style='height: 53px; margin-top: 25px; margin-left: 10px;' src='imgs/icon/add.ico'>
					 			</a>";
					 }?>	
					<!-- modal exclui post-->
                     <div class="modal fade" id="excluir-publicacao" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="panel panel-primary">
                                <div class="panel-heading" id="title-excluir">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                    <h4 class="panel-title" ></span>
                                        Excluir publicação
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="returnPublicacao.php">
                                        <div style="padding: 20px; text-align: center;">
                                            <p>Tem certeza que deseja excluir esta publicação?</p>
                                                <?php
                                                    echo $acao;
                                                   	echo $idpublicacao;
                                                ?>
                                        </div>
                                        <div class="panel-footer" style=" ">
                                            <button style="float: right;" type="button" class="btn btn-default btn-close" data-dismiss="modal">
                                                Cancelar
                                            </button>
                                            <input type="submit" value="Sim" class="btn btn-danger">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--modal adiciona/altera post-->
                    <div class="modal fade" id="add-publicacao" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                                    </button>
                                    <h4 class="panel-title" id="title-publicacao">
                                        Adicionar publicação
                                    </h4>
                                </div>
                                <form action="returnPublicacao.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                    <div class="modal-body" style="padding: 5px;">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                                <input class="form-control" id="titulo" name="titulo" placeholder="Título" type="text" required/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                                <input class="form-control" type="file" name="imagem" accept="image/*">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <textarea id="editor" name="descricao" style="resize:vertical;" class="form-control" placeholder="Texto..." rows="6" required></textarea>
                                            </div>
                                        </div>
                                        <?php
	                                        echo $acao;
	                                        echo $idpublicacao;
                                        ?>
                                    </div>
                                    <div class="panel-footer" style="margin-bottom:-14px;">
                                        <button style="float: right;" type="button" class="btn btn-default btn-close"
                                                data-dismiss="modal">
                                            Cancelar
                                        </button>
                                        <input type="submit" class="btn btn-success" value="Salvar"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

				
					
				<?php 
				include 'database.php';
				$pdo = Database::connect();
				$sql_post = "SELECT descricao, titulo, idpublicacao, image, idpublicacao FROM publicacoes ORDER BY idpublicacao DESC";
				$qry = $pdo->query($sql_post);
				
				$html = "";
				while($row = $qry->fetch(PDO::FETCH_OBJ)) {
				    $descricao = preg_replace( "/\r|\n/", "", $row->descricao);
//				    die(readMoreFunction($row->descricao, $row->idpublicacao));
					$html .= "
					<div id=\"link-post\"   style=\"display: flex;\">
						<div class=\"col-md-6 \">
							<div class=\"div-image\">
								<img class=\"imagem\" src=\"$row->image\">
							</div>
						</div>
						<div class=\"col-md-6 post\">
								<h3 id=\"titulo-".$row->idpublicacao."\">$row->titulo ";
					if($logado){
						$html .= "<a href=\"#\" onclick=\"Modal('Editar publicação', ".$row->idpublicacao.", '".$descricao."');return false;\" data-toggle=\"modal\" data-target=\"#add-publicacao\">
									<img style='width: 40px;' src='imgs/icon/edit.png'>
								  </a>
								  <a href=\"#\" onclick=\"Excluir(".$row->idpublicacao.");return false;\" data-toggle=\"modal\" data-target=\"#excluir-publicacao\">
									<img style='width: 40px;' src='imgs/icon/remove.png'>
								  </a>";

					}
					$html .= "</h3>
								<div id=\"descricao-".$row->idpublicacao."\">".readMoreFunction($row->descricao, $row->idpublicacao)."</div>
						</div>
					</div>";
					

				}
				echo $html;
				//echo $qry->rowCount();
				Database::disconnect(); ?>
				
		</section>

		<footer>
			<div id="bottombar">
				<div>
					Preventiva - (19)99999-9999 <br> Campinas, SP
				</div>
			</div>
		</footer>

		<script  src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="  crossorigin="anonymous"></script>
		<script  src="https://code.jquery.com/jquery-3.2.1.js"  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="  crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		<script type="text/javascript">
			function Excluir(id){
				$("[name='idpublicacao']").val(id);
                $("[name='acao']").val(3);
			}

			function Modal(valor, id, descricao){
			        
		        $("#title-publicacao").text(valor);
		        if(typeof id !== "undefined" || id == 0) { //alterando uma publicacao
                    console.log(descricao);
		            $('#titulo').val($('#titulo-'+id).text().trim());
		            $('#editor').val(descricao.trim());
		            $('#titulo').addClass('edit');
		            $('#descricao').addClass('edit');
                    $("[name='idpublicacao']").val(id);
                    $("[name='acao']").val(2);
		        }else{                                     //criando uma novo publicacao
		            $('#titulo').removeClass('edit');
		            $('#editor').removeClass('edit');
		            $('#titulo').val("");
		            $('#editor').val("");
		            $("[name='acao']").val(1);
		        }
		    }




        </script>
	</body>
</html>