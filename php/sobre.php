<?php session_start();?>

<?php

$logado = isset($_SESSION['usuarioID']);
if(!$logado){
	header('Location: http://www.preventiva.med.br/login.php');
}

$acao = '<input type="hidden" name="acao">';
$quem = '<input type="hidden" name="quem">';
require 'database.php';
$pdo = Database::connect();
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="mediaQ.css">
		<link rel="stylesheet" type="text/css" href="modal.css">
		<title>Preventiva</title>
	</head>
	<body>
		<?php include 'menu.html'; ?>

             <script type="text/javascript">

            function Modal(valor, id, field){
                    
                $("#title").text(valor);
                if(typeof id !== "undefined" || id == 0) { //alterando registro
                 
		    $('#descricao').val($('#descricao-'+id).text());	
                    $('#missao').val($('#missao-'+id).text());
                    $('#visao').val($('#visao-'+id).text());
		    $('#descricao').val($('#descricao-'+id).text().trim());
		    $('#missao').val($('#missao-'+id).text().trim());
		    $('#visao').val($('#visao-'+id).text().trim());
		    $('#descricao').addClass('edit');
                    $('#missao').addClass('edit');
                    $('#visao').addClass('edit');
                    $("[name='"+field+"']").val(1);
                    $("[name='acao']").val(2);

                }
            }
        </script> 


            <!--modal adiciona/altera post-->
            <div class="modal fade" id="add-descricao" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                            </button>
                            <h4 class="panel-title" id="title">
                                Editar descrição
                            </h4>
                        </div>
                        <form action="returnSobre.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                            <div class="modal-body" style="padding: 5px;">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <textarea id="descricao" name="descricao" style="resize:vertical;" class="form-control" placeholder="Texto..." rows="6" required></textarea>
                                    </div>
                                </div>
                                <?php
                                echo $acao;
                                echo $quem;
                                ?>
                            </div>
                            <div class="panel-footer" style="margin-bottom:-14px;">
                                <button style="float: right;" type="button" class="btn btn-default btn-close"
                                        data-dismiss="modal">
                                    Cancelar
                                </button>
                                <input type="submit" class="btn btn-success" value="Salvar"/>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        <div class="col-md-12" style="height: 20px"></div>

         <div class="modal fade" id="add-image" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                            </button>
                            <h4 class="panel-title" id="title">
                                Editar Imagem
                            </h4>
                        </div>
                        <form action="returnSobre.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                            <div class="modal-body" style="padding: 5px;">
                                  <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control" type="file" name="imagem" accept="image/*">
                                    </div>
                                </div>
                                <?php
                                echo $acao;
                                echo $quem;
                                ?>
                            </div>
                            <div class="panel-footer" style="margin-bottom:-14px;">
                                <button style="float: right;" type="button" class="btn btn-default btn-close"
                                        data-dismiss="modal">
                                    Cancelar
                                </button>
                                <input type="submit" class="btn btn-success" value="Salvar"/>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
        <div class="col-md-12" style="height: 20px"></div>

         <div class="modal fade" id="add-missao" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                            </button>
                            <h4 class="panel-title" id="title">
                                Editar Missão
                            </h4>
                        </div>
                        <form action="returnSobre.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                            <div class="modal-body" style="padding: 5px;">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <textarea id="missao" name="missao" style="resize:vertical;" class="form-control" placeholder="Texto..." rows="6" required></textarea>
                                    </div>
                                </div>
                                <?php
                                echo $acao;
                                echo $quem;
                                ?>
                            </div>
                            <div class="panel-footer" style="margin-bottom:-14px;">
                                <button style="float: right;" type="button" class="btn btn-default btn-close"
                                        data-dismiss="modal">
                                    Cancelar
                                </button>
                                <input type="submit" class="btn btn-success" value="Salvar"/>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
        <div class="col-md-12" style="height: 20px"></div>

         <div class="modal fade" id="add-visao" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                            </button>
                            <h4 class="panel-title" id="title">
                                Editar Visão
                            </h4>
                        </div>
                        <form action="returnSobre.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                            <div class="modal-body" style="padding: 5px;">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <textarea id="visao" name="visao" style="resize:vertical;" class="form-control" placeholder="Texto..." rows="6" required></textarea>
                                    </div>
                                </div>
                                <?php
                                echo $acao;
                                echo $quem;
                                ?>
                            </div>
                            <div class="panel-footer" style="margin-bottom:-14px;">
                                <button style="float: right;" type="button" class="btn btn-default btn-close"
                                        data-dismiss="modal">
                                    Cancelar
                                </button>
                                <input type="submit" class="btn btn-success" value="Salvar"/>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
        <div class="col-md-12" style="height: 20px"></div>

				<?php 
				$sql_post = "SELECT nome, descricao, missao, visao, image FROM sobre where quem = 1";
				$qry = $pdo->query($sql_post);
       			 //var_dump($pdo->errorInfo());
                while($row = $qry->fetch(PDO::FETCH_OBJ)) {
                ?>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
			<div class="team boxed-grey text-center">
				<div id="link-post" style=\"display: flex;\"><img src=" <?php echo $row->image ?>" width="200" height="200" style="border-radius: 100%; margin-top: 30px;" alt="" class="img-responsive img-circle" /></div>

					<?php if($logado){ ?>
                                <a href="#" onclick="Modal('Editar Imagem ', '<?php echo $row->quem ?>', 'quem');return false;" data-toggle="modal" data-target="#add-image">
                                    <img style='width: 40px;' src='imgs/icon/edit.png'>
                                </a>
                                  <div class="col-md-12" style="height: 20px"></div>
                                  <?php } ?>

				<h2><br/> <?php echo $row->nome ?></h2>
			</div>
		</div>	

		<div style="width:65%; margin:0 auto;">
			<p id="descricao-<?php echo $row->quem; ?>">
				<?php echo nl2br($row-> descricao); ?>
			
			     	<?php if($logado){ ?>
                                <a href="#" onclick="Modal('Editar Descrição', '<?php echo $row->quem; ?>', 'quem');return false;" data-toggle="modal" data-target="#add-descricao">
                                    <img style='width: 40px;' src='imgs/icon/edit.png'>
                                </a>
                                  <div class="col-md-12" style="height: 20px"></div>
                                  <?php } ?>

            </p>

		<h3>Missão da Empresa</h3>
			
            <p id="missao-<?php echo $row->quem; ?>">
				<?php echo nl2br($row-> missao); ?>
		
		          	<?php if($logado){ ?>
                                <a href="#" onclick="Modal('Editar Missão', '<?php echo $row->quem ?>', 'quem');return false;" data-toggle="modal" data-target="#add-missao">
                                    <img style='width: 40px;' src='imgs/icon/edit.png'>
                                </a>
                                  <div class="col-md-12" style="height: 20px"></div>
                                  <?php } ?>
            </p>
                                  
		<h3>Visão da Empresa</h3>

			<p id="visao-<?php echo $row->quem; ?>">
				<?php echo nl2br($row->visao); ?>
			
		      		<?php if($logado){ ?>
                                <a href="#" onclick="Modal('Editar Visão', '<?php echo $row->quem ?>', 'quem');return false;" data-toggle="modal" data-target="#add-visao">
                                    <img style='width: 40px;' src='imgs/icon/edit.png'>
                                </a>
                                  <div class="col-md-12" style="height: 20px"></div>
                                  <?php } ?>
            </p>
                                  
		</div>
		   <?php } ?>

    </section>

        <footer>
            <div id="bottombar">
                <div>
                    Preventiva - (19) 3367-2758 <br> Campinas, SP
                </div>
            </div>
        </footer>

        <script  src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="  crossorigin="anonymous"></script>
        <script  src="https://code.jquery.com/jquery-3.2.1.js"  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="  crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <!--<script src="modal-popup.js"></script>-->
    </body>
</html>