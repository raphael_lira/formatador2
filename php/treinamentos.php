<?php session_start();
$logado = isset($_SESSION['usuarioID']);
if(!$logado){
	header('Location: http://www.preventiva.med.br/login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="mediaQ.css">
		<link rel="stylesheet" type="text/css" href="modal.css">
    <title>Preventiva</title>
</head>
<body>

     <script type="text/javascript">

            function Excluir(id, field){
                $("[name='"+field+"']").val(id);
                $("[name='acao']").val(3);
            }


            function Modal(valor, id, field){

                $("#title").text(valor);
                if(typeof id !== "undefined" || id == 0) { //alterando registro

                    $('#titulo').val($('#titulo-'+id).text().trim());
                    $('#descricao').val($('#descricao-'+id).text());
                    $('#titulo').addClass('edit');
                    $('#descricao').addClass('edit');
                    $("[name='"+field+"']").val(id);
                    $("[name='acao']").val(2);

                  
                }else{                                     //criando uma novo registro
                    $('#titulo').removeClass('edit');
                    $('#descricao').removeClass('edit');
                    $('#titulo').val("");
                    $('#descricao').val("");
                    $("[name='acao']").val(1);
                }
            }
        </script> 
    <?php include "menu.html"; ?>
<?php
$acao = '<input type="hidden" name="acao">';
$idservico = '<input type="hidden" name="idservico">';
require 'database.php';
$pdo = Database::connect();
?>
    <section>
    <div class="col-md-12" style="height: 20px"></div>
    <div class="row">
        <div class="col-md-12">
            <h1>Treinamento</h1>
            <?php if($logado){ ?>
                   <a href="#" onclick="Modal('Adicionar treinamento');return false;" data-toggle="modal" data-target="#add-treinamento">
                                    <img style='height: 53px; margin-top: -15px; margin-left: 10px;' src='imgs/icon/add.ico'>
                                </a>
            <?php } ?>
            <!--modal exclui post-->
            <div class="modal fade" id="excluir-treinamento" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="panel panel-primary">
                        <div class="panel-heading" id="title-excluir">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h4 class="panel-title">
                                Excluir treinamento
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="returnTreinamentos.php">
                                <div style="padding: 20px; text-align: center;">
                                    <p>Tem certeza que deseja excluir este treinamento?</p>
                                    <?php
                                    echo $acao;
                                    echo $idservico;
                                    ?>
                                </div>
                                <div class="panel-footer">
                                    <button style="float: right;" type="button" class="btn btn-default btn-close" data-dismiss="modal">
                                        Cancelar
                                    </button>
                                    <input type="submit" value="Sim" class="btn btn-danger">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!--modal adiciona/altera post-->
            <div class="modal fade" id="add-treinamento" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                            </button>
                            <h4 class="panel-title" id="title">
                                Adicionar treinamento
                            </h4>
                        </div>
                        <form action="returnTreinamentos.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                            <div class="modal-body" style="padding: 5px;">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control" id="titulo" name="titulo" placeholder="Tí­tulo" type="text" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control" type="file" name="imagem" accept="image/*">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <textarea id="descricao" name="descricao" style="resize:vertical;" class="form-control" placeholder="Texto..." rows="6" required></textarea>
                                    </div>
                                </div>
                                <?php
                                echo $acao;
                                echo $idservico;
                                ?>
                            </div>
                            <div class="panel-footer" style="margin-bottom:-14px;">
                                <button style="float: right;" type="button" class="btn btn-default btn-close"
                                        data-dismiss="modal">
                                    Cancelar
                                </button>
                                <input type="submit" class="btn btn-success" value="Salvar"/>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
        <div class="col-md-12" style="height: 20px"></div>
        <?php
        $sql_post = "SELECT descricao, titulo, idservico, image FROM servicos where servicostreinamentos = 0";
        $qry = $pdo->query($sql_post);
        //var_dump($pdo->errorInfo());
                    while($row = $qry->fetch(PDO::FETCH_OBJ)) {
                        ?>
                        <div class="col-xs-6 col-sm-4 col-md-4 text-center">
                            <div class="team boxed-grey text-center">
                                <div class="avatar"><img class="imagem" src='<?php echo $row->image;?>' width="200" height="200" style="border-radius: 100%" alt="" class="img-responsive img-circle" /></div>
                                    <h2 id="titulo-<?php echo $row->idservico; ?>"><?php echo $row->titulo; ?></h2>
                                    <p id="descricao-<?php echo $row->idservico; ?>"><?php echo $row->descricao; ?></p>
                                 <?php if($logado){ ?>
                                <a href="#" onclick="Modal('Editar treinamento', '<?php echo $row->idservico; ?>' , 'idservico');" data-toggle="modal" data-target="#add-treinamento">
                                    <img style='width: 40px;' src='imgs/icon/edit.png'>
                                  </a>
                                  <a href="#" onclick="Excluir('<?php echo $row->idservico; ?>', 'idservico');return false;" data-toggle="modal" data-target="#excluir-treinamento">
                                    <img style='width: 40px;' src='imgs/icon/remove.png'>
                                  </a>
                                  <div class="col-md-12" style="height: 20px"></div>
                                  <?php } ?>
                            </div>
                        </div>
                    <?php } ?>  

    </section>

        <footer>
            <div id="bottombar">
                <div>
                    Preventiva - (19)99999-9999 <br> Campinas, SP
                </div>
            </div>
        </footer>

        <script  src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="  crossorigin="anonymous"></script>
        <script  src="https://code.jquery.com/jquery-3.2.1.js"  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="  crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <!--<script src="modal-popup.js"></script>-->
    </body>
</html>
