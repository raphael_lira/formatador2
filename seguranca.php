<?php
/**
* Sistema de segurança com acesso restrito
*
* Usado para restringir o acesso de certas páginas do seu site
*
* @author Thiago Belem <contato@thiagobelem.net>
* @link http://thiagobelem.net/
*
* @version 1.0
* @package SistemaSeguranca
*/
//  Configurações do Script
// ==============================
$_SG['conectaServidor'] = true;    // Abre uma conexão com o servidor MySQL?
$_SG['caseSensitive'] = false;     // Usar case-sensitive? Onde 'thiago' é diferente de 'THIAGO'
$_SG['validaSempre'] = true;       // Deseja validar o usuário e a senha a cada carregamento de página?
// ==============================

// Verifica se precisa iniciar a sessão

  session_start();
/**
* Função que valida um usuário e senha
*
* @param string $usuario - O usuário a ser validado
* @param string $senha - A senha a ser validada
*
* @return bool - Se o usuário foi validado ou não (true/false)
*/
function validaUsuario($usuario, $senha) {
  // Usa a função addslashes para escapar as aspas
  // Monta uma consulta SQL (query) para procurar um usuário

  require 'LaudoPericial/crud/database.php';
  $pdo = Database::connect();
  $sql = "SELECT id_usuario, login, senha, adm, id_licenca FROM usuario WHERE login = '$usuario' AND senha = '$senha' LIMIT 1";
  $qry = $pdo->query($sql);
  $rs = $qry->fetch(PDO::FETCH_ASSOC);
  $id_user = array_values($rs)[0];
  $user = trim(array_values($rs)[1]);
  $pass = trim(array_values($rs)[2]);
  $adm = trim(array_values($rs)[3]);
  $licenca = trim(array_values($rs)[4]);

  if($usuario == $user && $senha == $pass){
    $resultado = 1;
  }

  // Verifica se encontrou algum registro
  if(empty($resultado)){
    // Nenhum registro foi encontrado => o usuário é inválido
    return false;
  }else{
    // Definimos dois valores na sessão com os dados do usuário
    $_SESSION['usuarioID'] = $id_user; // Pega o valor da coluna 'id do registro encontrado no SQL
    $_SESSION['usuarioNome'] = $user;  // Pega o valor da coluna 'nome' do registro encontrado no SQL
    $_SESSION['adm'] = $adm;  // Pega o valor da coluna 'adm' do registro encontrado no SQL
    $_SESSION['id_licenca'] = $licenca;  // Pega o valor da coluna 'id_lincenca' do registro encontrado no SQL

    return $_SESSION['usuarioID'];
  }
}
/**
* Função que protege uma página
*/
function protegePagina() {
  global $_SG;
  if (!isset($_SESSION['usuarioID']) OR !isset($_SESSION['usuarioNome'])) {
    // Não há usuário logado, manda pra página de login
    expulsaVisitante();
  } else if (!isset($_SESSION['usuarioID']) OR !isset($_SESSION['usuarioNome'])) {
    // Há usuário logado, verifica se precisa validar o login novamente
    if ($_SG['validaSempre'] == true) {
      // Verifica se os dados salvos na sessão batem com os dados do banco de dados
      if (!validaUsuario($_SESSION['usuarioLogin'], $_SESSION['usuarioSenha'])) {
        // Os dados não batem, manda pra tela de login
        expulsaVisitante();
      }
    }
  }
}
/**
* Função para expulsar um visitante
*/
function expulsaVisitante() {
  global $_SG;
  // Remove as variáveis da sessão (caso elas existam)
  unset($_SESSION['usuarioID'], $_SESSION['usuarioNome'], $_SESSION['usuarioLogin'], $_SESSION['usuarioSenha']);
  // Manda pra tela de login
  header("Location: autenticar.php");
}

?>