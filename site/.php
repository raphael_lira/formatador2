<?php
require 'database.php';
$pdo = Database::connect();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">	

		<title>Preventiva</title>
	</head>
	<body>
		<header>
			<nav id="topbar">
				<menu>
					<ul>
						<li><a href="index.html"> HOME </a></li>
						<li><a href="sobre.html"> SOBRE </a></li>
						<li><a href="publicacoes.html"> PUBLICAÇÕES </a></li>
						<li><a href="servicos.html"> SERVIÇOS </a></li>
						<li><a href="treinamentos.html"> TREINAMENTOS </a></li>
						<li><a href="contato.html"> CONTATO </a></li>
					</ul>
				</menu>
			</nav>
		</header>
			
			<section id="title">
				<h1>Serviços</h1>
			</section>
								<section>
									<div class="row">
										<?php
												$sql_post = "SELECT descricao, titulo, idservico FROM servicos where servicostreinamentos = 1";
												$qry = $pdo->query($sql_post);
												//var_dump($pdo->errorInfo());
												while($row = $qry->fetch(PDO::FETCH_OBJ)) {
													?>
										<div class="col-md-12" style="background-color: #fff">
											<div class="row">
												<div class="col-md-12" style="height: 10px "></div>
													<div class="col-md-1" style="height: 10px"></div>
														<div class="col-md-3" style="background-color: #ffcc99"></div>
															<div class="col-md-7">
																<h2><?php echo $row->titulo?></h2>
																<p><?php echo $row->descricao?></p>
															</div>
														<div class="col-md-1" style="height: 10px"></div>
													<div class="col-md-12" style="height: 10px"></div>
											</div>
										</div>
										<?php }?>
									</div>
								</section>
							




		<footer>
			<div id="bottombar">
				<div>
					Preventiva - (19)99999-9999 <br> Campinas, SP
				</div>
			</div>
		</footer>
	</body>
</html>
