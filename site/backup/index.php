<?php
session_start();
require 'database.php';
$pdo = Database::connect();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">

		<style type="text/css">
			.post{
				text-decoration: none;
				color: #000;
			}

			.imagem{
				max-width: 122px;
				max-height: 127px;
			}
			.div-image{
				background-color: #ffcc99; 
				height: 127px;
				/*line-height: 150px;*/
				clear: both;
				text-align: center;
				min-width: 122px;
				max-width: 122px;
				padding: 0;
			}
		</style>

		
		<title>Preventiva</title>
	</head>
	<body>
		<header>
			<nav id="topbar">
				<div class="logo"> Preventiva </div>
				<menu>
					<ul>
						<li><a href="index.html"> HOME </a></li>
						<li><a href="sobre.html"> SOBRE </a></li>
						<li><a href="publicacoes.html"> PUBLICAÇÕES </a></li>
						<li><a href="servicos.html"> SERVIÇOS </a></li>
						<li><a href="treinamentos.html"> TREINAMENTOS </a></li>
						<li><a href="contato.html"> CONTATO </a></li>
					</ul>
				</menu>
			</nav>
		</header>
		
			<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
    				<div class="carousel-item active">
					    <img class="d-block img-fluid" src="imgs/6.jpg" alt="First slide">
    				</div>
			    	<div class="carousel-item">
      					<img class="d-block img-fluid" src="imgs/7.jpg" alt="Second slide">
    				</div>
    				<div class="carousel-item">
      					<img class="d-block img-fluid" src="imgs/14.jpg" alt="Third slide">
    				</div>
  				</div>
			</div>

		<section>

			<br>
			<div class="row">
				<div class="col-md-6" style="background-color: #fff"> 
					<a href="publicacoes.html"><h1> Publicações </h1></a>
					<div class="row">
<!-- 						<div class="col-md-3" style="background-color: #ffcc99"></div>
						<div class="col-md-9"> 
							<a href="lorem.html" style="text-decoration: none; color: #000"><h3>Lorem</h3>
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at neque urna. Sed tincidunt dolor. </p></a>
						</div>

						<div class="col-md-12" style="height: 10px">  </div>

						<div class="col-md-3" style="background-color: #ffcc99"></div>
						<div class="col-md-9"> 
							<a href="ipsum.html" style="text-decoration: none; color: #000"><h3>Ipsum</h3>
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at neque urna. Sed tincidunt dolor. </p></a>
						</div>

						<div class="col-md-12" style="height: 10px">  </div>

						<div class="col-md-3" style="background-color: #ffcc99"></div>
						<div class="col-md-9"> 
							<a href="phasellus.html" style="text-decoration: none; color: #000"><h3>Phasellus</h3>
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at neque urna. Sed tincidunt dolor. </p></a>
						</div>
 -->
						<?php
							$sql_post = "SELECT descricao, titulo, idpublicacao, image FROM publicacoes";
							$qry = $pdo->query($sql_post);
							
							//var_dump($pdo->errorInfo());
							while($row = $qry->fetch(PDO::FETCH_OBJ)) {

								echo "
								<a href=\"conteudo.php?id=$row->idpublicacao\"  style=\"display: flex;\">
									<div class=\"col-md-4 div-image\">
										<img class=\"imagem\" src=\"$row->image\">
									</div>
									<div class=\"col-md-8 post\">
											<h3>$row->titulo</h3>
											<p>$row->descricao</p>
									</div>
								</a>				
								<div class=\"col-md-12\" style=\"height: 20px\"></div>";
							}
						?>
					</div>
				</div>
				<!-- <div class="col-md-2" style="background-color: #fff"></div>  -->
				<div class="col-md-6" style="background-color: #fff"> 
					<a href="sobre.html"><h1> Sobre </h1></a>
					<p style="text-align: justify;">
						<!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at neque urna. Sed tincidunt dolor a est sagittis, vel malesuada lorem sollicitudin. Duis et ligula vitae ipsum consequat ornare sed vel erat. Phasellus maximus tortor eget nunc malesuada condimentum. Vestibulum a erat in nisi fermentum vehicula. Donec dignissim, quam sit amet ullamcorper pellentesque, enim arcu pulvinar arcu, ac pretium justo elit vitae ligula. Sed eros nibh, vestibulum luctus elementum vitae, efficitur ac purus. Maecenas et mauris feugiat orci aliquet bibendum nec et justo. -->
						<?php 
							$sql = 'SELECT descricao from sobre where quem = 0'; 
							$qry = $pdo->query($sql);
							$result = $qry->fetchAll();
							// $sobreEmpresa=$pdo->query($sql);
							/*foreach($pdo->query($sql) as $row){
								$sobreEmpresa = $row->descricao;
							}*/
							//echo $sobreEmpresa;
							echo $result[0]['descricao'];
							//var_dump($result);
						?>
					</p>
				</div>
			</div>

		</section>

		<footer>
			<div id="bottombar">
				<div>
					Preventiva - (19)99999-9999 <br> Campinas, SP
				</div>
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

	</body>
</html>