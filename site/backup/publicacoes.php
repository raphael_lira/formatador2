<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">

		<style type="text/css">
			.post{
				text-decoration: none;
				color: #000;
			}

			.imagem{
				max-width: 263px;
				max-height: 160px;
			}
			.div-image{
				background-color: #ffcc99; 
				height: 160px;
				line-height: 150px;
				clear: both;
				text-align: center;
				min-width: 263px;
				max-width: 263px;
				padding: 0;
			}
		</style>

		<title>Preventiva</title>
	</head>

	<body>
		<header>
			<nav id="topbar">
				<menu>
					<ul>
						<li><a href="index.html"> HOME </a></li>
						<li><a href="sobre.html"> SOBRE </a></li>
						<li><a href="publicacoes.php"> PUBLICAÇÕES </a></li>
						<li><a href="servicos.html"> SERVIÇOS </a></li>
						<li><a href="treinamentos.html"> TREINAMENTOS </a></li>
						<li><a href="contato.html"> CONTATO </a></li>
					</ul>
				</menu>
			</nav>
		</header>
		
		<section>
			<div class="row">
				<div class="col-md-12">
					<h1>Publicações</h1>
				</div>
				
					
				<?php 
				include 'database.php';
				$pdo = Database::connect();
				$sql_post = "SELECT descricao, titulo, idpublicacao, image FROM publicacoes";
				$qry = $pdo->query($sql_post);
				
				//var_dump($pdo->errorInfo());
				while($row = $qry->fetch(PDO::FETCH_OBJ)) {

					echo "
					<a href=\"conteudo.php?id=$row->idpublicacao\"  style=\"display: flex;\">
						<div class=\"col-md-4 div-image\">
							<img class=\"imagem\" src=\"$row->image\">
						</div>
						<div class=\"col-md-8 post\">
								<h3>$row->titulo</h3>
								<p>$row->descricao</p>
						</div>
					</a>				
					<div class=\"col-md-12\" style=\"height: 20px\"></div>";
				}
				//echo $qry->rowCount();?>

			</div>
		</section>

		<footer>
			<div id="bottombar">
				<div>
					Preventiva - (19)99999-9999 <br> Campinas, SP
				</div>
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

	</body>
</html>