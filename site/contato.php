<?php session_start(); ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="myStyle.css"/>
    <link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="modal.css">
	<link rel="stylesheet" type="text/css" href="mediaQ.css">
    <title>Preventiva</title>

    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
    <script>
        function Modal(rua, numero, complemento, bairro, cidade, cep, telefone, email,) {
                $('#rua').val(rua);
                $('#numero').val(numero);
                $('#complemento').val(complemento);
                $('#bairro').val(bairro);
                $('#cidade').val(cidade);
                $('#cep').val(cep);
                $('#telefone').val(telefone);
                $('#email').val(email);
        }
    </script>
</head>
<body>
<?php include "menu.html"; ?>
<section id="contato">
    <!--modal adiciona/altera post-->
    <div class="modal fade" id="contato-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                    </button>
                    <h4 class="panel-title" id="title">
                        Editar informações
                    </h4>
                </div>
                <form action="returnContato.php" id="modal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                    <div class="modal-body" style="padding: 5px;">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center; font-size: 18px;">Rua:</div>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="padding-bottom: 10px;">
                               <input class="form-control" id="rua" name="rua" placeholder="Rua" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center; font-size: 18px;">Número:</div>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="padding-bottom: 10px;">
                                <input class="form-control" id="numero" name="numero" placeholder="Número" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3" style="text-align: center; font-size: 18px;">Complemento:</div>
                            <div class="col-lg-9 col-md-9 col-sm-9" style="padding-bottom: 10px;">
                                <input class="form-control" id="complemento" name="complemento" placeholder="Complemento" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center; font-size: 18px;">Bairro:</div>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="padding-bottom: 10px;">
                                <input class="form-control" id="bairro" name="bairro" placeholder="Bairro" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center; font-size: 18px;">Cidade:</div>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="padding-bottom: 10px;">
                                <input class="form-control" id="cidade" name="cidade" placeholder="Cidade" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center; font-size: 18px;">CEP:</div>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="padding-bottom: 10px;">
                                <input class="form-control" id="cep" name="cep" placeholder="CEP" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center; font-size: 18px;">Telefone:</div>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="padding-bottom: 10px;">
                                <input class="form-control" id="telefone" name="telefone" placeholder="Telefone" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center; font-size: 18px;">E-mail:</div>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="padding-bottom: 10px;">
                                <input class="form-control" id="email" name="email" placeholder="E-mail" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="margin-bottom:-14px;">
                        <button style="float: right;" type="button" class="btn btn-default btn-close"
                                data-dismiss="modal">
                            Cancelar
                        </button>
                        <input type="submit" class="btn btn-success" value="Salvar"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h1>Contato</h1>
            <?php include ('form_valida.php'); ?>
	    <div class="sucesso"> <?= $sucesso ?> </div>
	    <form action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
                <div class="form-group">
                    <!-- <label for="nome" class="obrigatorio control-label">Nome completo</label> -->
                    <input placeholder="Nome" class="form-control" id="nome" type="text" name="nome" value="<?= $nome ?>"/>
                    <span class="error"> <?= $erro_nome ?> </span>
                </div>
                <div class="form-group">
                    <!-- <label for="empresa" class="control-label">Empresa</label> -->
                    <input placeholder="Empresa" class="form-control" id="empresa" type="text" name="empresa" value="<?= $empresa ?>"/>
                </div>
                <div class="form-group">
                    <!-- <label for="email" class="obrigatorio control-label">Email</label> -->
                    <input placeholder="E-mail" class="form-control" id="email" type="text" name="email" value="<?= $email ?>"/>
                    <span class="error"> <?= $erro_email ?> </span>
                </div>
                <div class="form-group">
                    <!-- <label for="telefone" class="obrigatorio control-label">Telefone</label> -->
                    <input placeholder="Telefone" class="form-control" id="telefone" type="text" name="telefone" value="<?= $telefone ?>"/>
                    <span class="error"> <?= $erro_telefone ?> </span>
                </div>
                <div class="form-group">
                    <!-- <label for="assunto" class="obrigatorio control-label">Assunto</label> -->
                    <input placeholder="Assunto" class="form-control" id="assunto" type="text" name="assunto" value="<?= $assunto ?>"/>
                </div>
                <div class="form-group">
                    <!-- <label for="mensagem" class="obrigatorio control-label">Mensagem</label> -->
                    <textarea placeholder="Digite aqui a mensagem..." id="mensagem" rows="5" class="form-control" name="mensagem" value="<?= $mensagem ?>"></textarea>
		    <span class = "error"> <?= $erro_mensagem ?> </span>
                </div>

                <div id="captcha" class="g-recaptcha" data-sitekey="6LfF-xQTAAAAAMbHq5HMFVGMkgaHExxlIjx1I05E"></div>
                <input type="submit" class="btn btn-success" value="Enviar"/>
                <div class="sucesso"> <?= $sucesso ?> </div>
            </form>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h1>Informações</h1>
			<?php
			require 'database.php';
			$pdo = Database::connect();
			$sql_contato = "SELECT rua, numero, complemento, bairro, cidade, cep, telefone, email, idcontato FROM contato WHERE idcontato = 1";
			$qry = $pdo->query($sql_contato);
			$edicao = '';
			$logado = isset($_SESSION['usuarioID']);
			while ($row = $qry->fetch(PDO::FETCH_OBJ)) {
				if ($logado) {
					$edicao .= "<a href=\"#\" onclick=\"Modal('" . $row->rua . "', '" . $row->numero . "', '" . $row->complemento . "', '" . $row->bairro . "', '" . $row->cidade . "', '" . $row->cep . "', '" . $row->telefone . "', '" . $row->email . "');return false;\" data-toggle=\"modal\" data-target=\"#contato-modal\">
											<img style='width: 40px;' src='imgs/icon/edit.png'>
										  </a>";

				}
				$telefone = $row->telefone; ?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<?php echo "$row->rua, $row->numero, $row->complemento, $row->bairro, $row->cidade - CEP $row->cep <br/> Tel: $row->telefone <br/> E-mail: $row->email $edicao<br><br>"; ?>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d394.84381327654853!2d-47.03582349623886!3d-22.821865812797153!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c8c46bb671d6fb%3A0x7853b5fbbd9a37df!2sR.+Embiru%C3%A7u%2C+250+-+Lot.+Alphaville+Campinas%2C+Campinas+-+SP%2C+13098-320%2C+Brasil!5e1!3m2!1spt-BR!2sus!4v1504020472418"
							width="425" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>

			<?php } ?>
		</div>
    </div>

    <br/><br/>
</section>

<footer>
    <div id="bottombar">
        <div>
            Preventiva - <?php echo $telefone; ?> <br> Campinas, SP
        </div>
    </div>
</footer>


</body>
</html>
