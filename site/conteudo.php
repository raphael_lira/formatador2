<?php session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="myStyle.css" />
    <link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="mediaQ.css">

    <style type="text/css">

        /* BODY */

        html,body{
            height: 100%;
            margin:0;padding:0;
        }

        /*A2*/

        #A2{
            position:relative;
            width:100%;
            height:auto;
            z-index:1;
            opacity:0;
        }

        #text-2{
            position:relative;
            background-color:transparent;
            width:550px;
            height:auto;
            font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
            top:50%;
            left:50%;
            margin-left:-275px;
            z-index:3;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            text-align:center;
            font-size:40px;
            line-height:150%;
            /*color:#444549;*/
            letter-spacing:-1px;
            padding-top:50px;
        }



        #wrapper-A2-date-1{
            position:relative;
            width:130px;
            height:40px;
            z-index:1;
            left:50%;
            margin-left:-65px;
            padding-bottom:75px;
        }

        /*A3*/

        #wrapper-A3{
            position:relative;
            width:600px;
            z-index:1;
            left:50%;
            margin-left:-240px;
            height:auto;
            overflow-x:hidden;
            opacity:0;
            max-width: 30em;

        }

        #A3-top{
            position:relative;
            width:100%;
            height:auto;
            z-index:1;
            top:0;
            left:0;
        }

        #A3-image{
            position:relative;
            z-index:1;
            top:0;
            left:0;
            width:100%;
        }

        #A3-image img{width:100%; text-align: center;}

        #A3-top-2{
            position:relative;
            width:100%;
            height:auto;
            z-index:1;
            padding-top:50px;
            left:0;
        }

        #A3-text-1{
            position:relative;
            width:100%;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            text-align:left;
            font-size:16px;
            line-height:170%;
            left:0;
            color:#474851;
            font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
        }

        /*POST WITH VIDEO*/

        iframe {
            width:100%;
        }

        .video-container-1 iframe,.video-container-1 object,.video-container-1 embed {
            width:100%;
            height:100%;
            position:absolute;
            top:0;
            left:0;
        }


        input {
            /* styles de caractères */
            font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
            font-size: 16px;
            color: #CCC;
            /* marge et taille */
            width: 578px;
            height: 40px;
            padding: 5px 10px 0px 10px;
            margin: 0 0 20px 0;
            /* couleur de fond */
            background:#FFF;
            /* bordures */
            border:1px solid #CCC;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            outline:none;
        }

        textarea {
            /* styles de caractères */
            font-size: 16px;
            color: #CCC;
            font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
            /* marge taille et ombre portée */
            width: 578px;
            height: 120px;
            padding: 10px 10px 0px 10px;
            margin: 0 0 20px 0;
            /* couleur de fond */
            background:#FFF;
            /* bordures */
            border:1px solid #CCC;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            outline:none;
        }

        input[type=submit] {
            /* position taille marges */
            cursor: pointer;
            width: 200px;
            height: 40px;
            padding: 5px 10px;
            /* coins arrondies */
            transition: all 0.5s ease-in-out;
            -webkit-transition: all 0.5s ease-in-out; /** Chrome & Safari **/
            -moz-transition: all 0.5s ease-in-out; /** Firefox **/
            -o-transition: all 0.5s ease-in-out; /** Opera **/
            /* fond */
            background-color:#31323a;
            /* styles de texte */
            color:#FFF;
            font-weight:bold;
            font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            border:1px solid #CCC;
            border-radius:5px;

        }

        input:focus, textarea:focus {
            background:#FFF;
        }

        input[type=submit]:hover {
            background: #51CE77;
            /* styles de texte */
            color: #FFF;
            border: none;
        }

        input[type=submit]:active {
            background: #51CE77;
            /* styles de texte */
            color: #CCC;
            border: none;
        }

        fieldset {
            border: none;
            margin: 0;
            padding:0;
        }

        textarea {
            resize: vertical;
            max-height:250px;
            min-height:100px;
            border:1px solid #CCC;
        }


        /* MQUERIES FOR SMARTPHONE VERY LARGE & E-READERS */
        @media (max-width: 640px) {
            #wrapper-A3{width:460px;margin-left:-170px;}
            #text-2{width:80%;margin-left:10%;left:0%;}

            input,textarea {width:438px;}
        }

        /* MQUERIES FOR NEW SMARTPHONE HYBRID AND SMALL TABLET BY HEIGHT */
        @media (max-height: 600px) {
            #wrapper-A3{width:400px;margin-left:-140px;}
        }


        /* MQUERIES FOR SMARTPHONE LARGE & IPHONE 4 HORIZONTAL */
        @media (max-width: 480px) {
            #wrapper-A3{width:400px;margin-left:-140px;}
            #text-2{font-size:30px;}
            input,textarea {width:378px;}
        }

        /* MQUERIES FOR SMARTPHONE LARGE & IPHONE 4 HORIZONTAL */
        @media (max-width: 400px) {
            #wrapper-A3{width:300px;margin-left:-90px;}
            input,textarea {width:278px;}
            #text-2{margin-top:20px;}
            #wrapper-A2-date-1{padding-top:20px;padding-bottom:20px;width:200px;margin-left:-100px;}
        }

        /* MQUERIES ADDITIONAL FOR SMARTPHONE IPHONE 3 & 4 VERTICAL POSITION BY HEIGHT*/
        @media (max-width: 320px) {
            #wrapper-A3{width:280px;margin-left:-80px;}
            input,textarea {width:258px;}

        }

        /* MQUERIES ADDITIONAL FOR SMARTPHONE IPHONE 3 & 4 HORIZONTAL POSITIONBY HEIGHT*/
        @media (max-height: 330px) {
            #text-2{margin-top:20px;}
            #wrapper-A2-date-1{padding-top:20px;padding-bottom:20px;width:200px;margin-left:-100px;}
        }

        #bg-yellow{
            background-color: #ffd133;
            padding: 2px 10px;
        }

    </style>

    <title>Preventiva</title>
</head>

<body>
<?php include "menu.html"; ?>
<?php
include 'database.php';
$id = isset($_GET['id']) ? $_GET['id'] : 0;
$pdo = Database::connect();
function convertYoutube($string) {
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
        $string
    );
}

$sql_post = "SELECT descricao, titulo, idpublicacao, image FROM publicacoes WHERE idpublicacao = $id";
$qry = $pdo->query($sql_post);


//die($qry);
?>
<section>

    <?php while($row = $qry->fetch(PDO::FETCH_OBJ)) { ?>
        <div id="A2" style="opacity: 1;">

            <div id="text-2">
                <span id="bg-yellow">
                    <?php echo $row->titulo; ?>
                </span>
            </div>

            <div id="wrapper-A2-date-1">
                <div id="A2-icone-eye-1"></div>
            </div>
        </div>


        <div id="wrapper-A3" class="object" style="opacity: 1;">

            <div id="A3-top">
                <div id="A3-image"><img src="../php/<?php echo $row->image;?>" alt="post" title="post"></div>
            </div>

            <div id="A3-top-2"  data-wow-offset="10" style="animation-name: none;">
                <div id="A3-text-1">
                    <?php echo convertYoutube($row->descricao); ?>
                </div>
            </div>
        </div>
    <?php } ?>
</section>



<footer>
    <div id="bottombar">
        <div>
            Preventiva - (19)3367-2758 <br> Campinas, SP
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

</body>
</html>

