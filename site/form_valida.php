<?php 

// define variables and set to empty values
$erro_nome = $erro_email = $erro_telefone = $erro_mensagem = "";
$nome = $empresa = $email = $telefone= $assunto = $mensagem = $mensagem_corpo = $sucesso = "";

//form is submitted with POST method
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["nome"])) {
    $erro_nome = "Nome obrigatorio";
  } else {
    $nome = test_input($_POST["nome"]);
    // check if name only contains letters and whitespace
    // if (!preg_match("/^[a-zA-Z ]*$/",$nome)) {
    //   $erro_nome = "Only letters and white space allowed"; 
    // }
  }

  if (empty($_POST["empresa"])) {
    $empresa = "";
  } else {
    $empresa = test_input($_POST["empresa"]);
  }

  if (empty($_POST["email"])) {
    $erro_email = "Email obrigatorio";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
//    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//      $erro_email = "Formato invalido de email"; 
//    }
  }
  
  if (empty($_POST["telefone"])) {
    $erro_telefone = "Telefone obrigatorio";
  } else {
    $telefone = test_input($_POST["telefone"]);
    // check if e-mail address is well-formed
   // if (!preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i",$telefone)) {
     // $erro_telefone = "Numero de telefone invalido"; 
    //}
  }

  // if (empty($_POST["url"])) {
  //   $url_error = "";
  // } else {
  //   $url = test_input($_POST["url"]);
  //   // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
  //   if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) {
  //     $url_error = "Invalid URL"; 
  //   }
  // }

  if (empty($_POST["assunto"])) {
    $assunto = "";
  } else {
    $assunto = test_input($_POST["assunto"]);
  }

  if (empty($_POST["mensagem"])) {
    $erro_mensagem = "Mensagem obrigatoria";
  } else {
    $mensagem = test_input($_POST["mensagem"]);
  }
  
  if ($erro_nome == '' and $erro_email == '' and $erro_telefone == '' and $erro_mensagem == ''){
      $mensagem_corpo = '';
      unset($_POST['submit']);
      foreach ($_POST as $key => $value){
          $mensagem_corpo .=  "$key: $value\n";
      }
      
      $to = 'teste@preventiva.med.br';
      $subject = 'Contact Form Submit';
      if (mail($to, $assunto, $mensagem_corpo)){
          $sucesso = "Mensagem enviada! Obrigado por nos contatar! Entraremos em contato o mais rapido possivel.";
          $nome = $empresa = $email = $telefone = $assunto = $mensagem = '';
      }
  }
  
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}