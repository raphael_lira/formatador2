<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="mediaQ.css">

		<style type="text/css">
			.post{
				text-decoration: none;
				color: #000;
			}

			.imagem{
				max-width: 263px;
				max-height: 160px;
			}
			.div-image{
				background-color: #fff /*#ffcc99*/; 
				height: 160px;
				line-height: 150px;				
				text-align: center;
				min-width: 263px;
				max-width: 263px;
				padding: 0;
				float: left;
			}
		</style>

		<title>Preventiva</title>
	</head>

	<body>
		<?php include "menu.html"; ?>
		<section>
			<div class="row">
				<div class="col-md-12">
					<h1>Publicações</h1>
				</div>
				
					
				<?php 
				include 'database.php';
				$pdo = Database::connect();
				$sql_post = "SELECT descricao, titulo, idpublicacao, image FROM publicacoes";
				$qry = $pdo->query($sql_post);
				//die($qry);
				//var_dump($pdo->errorInfo());
				
        function readMoreFunction($story_desc,$id) {
            //Number of characters to show
            $chars = 50;
//            $story_desc = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" allowfullscreen></iframe>",$story_desc);
//            $story_desc = preg_replace('/<iframe.*?\/iframe>/i','', $story_desc);
            $story_desc = substr($story_desc,0,$chars);
            $story_desc = substr($story_desc,0,strrpos($story_desc,' '));
            $story_desc = $story_desc." <a href='conteudo.php?id=$id'>Ler mais...</a>";
            return $story_desc;
        }

				while($row = $qry->fetch(PDO::FETCH_OBJ)) {

					echo "
					<a href=\"conteudo.php?id=$row->idpublicacao\"  style=\"display: flex; text-decoration: none;\">
						<div class=\"col-md-4 div-image\">
							<img class=\"imagem\" src=\"../php/$row->image\">
						</div>
						<div class=\"col-md-8 post\">
								<h3>$row->titulo</h3>
								<p>".readMoreFunction($row->descricao, $row->idpublicacao)."</p>
						</div>
					</a>				
					<div class=\"col-md-12\" style=\"height: 20px\"></div>";
				}
				//echo $qry->rowCount();?>

			</div>
		</section>

		<footer>
			<div id="bottombar">
				<div>
					Preventiva - (19)3367-2758 <br> Campinas, SP
				</div>
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

	</body>
</html>