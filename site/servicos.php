<?php session_start();?>
<!DOCTYPE html>
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="myStyle.css" />
		<link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="mediaQ.css">
		
		<title>Preventiva</title>
	</head>

	<body>
		<?php include "menu.html"; ?>
		<section>
    		<div class="col-md-12" style="height: 20px"></div>
   				<div class="row">
       				<div class="col-md-12">
            			<h1>Serviços</h1>
        </section>
        
        	<section>    				
            	<div class="row">

						<?php require 'database.php';
		 				  $pdo = Database::connect();
                        
                         $sql_post = "SELECT descricao, titulo, idservico, image FROM servicos where servicostreinamentos = 1";
				         $qry = $pdo->query($sql_post);
        				 //var_dump($pdo->errorInfo());
                    	while($row = $qry->fetch(PDO::FETCH_OBJ)) {
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 text-center">
                            <div class="team boxed-grey text-center">
                                <div class="avatar"><img class="imagem" src= "../php/<?php echo $row->image;?>"  width="200" height="200" style="border-radius: 100%" alt="" class="img-responsive img-circle" /></div>
                                    <h2 id="titulo-<?php echo $row->idservico; ?>"><?php echo $row->titulo; ?></h2>
                                    <p id="descricao-<?php echo $row->idservico; ?>"><?php echo $row->descricao; ?></p>
                                  <div class="col-md-12" style="height: 20px"></div>
                            </div>
                        </div>
                    <?php } ?>  
                </div>
			</section>

		<footer>
			<div id="bottombar">
				<div>
					Preventiva - (19) 3367-2758 <br> Campinas, SP
				</div>
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

	</body>
</html>
